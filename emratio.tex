\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Measurement of Charge-to-Mass (\emph{e/m}) Ratio for the Electron}

\textbf{Experiment objectives}: measure the ratio of the electron charge-to-mass ratio $e/m$ by studying the electron trajectories in a uniform magnetic field.

\subsection*{History}
    J.J. Thomson first measured the charge-to-mass ratio of the
    fundamental particle of charge in a cathode ray tube in
    1897. A cathode ray tube basically consists of two metallic
    plates in a glass tube which has been evacuated and filled
    with a very small amount of background gas. One plate is
    heated (by passing a current through it) and ``particles'' boil
    off of the cathode and accelerate towards the other plate
    which is held at a positive potential. The gas in between the
    plates inelastically scatters the electrons, emitting light
    which shows the path of the particles. The charge-to-mass
    (\emph{e/m}) ratio of the particles can be measured by observing
    their motion in an applied magnetic field. Thomson repeated
    his measurement of \emph{e/m} many times with different metals for
    cathodes and also different gases. Having reached the same
    value for \emph{e/m} every time, it was concluded that a fundamental
    particle having a negative charge \emph{e} and a mass 2000 times less
    than the lightest atom existed in all atoms. Thomson named
    these particles ``corpuscles'' but we now know them as
    electrons. In this lab you will essentially repeat Thomson's
    experiment and measure \emph{e/m} for electrons.


\section*{Theory}
         The apparatus shown in Figure \ref{emfig1}
	 consists of a glass tube that
        houses a small electron gun.  This gun has a cathode filament
        from which electrons can be thermionically released (boiled
        off), and a nearby anode which can be set to a potential which
        is positive relative to the cathode.  Electrons boiled off the
        cathode are accelerated to the anode, where most are
        collected.  The anode contains a slit, however, which lets a
        fraction of the electrons into the larger volume of the glass
        tube. Some of these electrons scatter inelastically with the
        background gas, thereby emitting tracer light to define the
        path of the electrons.

\begin{figure}[h]
\centering
\includegraphics[height=2.5in]{./pdf_figs/emfig1}
\caption{\label{emfig1}The schematic for the $e/m$ apparatus.}
\end{figure}

        To establish the uniform magnetic field a pair of circular
        Helmholtz coils are wound and the tube centered in the volume
        of the coils (see Appendix).  The tube is oriented so that the beam which
        exits the electron gun is traveling perpendicular to the
        Helmholtz field.  {\it We would like the field to be uniform, i.e., the same, over
        the orbit of the deflected electrons to the level of 1\% if
        possible}.

An electron released thermionically at the cathode has on the order of 1~eV of
kinetic energy. This electron ``falls'' through the positive anode potential
$V_a$ , gaining a kinetic energy of:
\begin{equation}\label{emeq1}
\frac{1}{2}mv^2=eV_a
\end{equation}
The magnetic field of the Helmholtz coils is perpendicular to this velocity,
and produces a magnetic force which is transverse to both ${\mathbf v}$  and
${\mathbf B}$: ${\mathbf F} = e{\mathbf v \times  B}$. This centripetal force
makes an electron move along the circular trajectory; the radius of this
trajectory $r$ can be found from Newton's Second Law:
\begin{equation}\label{emeq3}
m\left(\frac{v^2}{r}\right)=evB
\end{equation}
From this equation we obtain the expression for the charge-to-mass ratio of
the electron, expressed through the experimental parameters:
\begin{equation}\label{emeq4}
\frac{e}{m} = \frac{v}{rB}
\end{equation}

We shall calculate magnetic field $B$ using the Biot-Savart law for the two
current loops of the Helmholtz coils (see Appendix):
\begin{equation}\label{emeq6}
B=\frac{8}{\sqrt{125}}\frac{\mu_0NI_{hc}}{a}.
\end{equation}
Here $N$ is the number of turns of wire that form each loop, $I_{hc}$ is the
current (which is the same in both loops), $a$ is the radius of the loops (in
meters), and the magnetic permeability constant is  $\mu_0=4\pi  10^{-7}
T~m/A$).

Noting from Eq.(\ref{emeq1})  that the velocity is determined by the potential
$V_a$ as $v=\sqrt{2eV_a/m}$, and using Eq.(\ref{emeq6}) for the magnetic field
$B$, we get:

\begin{equation}\label{emeq7}
	\frac{e}{m}= 2 \frac{V_a}{r^2} \frac{1}{B^2}
	=2 \frac{V_a}{r^2} \left[ \frac{125}{64} \left( \frac{a}{\mu_0 N I_{hc}} \right)^2 \right]
\end{equation}

\begin{boxedminipage}{\linewidth}
The accepted value for the charge-to-mass ratio of the electron is $e/m = 1.7588196 \cdot 10^{11}$ C/kg.
\end{boxedminipage}


\section*{Experimental Procedure}

\textbf{Equipment needed}: Pasco $e/m$ apparatus (SE-9638), Pasco High Voltage
Power supply (for the accelerating voltage and the filament heater), GW power
supply (for the Helmholtz coils), two digital multimeters.
\begin{figure}[h]
\centering
\includegraphics[width=0.8\columnwidth]{./pdf_figs/emratioFig2}
\caption{\label{emfig2}(a) $e/m$ tube; (b) electron gun.}
\end{figure}

\subsection*{Pasco SE-9638 Unit:}

The $e/m$ tube (see Fig.~\ref{emfig2}a) is filled with helium at a pressure of
$10^{-2}$~mm Hg, and contains an electron gun and deflection plates. The
electron beam leaves a visible trail in the tube, because some of the electrons
collide with helium atoms, which are excited and then radiate visible light.
The electron gun is shown in Fig.~\ref{emfig2}b. The heater heats the cathode,
which emits electrons. The electrons are accelerated by a potential applied
between the cathode and the anode. The grid is held positive with respect to
the cathode and negative with respect to the anode. It helps to focus the
electron beam.

The Helmholtz coils of the $e/m$ apparatus have a radius and separation of
$a=15$~cm. Each coil has $N=130$~turns. The magnetic field ($B$) produced by
the coils is proportional to the current through the coils ($I_{hc}$) times
$7.80\cdot10^{-4}$~Tesla/Ampere [$B (tesla) = (7.80 \cdot 10^{-4}) I_{hc}$]. A
mirrored scale is attached to the back of the rear Helmholtz coil. It is
illuminated automatically when the heater of the electron
gun is powered. By lining the electron beam up with its image in the mirrored
scale, you can measure the radius of the beam path without parallax error. The
cloth hood can be placed over the top of the $e/m$ apparatus so the experiment
can be performed in a lighted room.

\subsection*{Pasco High Voltage Power Supply}

The Pasco High Voltage Power Supply in Fig.~\ref{hv_power.fig} provides up to 500 V DC accelerating voltage to the electrodes in the e/m tube, which is measured on the voltmeter shown in Fig.~\ref{emfig3}.  The electron gun heater is also powered by this supply using the right panel, which should be set to a maximum of 6 V AC using the dial on the supply.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{./pdf_figs/eoverm_powersupply.jpeg}
\caption{\label{hv_power.fig} Pasco High Voltage Power Supply.}
\end{figure}

\subsection*{Safety}
You will be working with high voltage. Make all connections when power is off.
Turn power off before changing/removing connections. Make sure that there
are no loose or open contacts.

\subsection*{Set up}
The wiring diagram for the apparatus is shown in Fig.~\ref{emfig3}.
\textbf{Important: Do not turn any equipment until an instructor has checked
your wiring.}
\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{./pdf_figs/emratioFig3}
\caption{\label{emfig3}Connections for e/m Experiment.}
\end{figure}

Acceptable power supplies settings:
\begin{description}
\item[Electron Gun/filament Heater] $6$~V AC. Do not go to \unit[7]{V}!
\item[Electrodes] $150$ to $300$~V DC
\item[Helmholtz Coils] $6-9$~V DC.
\end{description}
\textbf{Warning}: The voltage for a filament heater should \textbf{never} exceed 6.3 VAC. Higher values can burn out filament. \\The Helmholtz current should NOT exceed 2 amps. To avoid accidental overshoot run the power supply at a ``low''  setting in a \textit{constant current} mode and ask the TA or instructor how to set the current limit properly.

\subsection*{Data acquisition}
\begin{enumerate}

\item Slowly turn the current adjust knob for the Helmholtz coils clockwise.
Watch the ammeter and take care that the current is less than 2 A.

\item Wait several minutes for the cathode to heat up. When it does, you
will see the electron beam emerge from the electron gun. Its  trajectory
will be curved by the magnetic field.

\item Rotate the tube slightly if you see any spiraling of the beam.
Check that the electron beam is parallel to the Helmholtz coils. If it is not,
turn the tube until it is. Don't take it out of its socket. As you rotate the
tube, the socket will turn.

\item Measurement procedure for the radius of the electron beam $r$:\\
For each measurement record:
\begin{description}
\item[Accelerating voltage $V_a$]
\item[Current through the Helmholtz coils $I_{hc}$]
\end{description}
Look through the tube at the electron beam. To avoid parallax errors, move your head to align one side of the electron beam ring with its reflection that you can see on the mirrored scale. Measure the radius of the beam as you see it, then repeat the measurement on the other side, then average the results. Each lab partner should repeat this measurement, and estimate the uncertainty. Do this silently and tabulate results. After each set of measurements (e.g., many values of $I_{hc}$ at one value of $V_a$) compare your results. This sort of procedure helps reduce group-think, can get at some sources of systematic errors, and is a way of implementing experimental skepticism. 

\item Repeat the radius measurements for at least 4 values of $V_a$ and for each $V_a$ for 5-6 different values of the magnetic field.

\end{enumerate}

\subsection*{Improving measurement accuracy}
\begin{enumerate}

\item The greatest source of error in this experiment is the velocity of the
electrons. First, the non-uniformity of the accelerating field caused by the
hole in the anode causes the velocity of the electrons to be slightly less than
their theoretical value. Second, collisions with the helium atoms in the tube
further rob the electrons of their velocity. Since the equation for $e/m$ is
proportional to $1/r^2$, and $r$ is proportional to $v$, experimental values
for $e/m$ will be greatly affected by these two effects.

\item To minimize the error due to this lost electron velocity, measure radius
to the outside of the beam path.

\item To minimize the relative effect of collisions, keep the accelerating
voltage as high as possible. (Above $250$~V for best results.) Note, however,
that if the voltage is too high, the radius measurement will be distorted by
the curvature of the glass at the edge of the tube. Our best results were made
with radii of less than $5$~cm.

\item Your experimental values will be higher than theoretical, due to the fact that both major sources of error
cause the radius to be measured as smaller than it should be.

\end{enumerate}


\subsection*{Calculations and Analysis:}
\begin{enumerate}
 \item Calculate $e/m$ for each of the readings using Eq. \ref{emeq7} and determine the uncertainty on $e/m$ based on your uncertainty estimates for those readings.  NOTE: Use MKS units for calculations.
\item  For each of the four $V_a$ settings calculate the mean
$<e/m>$ and the standard deviation $\sigma$.
Are these means consistent with one another sufficiently that you can
combine them?  [Put quantitatively, are they within $2 \sigma$ of each
  other?]
%\item Calculate the {\bf grand mean} for all $e/m$ readings and its standard deviation $\sigma$.  
%\item Specify how this grand mean compares to the accepted value, i.e., how many $\sigma$'s is it from the accepted value ?

\item Finally, plot the data in the following way which should, ( according to Eq. \ref{emeq7}), reveal a linear relationship: plot  $V_a$ on the abscissa [x-axis] versus  $r^2 B^2/2$ on the ordinate [y-axis]. The uncertainty in $r^2 B^2/2$ should come from the standard deviation of the different measurements made by your group at the fixed $V_a$. The optimal slope of this configuration of data should be $<m/e>$. Determine the slope from your plot and its error by doing a linear fit, and compare the slope parameter to the accepted value for $<m/e>$. What is the value of the intercept? What should you expect it to be?

\item Comment on which procedure gives a better value of $<e/m>$ (averaging or linear plot).
\end{enumerate}

\section*{Appendix: Helmholtz coils}

The term Helmholtz coils refers to a device for producing a region of
nearly uniform magnetic field. It is named in honor of the German physicist
Hermann von Helmholtz. A Helmholtz pair consists of two identical coils
with electrical current running in the same direction that are placed
symmetrically  along a common axis, and separated by a distance equal to
the radius of the coil $a$. The magnetic field in the central region may be
calculated using the Biot-Savart law:
\begin{equation}\label{emeq_apx}
B_0=\frac{\mu_0Ia^2}{(a^2+(a/2)^2)^{3/2}},
\end{equation}
where $\mu_0$ is the magnetic permeability constant, $I$ is the total electric current in each coil, $a$ is the radius of the coils, and the separation between the coils is equal to $a$.

This configuration provides very uniform magnetic field along the common axis of the pair, as shown in Fig.~\ref{emfig4}. The correction to the constant value given by Eq.(\ref{emeq_apx}) is proportional to $(x/a)^4$ where $x$ is the distance from the center of the pair. However, this is true only in the case of precise alignment of the pair: the coils must be parallel to each other!
\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{./pdf_figs/emratioFig4}
\caption{\label{emfig4}Dependence of the magnetic field produced by a Helmholtz
coil pair $B$ of the distance from the center (on-axis) $x/a$. The magnetic
field is normalized to the value $B_0$ in the center.}
\end{figure}

\end{document}
