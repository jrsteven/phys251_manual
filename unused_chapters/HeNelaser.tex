%\chapter*{Helium-Neon Laser}
%\addcontentsline{toc}{chapter}{Helium-Neon Laser}

\documentclass{article}
\usepackage{tabularx,amsmath,boxedminipage,epsfig}
    \oddsidemargin  0.0in
    \evensidemargin 0.0in
    \textwidth      6.5in
    \headheight     0.0in
    \topmargin      0.0in
    \textheight=9.0in

\begin{document}
\title{Helium-Neon Laser}
\date {}
\maketitle \noindent
 \textbf{Experiment objectives}: assemble and align  a 3-mW HeNe laser from
readily available optical components, record photographically the transverse mode structure of the
laser output beam, and determine the linear polarization of the light produced by the HeNe laser.

\subsection*{Basic operation of the laser}

    The bright, highly collimated, red light beam ($\lambda = 6328 {\AA}$) from a helium-neon (HeNe)
laser is a familiar sight in the scientific laboratory, in the industrial workplace, and even at the
checkout counter in most supermarkets. HeNe lasers are manufactured in large quantities at low cost
and can provide thousands of hours of useful service. Even though solid-state diode lasers can now
provide red laser beams with intensities comparable to those obtained from HeNe lasers, the HeNe
laser will likely remain a common component in scientific and technical instrumentation for the
foreseeable future.
%
%In this experiment you will (a) assemble a 3-mW HeNe laser from readily available optical components,
%(b) align a HeNe laser cavity using two different cavity mirror configurations, (c) record
%photographically the transverse mode structure of the laser output beam, and (d) determine the linear
%polarization of the light produced by the HeNe laser. The principal goal of this experiment is for
%you to get hands-on experience with the various optical components of a working laser; however, to
%help you appreciate fully the role played by each of the components, a brief overview of the
%principles of HeNe laser operation is given here.

\begin{figure}[h]
\centerline{\epsfig{width=\textwidth, file=HeNesetup.eps}} \caption{\label{HeNesetup.fig}Diagram of
optical and electrical components used in the HeNe laser experiment.}
\end{figure}

The principal goal of this experiment is for
you to get hands-on experience with the various optical components of a working laser; however, to
help you appreciate fully the role played by each of the components, a brief overview of the
principles of HeNe laser operation is given here. The three principal elements of a laser are:
(1) an energy pump, (2) an optical gain medium, and (3)
an optical resonator. These three elements are described in detail below for the case of the HeNe
laser used in this experiment.
\begin{enumerate}
\item \textbf{Energy pump}. A 1400-V DC power supply
maintains a glow discharge or plasma in a glass tube containing an optimal mixture (typically 5:1 to
7:1) of helium and neon gas, as shown in Fig.~\ref{HeNesetup.fig}. The discharge current is limited
to about 5 mA by a 91-k$\Omega$ ballast resistor. Energetic electrons accelerating from the cathode
to the anode collide with He and Ne atoms in the laser tube, producing a large number of neutral He
and Ne atoms in excited states. He and Ne atoms in excited states can deexcite and return to their
ground states by emitting light spontaneously. This light makes up the bright and diffuse pink-red
glow of the plasma that is seen at even in the absence of laser action.

The process of producing He and Ne in specific excited states is known as pumping, and in the HeNe
laser this pumping process occurs through electron-atom collisions in the discharge. In other types
of lasers, pumping is achieved by using light from a bright flashlamp or by using chemical reactions.
Common to all lasers is a process for preparing large numbers of atoms, ions, or molecules in
appropriate excited states so that a desired type of light emission can occur.

\item \textbf{Optical gain medium}.
To achieve laser action it is necessary to have more atoms in excited states than in ground states,
and to establish what is called a \emph{population inversion}. To understand the significance of a
population inversion to HeNe laser action, it is useful to consider the processes leading to
excitation of He and Ne atoms in the discharge, using the simplified diagram of atomic He and Ne
energy levels given in Fig.~\ref{HeNelevels.fig}. The rather complex excitation process necessary for
lasing occurs
in four steps. \\
\emph{(a)} An energetic electron collisionally excites a He atom to the state labeled $2_1S^0$ in
Fig.~\ref{HeNelevels.fig}. A He atom in this excited state is often written He*($2_1S^0$), where the
asterisk is used
to indicate that the He atom is in an excited state. \\

\emph{(b)} The excited He*($2_1S^0$) atom collides with an unexcited Ne atom and the two atoms
exchange internal energy, with an unexcited He atom and excited Ne atom, written Ne*(3$s_2$),
resulting. This energy exchange process occurs with high probability because of the accidental near
equality of the excitation energies of the two levels in these atoms.\\

\emph{(c)} The 3$s_2$ level of Ne is an example of a metastable atomic state, meaning that it is only
after a relatively long time -- on atomic that is -- that the Ne*(3$s_2$) atom deexcites to the
2$p_4$ level by emitting a photon of wavelength 6328 $\AA$. It is this emission of 6328 $\AA$ light
by Ne atoms that, in the presence of a suitable optical suitable optical configuration,
leads to lasing action. \\

\emph{(d)} The excited Ne*(2$p_4$) atom rapidly deexcites to the Ne ground state by emitting
additional photons or by collisions with the plasma tube deexcitation process occurs rapidly, there
are more Ne atoms in the 3$s_2$ state than there are in the 2$p_4$ state at any given moment in the
HeNe plasma, and a population inversion is said to be established between these two levels. When a
population inversion is established between the 3$s_2$ and 2$p_4$ levels of the excited Ne atoms, the
discharge can act as an optical gain medium (a light light amplifier) for light of wavelength 6328
$\AA$. This is because a photon incident on the gas will have a greater probability of being
replicated in a 3$s_2\rightarrow 2p_4$ stimulated emission process (discussed below) than of being
destroyed in the complementary $2p_4\rightarrow 3s_2$ absorption process.


\begin{figure}[h]
\centerline{\epsfig{width=0.8\textwidth, file=HeNelevels.eps}}
\caption{\label{HeNelevels.fig}Simplified atomic energy level diagram showing excited states of
atomic He and Ne that are relevant to the operation of the HeNe laser at 6328~$\AA$.}
\end{figure}

\item \textbf{Optical resonator}. As mentioned in 2(c) above, Ne atoms in the 3$s_2$ metastable
state decay spontaneously to the 2$p_4$ level after a relatively long period of time under normal
circumstances; however, a novel circumstance arises if, as shown in Fig.~\ref{HeNesetup.fig}, a HeNe
discharge is placed between two highly reflecting mirrors that form an \emph{optical cavity} or
\emph{resonator} along the axis of the discharge. When a resonator structure is in place, photons
from the Ne* 3$s_2\rightarrow 2p_4$ transition that are emitted along the axis of the cavity can be
reflected hundreds of times between the two high-reflectance end mirrors of the cavity. These
reflecting photons can interact with other excited Ne*(3$s_2$) atoms and cause them to emit 6328
$\AA$ light in a process known as \emph{stimulated} emission. The new photon produced in stimulated
emission has the same wavelength and polarization as the stimulating photon, and it is emitted in the
same direction. It is sometimes useful for purposes of analogy to think of the stimulated emission
process as a "cloning" process for photons. The stimulated emission process should be contrasted with
spontaneous emission processes that, because they are not caused by any preceding event, produce
photons that are emitted isotropically, with random polarization, and over a broader range of
wavelengths. As stimulated emission processes occur along the axis of the resonator, a situation
develops in which essentially all Ne* 3$s_2\rightarrow 2p_4$ decays contribute deexcitation photons
to the photon stream reflecting between the two mirrors. This photon multiplication (light
amplification) process produces a very large number of photons of the same wavelength and
polarization that travel back and forth between the two cavity mirrors. To extract a light beam from
the resonator, it is only necessary that one of the two resonator mirrors, usually called \emph{the
output coupler}, has a reflectivity of only 99\% so that 1\% of the photons incident on it travel out
of the resonator to produce an external laser beam. The other mirror, called the high reflector,
should be as reflective as possible. The diameter, bandwidth, and polarization of the HeNe laser beam
are determined by the properties of the resonator mirrors and other optical components that lie along
the axis of the optical resonator.

\end{enumerate}


\section*{Experimental Procedure}

\textbf{Equipment needed}: Commercial HeNe laser, HeNe discharge tube connected to the power supply,
two highly reflective mirrors, digital camera, polarizer, photodetector, digital multimeter.

\subsection*{Safety}
A few words of caution are important before you begin setting up your HeNe laser. \\
First, \textbf{never} look directly into a laser beam, as severe eye damage could result. During alignment, you
should observe the laser beam by placing a small, white index card at the appropriate point in the optical path.
Resist the temptation to lower your head to the level of the laser beam in order to see where it is going. \\
Second, \textbf{high voltage} ($\approx 1200$~V) is present at the HeNe discharge tube and you should avoid any
possibility of contact with the bare electrodes of the HeNe plasma tube. \\ Finally, the optical cavity mirrors
and the Brewster windows of the laser tube have \textbf{very delicate optical surfaces} that can be easily
scratched or damaged with a single fingerprint. If these surfaces need cleaning, ask the instructor to
demonstrate the proper method for cleaning them.



\subsection*{Alignment of the laser}

To assemble the HeNe laser and investigate its properties, proceed with the following steps.

\begin{itemize}

\item The discharge lamp has very small and angled windows, so first practice to align the beam of
the commercial HeNe laser through the discharge tube. To do that turn on the commercial laser, place a white
screen or a sheet of paper at some distance and mark the position of the laser spot. Now without turning the
power, carefully place the discharge tube such that the laser beam passes through both angled windows without
distortion, and hit the screen almost in the same point as without the tube. Repeat this step a few times until
you are able to insert the tube inside the cavity without loosing the alignment. Then carefully slide the tube
out of the beam and clamp it down.

\item Set up a hemispherical resonator configuration using a flat, high reflectivity (R = 99.7\%)
mirror, and a spherical mirror with a radius of curvature of r = 0.500 m and reflectivity R = 99\%.
The focal length f of the spherical mirror is given by f = r/2 = 0.250 m. In the diagram of
Fig.~\ref{HeNesetup.fig}, the flat, highly-reflective mirror will be serving as the right end of the
cavity, and the spherical, less-reflective mirror will be serving as the left end of the cavity and
is known as the output coupler. The high reflectivity of each mirror is due to a multilayer
dielectric coating that is located on only one side of each mirror. Be sure to have the reflecting
surfaces of both mirrors facing the interior of the optical cavity. Set the distance between the two
mirrors to approximately d = 47 cm.

\item To align the optical resonator of your HeNe laser it is easiest to use a beam of a working,
commercial HeNe laser as a guide. Direct this alignment laser beam to the center of the high reflector mirror,
with the output coupler and the HeNe discharge tube removed. With the room lights turned off, adjust the high
reflector mirror so that its reflected beam returns directly into the output aperture of the alignment laser.
Now insert and center the output coupler mirror, and also adjust it such that the reflected beam (from the back
of the mirror) returns to the alignment laser. Now insert a small white card near the front of the output
couplers very close to the laser beam but without blocking it, and locate the reflected beam from the high
reflector mirror - it should be fairly close to the input beam.  Using fine adjustment screws in the high
reflector mirror overlap these two beams as good as you can. In case of success you most likely will see some
light passing through a high reflection mirror - fine-tune the position of the mirror some more to make this
light as bright as possible.
%and
%aAdjust the output coupler mirror until you observe concentric interference rings on its intracavity
%surface. It is likely that the interference rings will be converging or diverging slowly. It may be
%necessary to adjust the spacing, d, between the two mirrors to achieve perfectly circular rings.

\item Now reinsert the HeNe plasma tube between the two mirrors of the optical cavity and adjust the
plasma tube position so that the alignment beam passes through the center of the Brewster windows of the plasma
tube. Be careful not to touch the Brewster windows or mirror surfaces during this process. With the HeNe plasma
tube in place, it should be possible to see a spot at the center of the high reflector mirror that brightens and
dims slowly.  %at approximately the same rate as the diverging and converging circular interference rings
%observed earlier.

\item Turn on the high voltage power supply to the HeNe plasma tube and (with luck) you will observe
the HeNe lasing. If lasing does not occur, make small adjustments to the plasma tube and the two
mirrors. If lasing still does not occur, turn off the high voltage supply, remove the HeNe plasma
tube, and readjust the resonator mirrors for optimal interference rings. If after several attempts
you do not achieve proper lasing action, ask the instructor for help in cleaning the Brewster windows
and resonator mirrors.

\item Once lasing is achieved, record your alignment procedure in your laboratory notebook. %Describe
%with a well-labeled sketch the nature of the concentric rings that you observed when aligning the
%optical cavity. Determine the range of distances between the two mirrors for which lasing action can
%be maintained in the confocal resonator configuration. Do this in small steps, by increasing or
%decreasing the mirror separation distance d in small increments, and making small adjustments to the
%two mirrors to maintain laser output.
Turn off the alignment laser - you do not need it anymore.

\end{itemize}

\subsection*{Study of the mode structure of the laser output}

Place a white screen at the output of your laser at some distance and inspect the shape of your beam.
Although it is possible that your beam is one circular spot, most likely you will notice some
structure as if the laser output consists of several beams. If you now slightly adjust the alignment
of either mirror you will see that the mode structure changes as well.

As you remember, the main purpose of the laser cavity is to make the light bounce back and forth
repeating its path to enhance the lasing action of the gain medium. However, depending on the precise
alignment of the mirrors it may take the light more than two bounces to close the loop: it is often
possible for the beam to follow a rather complicated trajectory inside the resonator, resulting in
complex transverse mode structure at the output.
\begin{itemize}

\item
Take photographs of the transverse mode structure of the HeNe laser output beam. By making small
adjustments to the mirrors and the position of the HeNe plasma tube it should be possible to obtain
transverse mode patterns. Mount your photographs in your laboratory notebook.

\item
Adjust the mirrors such that the output mode has several maxima and minima in one direction. To
double-check that this mode is due to complicated trajectory of a light inside the resonator, very
carefully insert an edge of a white index card into the cavity, and move it slowly until the laser
generation stops. Now mover the card back and force around this point while watching the generation
appear and disappear, and pay close attention to the mode structure of the laser output. You may
notice that the complicated transverse mode pattern collapses to simpler mode when the card blocks
part of the original mode volume, forcing the generation in a different mode. Describe your
observation in the lab journal.


\end{itemize}

\subsection*{Measure the polarization of the laser light}

When a linearly polarized light beam of intensity $I_0$ passes through a linear polarizer that has
its axis rotated by angle $\theta$ from the incident light beam polarization, the transmitted
intensity $I$ is given by Malus's law:
\begin{equation}
I = I_0 cos^2\theta.
\end{equation}

In our experiment the laser generates linearly polarized light field. This is insured by the Brewster
windows of the HeNe plasma tube: the angle of the windows is such that one light polarization
propagates almost without reflection. This polarization direction is in the same plane as the
incident beam and the surface normal (i.e. the plane of incidence). The light of the orthogonal
polarization experiences reflection at every window, that makes the optical losses too high for such
light.

\begin{itemize}

\item Visually inspect the discharge tube, note its orientation in the lab book. Make a rough prediction of
the expected polarization of the generated beam.

\item Determine the linear polarization of the HeNe laser output beam using the rotatable polarizer
and photodiode detector.  Make detector readings at several values of angle $\theta$ (every
$20^\circ$ or so) while rotating the polarizer in one full circle, and record them in a neat table in
your laboratory notebook. Graph your data to demonstrate, fit with the expected $cos^2\theta$
dependence, and from this graph determine the orientation of the laser polarization. Compare it with
your predictions based on the Brewster windows orientation, and discuss the results in your lab
report.

\end{itemize}


\section*{Acknowledgements}

This lab would be impossible without help of Dr. Jeff Dunham from the Physics Department of the
Middlebury College, who shared important information about experimental arrangements and supplies, as
well as the lab procedure. This manual is based on the one used in Physics 321 course in Middlebury
College.

\end{document}
\newpage
