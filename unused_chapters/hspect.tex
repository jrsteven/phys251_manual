%\chapter*{Atomic Spectroscopy of the Hydrogen Atom}
%\addcontentsline{toc}{chapter}{Hydrogen Spectrum}
\documentclass{article}
\usepackage{tabularx,amsmath,boxedminipage,epsfig}
    \oddsidemargin  0.0in
    \evensidemargin 0.0in
    \textwidth      6.5in
    \headheight     0.0in
    \topmargin      0.0in
    \textheight=9.0in

\begin{document}
\title{Atomic Spectroscopy of Hydrogen Atoms}
\date {}
\maketitle \noindent
 \textbf{Experiment objectives}: test and calibrate a diffraction grating-based spectrometer
 and measure the energy spectrum of atomic hydrogen.

\subsection*{History}

    The observation of discrete lines in the emission spectra of
    atomic gases gives insight into the quantum nature of
    atoms. Classical electrodynamics cannot explain the existence
    of these discrete lines, whose energy (or wavelengths) are
    given by characteristic values for specific atoms. These
    emission lines are so fundamental that they are used to
    identify atomic elements in objects, such as in identifying
    the constituents of stars in our universe. When Niels Bohr
    postulated that electrons can exist only in orbits of discrete
    energies, the explanation for the discrete atomic lines became
    clear.  In this laboratory you will measure the wavelengths of
    the discrete emission lines from hydrogen gas, which will give
    you a measurement of the energy levels in the hydrogen atom.

\section*{Theory}

    The hydrogen atom is composed of a proton nucleus and a single
electron in a bound state orbit. Bohr's groundbreaking hypothesis, that the
electron's orbital angular momentum is quantized, leads directly to the
quantization of the atom's energy, i.e., that electrons in atomic systems exist
only in discrete energy levels.  The energies specified for a Bohr atom of
atomic number $Z$ in which the nucleus is fixed at the origin (let the nuclear
mass $\rightarrow \infty$) are given by the expression:
\begin{equation}\label{Hlevels_inf}
E_n=- \frac{2\pi^2m_ee^4Z^2}{(4\pi\epsilon_0)^2h^2n^2}
 = -hcZ^2R_{\infty}\frac{1}{n^2}
\end{equation}
%
where  $n$  is the  label for  the {\bf principal quantum number}
 and $R_{\infty}=\frac{2\pi m_ee^4}{(4\pi\epsilon_0)^2ch^3}$   is called the
{\bf Rydberg wave number} (here $m_e$ is the electron mass).  Numerically,
$R_{\infty}
= 1.0974 \times  10^5 cm^{-1}$ and $hcR_{\infty} = 13.605 eV$.

An electron can change its state only by making a transition ("jump") from an
``initial'' excited state of energy  $E_1$ to a ``final'' state of lower energy
$E_2$ by emitting a photon of energy  $h\nu  = E_1 - E_2$ that carries away the
excess energy.  Thus frequencies of spectral emission lines are proportional to
the difference between two allowed discrete energies for an atomic
configuration. Since  $h\nu = hc/\lambda$, we can write for this case:
\begin{equation} \label{Hlines_inf}
\frac{1}{\lambda}=\frac{2\pi^2m_ee^4Z^2}{(4\pi\epsilon_0)^2ch^3}
\left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right]=
R_{\infty}Z^2\left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right]
\end{equation}
Based on this description it is clear that by measuring the frequencies (or
wavelengths) of photons emitted by an excited atomic system, we can glean
important information about allowed electron energies in atoms.

To make more accurate calculation of the Hydrogen spectrum, we need to take
into account that a hydrogen nucleus has a large, but finite mass, M=AMp (mass
number A=1 and Mp = mass of proton)\footnote{This might give you the notion
that the mass of any nucleus of mass number $A$ is equal to $AM_p$. This is not
very accurate, but it is a good first order approximation.} such that the
electron and the nucleus orbit a common center of mass.  For this two-mass
system the reduced mass is given by $\mu=m_e/(1+m_e/AM_p)$. We can take this
into account by modifying the above expression (\ref{Hlines_inf}) for
1/$\lambda$ as follows:
\begin{equation}\label{Hlines_arb}
\frac{1}{\lambda_A}=R_AZ^2\left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right] \mbox{
where } R_A=\frac{R_{\infty}}{1+\frac{m_e}{AM_p}}
\end{equation}
In particular, for the hydrogen case of  ($Z=1$; $M=M_p$) we have:
\begin{equation}\label{Hlines_H}
\frac{1}{\lambda_H}=R_H\left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right]
\end{equation}
Notice that the value of the Rydberg constant will change slightly for
different elements. However, these corrections are small since nucleus is
typically several orders of magnitude heavier then the electron.


    Fig. \ref{spec} shows a large number of observed transitions between
    Bohr energy levels in hydrogen, which  are grouped into series.  Emitted photon
    frequencies (wavelengths) span the spectrum from the UV
    (UltraViolet) to the IR (InfraRed).  Given our lack of UV or
    IR spectrometers, we will focus upon the optical spectral lines
    that are confined to the Balmer series (visible).  These are
    characterized by a common final state of $n_2$ = 2.  The
    probability that an electron will make a particular
$n_1\rightarrow  n_2$
    transition in the Balmer series can differ considerably,
    depending on the likelihood that the initial $n_1$ level is
    populated from above in the deexcitation process.  This
    results in our being able to observe and measure only the following four
    lines: $6 \rightarrow  2$, $5 \rightarrow  2$, $4 \rightarrow 2$,
 and $3 \rightarrow 2$.


\begin{figure}
\includegraphics[width=0.7\linewidth]{spec.eps}
\caption{\label{spec}Spectrum of Hydrogen. The numbers on the left show the
energies of the hydrogen levels with different principle quantum numbers $n$ in
$eV$. The wavelength of emitted photon in ${\AA}$ are shown next to each
electron transition. }
\end{figure}

In this lab, the light from the hydrogen gas is broken up into its spectral
components by a diffraction grating.  You will measure the angle at which each
line occurs on the left ($\theta_L$) and ($\theta_R$) right sides for as many
diffraction orders $m$ as possible, and use Eq.(\ref{mlambda}) to calculate
$\lambda$, using the following expression, derived in the Appendix.
\begin{equation}\label{mlambda}
m\lambda = \frac{h}{2}\left(\sin\theta_L+\sin\theta_R\right)
\end{equation}
 Then the same
expression will be used to check/calibrate the groove spacing $h$ by making
similar measurements for a sodium spectral lines with known wavelengths.

We will approach the data in this experiment both with an eye to confirming
    Bohr's theory and from Balmer's early perspective of someone
    trying to establish an integer power series linking the
    wavelength of these four lines.

\section*{Spectrometer Alignment Procedure}

Fig. \ref{expspec} gives a top view of the Gaertner-Peck optical spectrometer
used in this lab.
\begin{figure}
\includegraphics[height=4in]{expspec.eps}
\caption{\label{expspec}Gaertner-Peck Spectrometer}
\end{figure}

\subsubsection*{Telescope Conditions:} Start by adjusting the
telescope eyepiece in
    or out to bring the crosshairs into sharp focus.  Next aim the
    telescope out the window to view a distant object such as
    leaves in a tree.  If the distant object is not in focus or if
    there is parallax motion between the crosshairs and the
    object, pop off the side snap-in button to give access to a
    set screw.  Loosen this screw and move the ocular tube in or
    out to bring the distant object into sharp focus.  This should
    result in the elimination of parallax.  Tighten the set screw
    to lock in this focussed condition.

\subsubsection*{Collimator Conditions:} Swing the telescope to view the collimator
    which is accepting light from the hydrogen discharge tube
    through a vertical slit of variable width.  The slit opening
    should be set to about 5-10 times the crosshair width to
    permit sufficient light to see the faint violet line and to be
    able to see the crosshairs.  If the bright column of light is
    not in sharp focus, you should remove a side snap-in button
    allowing the tube holding the slit to move relative to the
    collimator objective lens.  Adjust this tube for sharp focus
    and for elimination of parallax between the slit column and
    the crosshairs.  Finally, tighten the set screw.

\subsubsection*{  Diffraction Grating Conditions:}
\textbf{Appendix in this handout describes the operation of a diffraction
grating!}
    Mount a diffraction grating which nominally
    has 600 lines per mm in a grating baseclamp.
    %Put a piece of
   % doublesided scotch tape on the top surface of the table plate.
    Fix the grating baseclamp to the table such that the grating's
    vertical axis will be aligned with the telescope pivot axis.
    Since the table plate can be rotated, orient the normal of the
    grating surface to be aligned with the collimator axis.  Use
    the AUTOCOLLIMATION procedure  to achieve a fairly accurate
    alignment of the grating surface.  This will determine how to
    adjust the three leveling screws H1, H2, and H3 and the
    rotation angle set screw for the grating table.

    \textbf{AUTOCOLLIMATION} is a sensitive way to align an optical
    element.  First, mount a ``cross slit'' across the objective lens of
    the collimator, and direct a strong light source into the
    input end of the collimator.  Some of the light exiting through
    the cross slit will reflect from the grating and return to the
    cross slit.  The grating can then be manipulated till this
    reflected light retraces its path through the cross slit
    opening.  With this the grating surface is normal to the
    collimator light.
     Then, with the hydrogen tube ON and in place at
    the collimator slit, swing the rotating telescope slowly
    through 90 degrees both on the Left \& Right sides of the forward
    direction.  You should observe diffraction maxima for most
    spectral wavelength, $\lambda$, in 1st, 2nd, and 3rd order.  If these
    lines seem to climb uphill or drop downhill
    the grating will have to be rotated in its baseclamp to
    bring them all to the same elevation.

\section*{Data acquisition and analysis}

Swing the rotating telescope slowly and determine which spectral lines from
Balmer series you observe.

\emph{Lines to be measured:}
\begin{itemize}
\item \emph{Zero order} (m=0): All spectral lines merge.
\item \emph{First order} (m=1): Violet, Blue, Green, \& Red on both Left \&
    Right sides.
\item \emph{Second order} (m=2): Violet, Blue, Green, \& Red on
    both Left \& Right sides.
\item \emph{Third order} (m=3):  Blue, \& Green.
\end{itemize}
    You might not see the Violet line due to its low
    intensity. Red will not be seen in 3rd  order.

Read the angle at which each line occurs, measured with the crosshairs centered
on the line as accurately as possible. Each lab partner should record the
positions of the spectral lines at least once. Use the bottom scale to get the
rough angle reading in degrees, and then use the upper scale for more accurate
reading in minutes. The width of lines is controlled by the Collimator Slit
adjustment screw. If set too wide open, then it is hard to judge the center
    accurately; if too narrow, then not enough light is available
    to see the crosshairs. For Violet the intensity is noticeably
    less than for the other three lines.  Therefore a little
    assistance is required in order to locate the crosshairs at
    this line.  We suggest that a low intensity flashlight be
    aimed toward the Telescope input, and switched ON and OFF
    repeatedly to reveal the location of the vertical crosshair
    relative to the faint Violet line.

\subsubsection*{ Calibration of Diffraction Grating:} Replace the hydrogen tube with
    a sodium (Na) lamp and take readings for the following two
    lines from sodium: $568.27$~nm (green) and $589.90$~nm (yellow).  Extract from
    these readings the best average value for $h$ the groove
    spacing in the diffraction grating.  Compare to the statement
    that the grating has 600 lines per mm.  Try using your measured value
    for $h$ versus the stated value $600$ lines per mm in
    the diffraction formula when obtaining the measured
    wavelengths of hydrogen. Determine which one provide more accurate results, and discuss the conclusion.

\subsubsection*{ Data analysis}
\textbf{Numerical approach}: Calculate the wavelength $\lambda$ for each line
observed. For lines observed in more than one order, obtain the mean value
$\lambda_ave$ and the standard error of the mean $\Delta \lambda$. Compare to
the accepted values which you should calculate using the Bohr theory.

\textbf{Graphical approach}: Make a plot of $1/\lambda$  vs  $1/n_1^2$  where
$n_1$ = the principal quantum number of the electron's initial state.  Put all
$\lambda$ values you measure above on this plot.  Should this data form a
straight line? If so, determine both slope and intercept and compare to the
expected values for each.  The slope should be the Ryberg constant for
hydrogen, $R_H$. The intercept is $R_H/(n_2)^2$. From this, determine the value
for the principal quantum number $n_2$. Compare to the accepted value in the
Balmer series.

\textbf{Example data table for writing the results of the measurements}:

\noindent
\begin{tabular}{|p{1.in}|p{1.in}|p{1.in}|p{1.in}|}
\hline
 Line &$\theta_L$&$\theta_R$&Calculated $\lambda$  \\ \hline
 m=1 Violet&&&\\ \hline
 m=1 Blue&&&\\ \hline
 m=1 Green&&&\\ \hline
 m=1 Red&&&\\ \hline
 m=2 Violet&&&\\ \hline
 \dots&&&\\ \hline
 m=3 Blue&&&\\ \hline
 \dots&&&\\\hline
\end{tabular}

\section*{Appendix: Operation of a diffraction grating-based optical spectrometer}

%\subsection*{Fraunhofer Diffraction at a Single Slit}
%Let's consider a plane electromagnetic wave incident on a vertical slit of
%width $D$ as shown in Fig. \ref{frn}. \emph{Fraunhofer} diffraction is
%calculated in the far-field limit, i.e. the screen is assume to be far away
%from the slit; in this case the light beams passed through different parts of
%the slit are nearly parallel, and one needs  a lens to bring them together and
%see interference.
%\begin{figure}[h]
%\includegraphics[width=0.7\linewidth]{frnhfr.eps}
%\caption{\label{frn}Single Slit Fraunhofer Diffraction}
%\end{figure}
%To calculate the total intensity on the screen we need to sum the contributions
%from different parts of the slit, taking into account phase difference acquired
%by light waves that traveled different distance to the lens. If this phase
%difference is large enough we will see an interference pattern. Let's break the
%total hight of the slit by very large number of point-like radiators with
%length $dx$, and we denote $x$ the hight of each radiator above the center of
%the slit (see Fig.~\ref{frn}). If we assume that the original incident wave is
%the real part of $E(z,t)=E_0e^{ikz-i2\pi\nu t}$, where $k=2\pi/\lambda$ is the
%wave number. Then the amplitude of each point radiator on a slit is
%$dE(z,t)=E_0e^{ikz-i2\pi\nu t}dx$. If the beam originates at a hight $x$ above
%the center of the slit then the beam must travel an extra distance $x\sin
%\theta$ to reach the plane of the lens. Then we may write a contributions at
%$P$ from a point radiator $dx$ as the real part of:
%\begin{equation}
%dE_P(z,t,x)=E_0e^{ikz-i2\pi\nu t}e^{ikx\sin\theta}dx.
%\end{equation}
%To find the overall amplitude one sums along the slit we need to add up the
%contributions from all point sources:
%\begin{equation}
%E_P=\int_{-D/2}^{D/2}dE(z,t)=E_0e^{ikz-i2\pi\nu
%t}\int_{-D/2}^{D/2}e^{ikx\sin\theta}dx = A_P e^{ikz-i2\pi\nu t}.
%\end{equation}
%Here $A_P$ is the overall amplitude of the electromagnetic field at the point
%$P$. After evaluating the integral we find that
%\begin{equation}
%A_P=\frac{1}{ik\sin\theta}\cdot
%\left(e^{ik\frac{D}{2}\sin\theta}-e^{-ik\frac{D}{2}\sin\theta}\right)
%\end{equation}
%After taking real part and choosing appropriate overall constant multiplying
%factors the amplitude of the electromagnetic field at the point $P$ is:
%\begin{equation}
%A=\frac{\sin (\frac{\pi D}{\lambda}\sin\theta)}{\frac{\pi
%D}{\lambda}\sin\theta}
%\end{equation}
%The intensity is proportional to the square of the amplitude and thus
%\begin{equation}
%I_P=\frac{(\sin (\frac{\pi D}{\lambda}\sin\theta))^2}{(\frac{\pi
%D}{\lambda}\sin\theta)^2}
%\end{equation}
%The minima of the intensity occur at the zeros of the argument of the sin. The
%maxima are near, but not exactly equal to the solution of:
%\begin{equation}
%  (\frac{\pi D}{\lambda}\sin\theta)=(m+\frac{1}{2})\pi \end{equation}
%for integer $m$.
%
%The overall pattern looks like that shown in Fig. \ref{sinxox}.
%\begin{figure}
%\includegraphics[width=\linewidth]{sinxox.eps}
%\caption{\label{sinxox}Intensity Pattern for Fraunhofer Diffraction}
%\end{figure}

%\subsection*{The Diffraction Grating}
A diffraction grating is a common optical element, which consists of a pattern
with many equidistant slits or grooves. Interference of multiple beams passing
through the slits (or reflecting off the grooves) produces sharp intensity
maxima in the output intensity distribution, which can be used to separate
different spectral components on the incoming light. In this sense the name
``diffraction grating'' is somewhat misleading, since we are used to talk about
diffraction with regard to the modification of light intensity distribution  to
finite size of a single aperture.
\begin{figure}[h]
\includegraphics[width=\linewidth]{grating.eps}
\caption{\label{grating}Intensity Pattern for Fraunhofer Diffraction}
\end{figure}

To describe the properties of a light wave after passing through the grating,
let us first consider the case of 2 identical slits separated by the distance
$h$, as shown in Fig.~\ref{grating}a. We will assume that the size of the slits
is much smaller than the distance between them, so that the effect of
Fraunhofer diffraction on each individual slit is negligible. Then the
resulting intensity distribution on the screen is given my familiar Young
formula:
\begin{equation} \label{2slit_noDif}
I(\theta)=\left|E_0 +E_0e^{ikh\sin\theta} \right|^2 = 4I_0\cos^2\left(\frac{\pi
h}{\lambda}\sin\theta \right),
\end{equation}
where $k=2\pi/\lambda$, $I_0$ = $|E_0|^2$, and the angle $\theta$ is measured
with respect to the normal to the plane containing the slits.
%If we now include the Fraunhofer diffraction on each slit
%same way as we did it in the previous section, Eq.(\ref{2slit_noDif}) becomes:
%\begin{equation} \label{2slit_wDif}
%I(\theta)=4I_0\cos^2\left(\frac{\pi h}{\lambda}\sin\theta
%\right)\left[\frac{\sin (\frac{\pi D}{\lambda}\sin\theta)}{\frac{\pi
%D}{\lambda}\sin\theta} \right]^2.
%\end{equation}

An interference on $N$ equidistant slits illuminated by a plane wave
(Fig.~\ref{grating}b) produces much sharper maxima. To find light intensity on
a screen, the contributions from all N slits must be summarized taking into
account their acquired phase difference, so that the optical field intensity
distribution becomes:
\begin{equation} \label{Nslit_wDif}
I(\theta)=\left|E_0
+E_0e^{ikh\sin\theta}+E_0e^{2ikh\sin\theta}+\dots+E_0e^{(N-1)ikh\sin\theta}
\right|^2 = I_0\left[\frac{sin\left(N\frac{\pi
h}{\lambda}\sin\theta\right)}{sin\left(\frac{\pi h}{\lambda}\sin\theta\right)}
\right]^2.
\end{equation}
 Here we again neglect the diffraction form each individual slit, assuming that the
 size of the slit is much smaller than the separation $h$ between the slits.

The intensity distributions from a diffraction grating with illuminated
 $N=2,5$ and $10$ slits are shown in Fig.~\ref{grating}c. The tallest (\emph{principle}) maxima occur when the denominator
 of Eq.(~\ref{Nslit_wDif}) becomes zero: $h\sin\theta=\pm m\lambda$ where
 $m=1,2,3,\dots$ is the diffraction order. The heights of the principle maxima are
 $I_{\mathrm{max}}=N^2I_0$, and their widths are $\Delta \theta =
 2\lambda/(Nh)$.
 Notice that the more slits are illuminated, the narrower diffraction peaks
 are, and the better the resolution of the system is:
 \begin{equation}
\frac{ \Delta\lambda}{\lambda}=\frac{\Delta\theta}{\theta} \simeq \frac{1}{Nm}
\end{equation}
For that reason in any spectroscopic equipment a light beam is usually expanded
to cover the maximum surface of a diffraction grating.

\subsection*{Diffraction Grating Equation when the Incident Rays are
not Normal}

Up to now we assumed that the incident optical wavefront is normal to the pane
of a grating. Let's now consider more general case when the angle of incidence
$\theta_i$ of the incoming wave is different from the normal to the grating, as
shown in Fig. \ref{DGnotnormal}a. Rather then calculating the whole intensity
distribution, we will determine the positions of principle maxima. The path
length difference between two rays 1 and 2 passing through the consequential
slits us  $a+b$, where:
\begin{equation}
a=h\sin \theta_i;\,\, b=h\sin \theta_R
\end{equation}
Constructive interference occurs for order $m$ when $a+b=m\lambda$, or:
\begin{equation}
h\sin \theta_i + \sin\theta_R=m\lambda
\end{equation}
\begin{figure}[h]
\includegraphics[width=\columnwidth]{pic4i.eps}
%\includegraphics[height=3in]{dn.eps}
\caption{\label{DGnotnormal}Diagram of the light beams diffracted to the Right
(a) and to the Left (b).}
\end{figure}
Now consider the case shown in Fig. \ref{DGnotnormal}. The path length between
two beams is now $b-a$ where $b=h\sin\theta_L$. Combining both cases we have:
\begin{eqnarray} \label{angles}
h\sin\theta_L-\sin\theta_i&=&m\lambda\\
h\sin\theta_R+\sin\theta_i&=&m\lambda \nonumber
\end{eqnarray}
Adding these equations and dividing the result by 2 yields Eq.(\ref{mlambda}):
\begin{equation}m\lambda=\frac{h}{2}\left(\sin\theta_L+\sin\theta_R\right)
\end{equation}

\end{document}
\newpage
