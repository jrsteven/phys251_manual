%\chapter*{Michelson Interferometer}
%\addcontentsline{toc}{chapter}{Michelson Interferometer}
\documentclass{article}
\usepackage{tabularx,amsmath,boxedminipage,epsfig}
    \oddsidemargin  0.0in
    \evensidemargin 0.0in
    \textwidth      6.5in
    \headheight     0.0in
    \topmargin      0.0in
    \textheight=9.0in

\begin{document}
\title{Michelson Interferometer}
\date {}
\maketitle

\noindent
 \textbf{Experiment objectives}: Assemble and align a Michelson
interferometer, and use it to measure wavelength of unknown laser, and the
refractive index of air.

\section*{History}

Michelson interferometer is an extremely important apparatus. It was used by Michelson and Morley in 1887 to
determine that electromagnetic waves propagate in vacuum, giving the first strong evidence against the theory of
a \textit{luminiferous aether} (a  fictious medium for light wave propagation) and providing the insight into
the true nature of electromagnetic radiation. Nowedays, Michelson interferometer remains a widely used tool in
many areas of physics and engineering. In this laboratory you will use the interferometer to accurately measure
the wavelength of laser light and the index of refraction of air.
\begin{figure}[h]
\centerline{\epsfig{width=0.8\linewidth,file=fig1.eps}} \caption{\label{fig1mich.fig}A Michelson Interferometer
setup.}
\end{figure}

\section*{Theory}

    The interferometer works by combining two light waves
    traversing two path lengths. A diagram of this type of
    interferometer is shown in Figure!\ref{fig1mich.fig}
 A beamsplitter (a glass
    plate which is partially silver-coated on the front surface
    and angled at 45 degrees) splits the laser beam into two parts of equal
    amplitude. One beam (reflected by the
    beamsplitter) travels at 90 degrees toward mirror $M_2$ and back
    again, passing twice through a clear glass plate called the
    compensator plate.  At the beamsplitter one-half of
    this light is transmitted to an observer (you will use a
    viewing screen). At the same time the other beam (that was initially transmitted by the beamsplitter)
    travels to
    a fixed mirror $M_1$ and back again. One-half of this amplitude
    is reflected from the partially-silvered surface and directed
    at 90 degrees toward the observer.  Thus, the total amplitude of the light the observer
    records is a combination of amplitude of  the two beams:
\begin{equation}
\mathbf{E}_{total} =  \mathbf{E}_1 + \mathbf{E}_2 = \frac12 \mathbf{E}_0 + \frac12 \mathbf{E}_0\cos(k\Delta l)
\end{equation}

Here $k\Delta l$ is a phase shift (``optical delay'') between the two wavefronts caused by the difference in
pathlengths  for the two beams $\Delta l$ , $k=2\pi n/\lambda$ is the wave number, $\lambda$ is the wavelength
of light in vacuum, and $n$ is the refractive index of the optical medium (in our case - air).

Mirror $M_2$ is mounted on a precision traveling platform. Imagine that we adjust its position (by turning the
micrometer screw)  such that the distance traversed on both arms is exactly identical. Because the thickness of
the compensator plate and the beamsplitter are the same, both wavefronts pass through the same amount of glass
and air, so the pathlength of the light beams in both interferometer arms will be exactly the same. Therefore,
two fields will arrive in phase to the observer, and their amplitudes will add up constructively, producing a
bright spot on the viewing screen. If now you turn the micrometer to offset the length of one arm by a half of
light wavelength, $\Delta l = \lambda/2$, they will acquire a relative phase of $\pi$, and total destructive
interference will occur:
\begin{displaymath}
\mathbf{E}_1 +\mathbf{E}_2=0\;\;\mathrm{or} \;\;\mathbf{E}_1 = -\mathbf{E}_2.
%\end{displaymath}
% or
%\begin{displaymath}\mathbf{E}_1(t) = -\mathbf{E}_2(t).
\end{displaymath}
It is easy to see that constructive interference happens when the difference between pathlengths in two
interferometer arms is equal to the integer number of wavelengths $\Delta l = m\lambda$, and destructive
interference corresponds to the half-integer number of wavelengths $\Delta l = (m + 1/2) \lambda$ (here $m$ is
an integer number). Since the wavelength of light is small ($600-700$~nm for a red laser), Michelson
interferometers are able to measure distance variation with very good precision.




%Figure 1. The Michelson Interferometer

To simplify the alignment of a Michelson interferometer, it is convenient to work with diverging optical beams.
In this case an interference pattern will look like a set of concentric bright and dark circles, since the
components of the diverging beam travel at slightly different angles, and therefore acquire different phase, as
illustrated in Figure \ref{fig2mich.fig}. Suppose that the actual pathlength difference between two arms is $d$.
Then the path length difference for two off-axis rays arriving at the observer is $\Delta l = a + b$ where $a =
d/\cos \theta$ and  $b = a\cos 2\theta$:
\begin{equation}
\Delta l=\frac{d}{\cos \theta}+\frac{d}{\cos \theta}\cos 2\theta
\end{equation}
Recalling that  $\cos 2\theta = 2(\cos \theta)^2-1$, we obtain $ \Delta l=2d\cos\theta$. The two rays interfere
constructively for any angle $\theta_c$ for which $ \Delta l=2d\cos\theta = m\lambda$ ($m$=integer); at the same
time, two beams traveling at the angle $\theta_d$ interfere destructively when $ \Delta l=2d\cos\theta =
(m+1/2)\lambda$ ($m$=integer). Because of the symmetry about the normal direction to the mirrors, this will mean
that interference ( bright and dark fringes) appears in a circular shape. If fringes are not circular, it means
simply that the mirrors are not parallel, and additional alignment of the interferometer is required.

\begin{figure}
\centerline{\epsfig{width=0.8\linewidth,file=fig2.eps}} \caption{\label{fig2mich.fig}Explanation of circular
fringes. Notice that to simplify the figure we have ``unfold'' the interferometer by neglecting the reflections
on the beamsplitter.}
\end{figure}

When the path length difference $\Delta l$ is varied by moving one of the mirrors using the micrometer, the
fringes appear to "move". As the micrometer is turned, the condition for constructive and destructive
interference is alternately satisfied at any given angle.  If we fix our eyes on one particular spot and count,
for example, how many bright fringes pass that spot as we move mirror $M_2$ by known distance, we can determine
the wavelength of light in the media using the condition for constructive interference, $\Delta l = 2d\cos
\theta = m\lambda$.

For simplicity, we might concentrate on the center of the fringe bullseye at $\theta = 0$. The equation above
for constructive interference then reduces to $2\Delta l = m\lambda$ (m = integer). If $X_1$ is the initial
position of the mirror $M_2$ (as measured on the micrometer) and $X_2$ is the final position after a number of
fringes $\delta m$ has been counted, we have $2(X_2-X_1) = \lambda\delta m$.   Then the laser wavelength,
$\lambda$, is then given as:
\begin{equation}\label{old3}
\lambda = 2(X_2-X_1)/\delta m.
\end{equation}

\section*{Procedure}

\subsection*{Laser Safety}
While this is a weak laser caution should still be used. \textbf{Never look directly at the laser beam!} Align
the laser so that it is not at eye level.

\subsection*{Set Up}
\textbf{Equipment needed}: Pasco precision interferometry kit, a laser,
adjustable-hight platform.

Set up the interferometer as shown in Figure~\ref{fig1mich.fig} using the components of Pasco precision
interferometry  kit. A mirrors $M_{1,2}$ are correspondingly a movable and an adjustable mirror from the kit.
Make initial alignment of the interferometer with a non-diverging laser beam. Adjust the beams so that it is
impinging on the beamsplitter and on the viewing screen. Make sure the beam is hitting near the center of all
the optics, including both mirrors, the compensator plate and beam splitter. The interferometer has leveling
legs which can be adjusted.

Then insert a convex lens after the laser to spread out the beam (ideally the laser beam should be pass through
the center of the lens to preserve alignment). After the beams traverse through the system, the image of the
interfering rays will be a circular pattern projected onto a screen. The two beam reflected off the mirrors
should be aligned as parallel as possible to give you a circular pattern.

\subsection*{ Measurement of laser wavelength}

Note the reading on the micrometer. Focus on a particular fringe (the center is a good place). Begin turning the
micrometer so that the fringes move (for example, from bright to dark to bright again is the movement of 1
fringe). Count a total of about 100 fringes and record the new reading on the micrometer. Calculate the
wavelength from Eq. \ref{old3} above, remembering that you may need to convert the distance traveled on the
micrometer to the actual distance traveled by the mirror.


    Each lab group must make at least four (4) measurements of  $\lambda$. Each
    partner must do at least one. For each trial, a minimum of 100
    fringes should be accurately counted, and related to an
    initial $X_1$ and final $X_2$ micrometer setting.  A final mean
    value of $\lambda$ and its uncertainty  should be
    generated. Compare your value with the accepted value (given
    by the instructor).

\textbf{\emph{Experimental tips}}:
\begin{enumerate}
\item Avoid touching the face of the front-surface mirrors, the beamsplitter, and any other optical elements!
\item Engage the micrometer with both hands as you turn, maintaining
positive torque.
\item The person turning the micrometer should also do the counting of
fringes. It can be easier to count them in bunches of 5 or 10 (\textit{i.e.}
100 fringes = 10 bunches of 10 fringes).
\item  Before the initial position $X_1$ is read make sure that the micrometer has engaged the
drive screw (There can be a problem with "backlash").
\item  Before starting the measurements make sure you understand how to read a
micrometer! See Fig.\ref{fig3mich.fig}.
\item Move the travel plate to a slightly different location for the
four readings.  This can done by loosening the large nut atop the traveling
plate,and then locking again.
\item  Avoid hitting the table which can cause a sudden jump in the
number of fringes.

\end{enumerate}

\begin{figure}[h]
\centerline{\epsfig{width=0.7\columnwidth,file=fig3.eps}} \caption{\label{fig3mich.fig}Micrometer readings. The
course scale is in mm, and smallest division on the rotary dial is 1~$\mu$m (same as 1 micron). The final
measurements is the sum of two. }
\end{figure}


\subsection*{Measurement of the index of refraction of air}

     If you recall from the speed of light experiment, the value
for air's index of refraction  $n_{air}$  is very close to unity:
$n_{air}$=1.000293.  Amazingly, a Michelson interferometer is precise enough to
be able to make an accurate measurement of this quantity!

Let's remind ourselves that a Michelson interferometer is sensitive to a phase
difference acquired by the beams travelling in two arms
\begin{equation}\label{phase}
k\Delta l=2\pi n\Delta l/\lambda.
\end{equation}
In previous calculations we assumed that the index of refraction of air $n$ is exactly one, like in vacuum.
However, it is actually slightly varies with air pressure, as shown in Fig.~\ref{fig4mich.fig}. Any changes in
air pressure affect the phase $k\Delta l$.
%
\begin{figure}
\centerline{\epsfig{file=macfig1add.eps}} \caption{\label{fig4mich.fig}Index of refraction as a function of air
gas pressure}
\end{figure}

To do the measurement, place a cylindrical gas cell which can be evacuated in
the path of light heading to mirror $M_1$ and correct alignment of the
Michelson interferometer, if necessary. Make sure that the gas cell is
initially at the atmospheric pressure.

Now pump out the cell  by using a hand pump at your station and count the number of fringe transitions $\delta
m$ that occur. When you are done, record $\delta m$ and the final reading of the vacuum gauge $p_{fin}$.
\textbf{Note}: most vacuum gauges display the difference between  measured and atmospheric pressure . If
absolute pressure is needed, it should be found by subtracting the gauge reading from the atmospheric pressure
($p_0=76$~cm Hg). For example, if the gauge reads $23$~cm Hg, the absolute pressure is $53$~cm Hg.
Alternatively, you can pump out the air first, and then admit air is slowly to the cell while counting the
number of fringes that move past a selected fixed point.

The shifting fringes indicate a change in relative optical phase difference for the two arms caused by the the
difference in refractive indices of the gas cell at low and atmospheric pressures $\Delta n$. According to
Eq.(\ref{phase}), this difference is
\begin{equation} \label{delta_n}
\Delta n=\delta m \frac{\lambda}{2d_{cell}}
\end{equation}
where $d_{cell}=3$~cm is the length of the gas cell.

Since the change in the refractive index $\Delta n$ is linearly depends on the
air pressure $\Delta p=p_0-p_{fin}$, it is now easy to find out the
proportionality coefficient $\Delta n/\Delta p$ and calculate the value of the
refractive index at the atmospheric pressure $n_{air}$.

Each partner should make one measurement of the fringe shift quantity $\delta m$. Use Eq.(\ref{delta_n}) to find
mean values of the relative change of the refractive index $\Delta n$,  proportionality coefficient $\Delta
n/\Delta p$ and $n_{air}$ with corresponding uncertainties. Compare your measurements to the following
accepted experimental values: \\
Index of Refraction of Air(STP) = 1.000293 \\


\subsection*{\emph{Detection of Gravitational Waves}}

\textbf{\emph{A Michelson interferometer can help to test the theory of
relativity!}} \emph{
%
Gravity waves, predicted by the theory of relativity, are ripples in the fabric
of space and time produced by violent events in the distant  universe, such as
the collision of two black holes. Gravitational waves are  emitted by
accelerating masses much as electromagnetic waves are produced by accelerating
charges, and often travel to Earth. The only indirect evidence for these  waves
has been in the observation of the rotation of a binary pulsar  (for which the
1993 Nobel Prize was awarded).}
%
\begin{figure}[h]
\centerline{\epsfig{file=LIGO.eps}} \caption{\label{LIGO.fig}For more details see http://www.ligo.caltech.edu/}
\end{figure}
\emph{
%
Laser Interferometry Gravitational-wave Observatory (LIGO) sets the ambitious
goal to direct detection of gravitational wave. The measuring tool in this
project is a giant Michelson interferometer. Two mirrors hang $2.5$~mi apart,
forming one "arm" of the interferometer, and two more mirrors make a second arm
perpendicular to the first. Laser light enters the arms through a beam splitter
located at the corner of the L, dividing the light between the arms. The light
is allowed to bounce between the mirrors repeatedly before it returns to the
beam splitter. If the two arms have identical lengths, then interference
between the light beams returning to the beam splitter will direct all of the
light back toward the laser. But if there is any difference between the lengths
of the two arms, some light will travel to where it can be recorded by a
photodetector.}

\emph{
%The space-time ripples cause the distance measured by a light beam to change as
the gravitational wave passes by. These changes are minute: just $10^{-16}$
centimeters, or one-hundred-millionth the diameter of a hydrogen atom over the
$2.5$ mile length of the arm. Yet, they are enough to change the amount of
light falling on the photodetector, which produces a signal defining how the
light falling on changes over time. LlGO requires at least two widely separated
detectors, operated in unison, to rule out false signals and confirm that a
gravitational wave has passed through the earth. Three interferometers were
built for LlGO -- two near Richland, Washington, and the other near Baton
Rouge, Louisiana.}
%
\begin{figure}
 \centerline{\epsfig{file=LISA.eps}} \caption{\label{LISA.fig}For
more details see http://lisa.nasa.gov/}
\end{figure}

\emph{
%
LIGO is the family of the largest existing Michelson interferometers, but just
wait for a few years until LISA (Laser Interferometer Space Antenna) - the
first space gravitational wave detector - is launched. LISA is essentially a
space-based Michelson interferometer: three spacecrafts will be arranged in an
approximately equilateral triangle. Light from the central spacecraft will be
sent out to the other two spacecraft. Each spacecraft will contain freely
floating test masses that will act as mirrors and reflect the light back to the
source spacecraft where it will hit a detector causing an interference pattern
of alternating bright and dark lines. The spacecrafts will be positioned
approximately 5 million kilometers from each other; yet it will be possible to
detect any change in the distance between two test masses down to 10 picometers
(about 1/10th the size of an atom)!
%
}

\end{document}
\newpage
