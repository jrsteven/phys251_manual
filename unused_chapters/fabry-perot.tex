%\chapter*{Fabry-Perot Interferometer and the  Sodium Doublet}
%\addcontentsline{toc}{chapter}{Fabry-Perot Interferometer}
\documentclass{article}
\usepackage{tabularx,amsmath,boxedminipage,epsfig}
    \oddsidemargin  0.0in
    \evensidemargin 0.0in
    \textwidth      6.5in
    \headheight     0.0in
    \topmargin      0.0in
    \textheight=9.0in

\begin{document}
\title{Fabry-Perot Interferometer and the  Sodium Doublet}
\date {}
\maketitle


\noindent
 \textbf{Experiment objectives}: Assemble and align Fabry-Perot interferometer,
 and use it to measure differential wavelength for the Na doublet.

 \section*{Theory}

\subsection*{The Fabry-Perot Interferometer}

Any interferometer relies on interference between two or more light field. In a Fabry-Perot configuration input
light field bounces between two closely spaced partially reflecting surfaces, creating a large number of
reflections. Interference of these multiple beams produces sharp spikes in the transmission for certain light
frequencies. Thanks to the large number of interfering rays, this type of interferometer has extremely high
resolution, much better than, for example, a Michelson interferometer. For that reason Fabry-Perot
interferometers are widely used in telecommunications, lasers and spectroscopy to control and measure the
wavelengths of light. In this experiment we will take advantage of high spectral resolution of the Fabry-Perot
interferometer to resolve two very closely-spaces emission lines in Na spectra by observing changes in a
overlapping interference fringes from these two lines.
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.8\linewidth]{./pdf_figs/pfig1}
\caption{\label{fpfig1}Sequence of Reflection and
Transmission for a ray arriving at the treated inner surfaces $P_1 \& P_2$.}
\end{figure}
\end{figure}

A Fabry-Perot interferometer consists of two parallel glass plates, flat to better than 1/4 of an optical
wavelength $\lambda$, and coated on the inner surfaces with a partially transmitting metallic layer. Such
two-mirror arrangement is normally called an {\it optical cavity}. The light in a cavity by definition bounces
back and forth many time before escaping; the idea of such a cavity is crucial for the construction of a laser.
Any light transmitted through such cavity is a product of interference between beams transmitted at each bounce
as diagrammed in Figure~\ref{fpfig1}. When the incident ray arrives at interface point $A$, a fraction $t$ is
transmitted and the remaining fraction $r$  is reflected, such that $t + r = 1$ ( this assumes no light is lost
inside the cavity). The same thing happens at each of the points $A,B,C,D,E,F,G,H\ldots$, splitting the initial
ray into parallel rays $AB,CD,EF,GH,$ etc. Between adjacent ray pairs, say $AB$ and $CD$, there is a path
difference of :
\begin{equation}
 \delta = BC+CK
\end{equation}%eq1
    where $BK$ is normal to $CD$.  In a development
similar to that used for the Michelson interferometer, you can show
that:
\begin{equation}
 \delta = 2d\cos\theta
\end{equation}%eq.2
     If this path difference produces
constructive interference, then  $\delta$  is some integer multiple of   ,
$\lambda$ namely,
\begin{equation}
  m\lambda = 2d\cos\theta %eq.3
\end{equation}%eq.3

This applies equally to ray pairs $CD$ and $EF, EF$ and $GH$, etc, so that all parallel rays to the right of
$P2$ will constructively interfere with one another when brought together.

Issues of intensity of fringes \& contrast between fringes and dark background
are addressed in Melissinos, {\it Experiments in Modern Physics}, pp.309-312.

\subsection*{The Sodium Doublet}

    In this lab you will measure the separation between the two
    famous ``sodium doublet'' lines, the two closely spaced lines
    which occur at 589 $nm$ and 589.59 $nm$, respectively. This ``doublet''
    emission is evidence that the atomic electron has the property
    of intrinsic angular momentum, or spin S.  As you are learning
    in Modern Physics 201, the discrete spectral lines in atomic
    emission are due to the quantization of electron energies in
    the atom. As Niels Bohr postulated, electrons in atoms are
    only allowed to absorb and emit energy in discrete
    quantities. When an electron moves from one orbit to another
    in an atom, a well-defined amount of energy is emitted as
    light at a fixed wavelength. Later in this class we will
    explore the spectra of various atomic gases.
\begin{figure}[h]
\centerline{\epsfig{height=5cm, file=fpfig2.eps}} \caption{\label{fpfig2.fig}Fine Structure Splitting in sodium
giving rise to the sodium doublet lines}
\end{figure}
    For many
     atoms, {\bf atomic levels are further split}, for example,
    by interactions of electrons with each other (Russell-Saunders
    coupling), external magnetic fields (Zeeman effect), and even
    the interaction between the spin of an electron and the
    magnetic field created by its orbital angular momentum
    (spin-orbit coupling). This is known as fine structure
    splitting (FSS). The fine structure splitting for the sodium
    3P state is due to spin-orbit coupling, and is illustrated in
    Figure \ref{fpfig2.fig}.  The "3P" state refers to sodium's
valence electron
    which has a principal quantum number of $n=3$ and an orbital
    quantum number of $l=1$ (a P-state). Further, the electron has
    an intrinsic spin (like a top), described by a spin quantum
    number $S$, which is either +1/2 or -1/2. The electron has a
    magnetic moment due to its intrinsic spin, given by $m_S$. Due to
its orbital angular momentum around a charged nucleus, it
    senses a magnetic field ${\mathbf H}$. The energy of interaction of a
    magnetic moment in a field is given by $E = -\mu\cdot {\mathbf H}$.
This gives
    rise to the splitting and two spectral emission lines.


\section*{Procedure}

\subsection*{Set Up}
\textbf{Equipment needed}: Pasco precision interferometry kit, Na lamp,
adjustable-hight platform.

\begin{figure}
\centerline{\epsfig{width=0.7\linewidth,file=fpfig3new.eps}} \caption{\label{fpfig3.fig}The Fabry-Perot
Interferometer}
\end{figure}

The interferometer layout is shown in Figure \ref{fpfig3.fig}. The inner spacing  $d$ between two
partially-reflecting mirrors ($P1$ and $P2$) can be roughly adjusted by loosening the screw that mounts $P2$ to
its base. It is important that the plates are as closely spaced as possible. Move the plates to within  $1.0 -
1.5$~mm of each other, but make sure the mirrors do not touch!

\subsection*{Data acquisition}

\begin{enumerate}
\item \textbf{Turn on the sodium lamp as soon as you arrive. It should warm up for about 20 minutes
before starting}.
\item Turn the micrometer close to or at 0.00.
\item Remove the diffuser sheet from in front of the lamp. Look through
plate $P2$ towards the lamp.  If the plates are not parallel, you will see
multiple images of the lamp filament. Adjust the knobs on $P1$ until the images
collapse into one. At this point, you should see faint circular interference
fringes appear.
\item Place the diffuser sheet in
front of the lamp so you will now only see the fringes. Continue to adjust the
knobs on one plate (not the knobs to move the plate back and forth, but the
ones to bring it parallel) to get the best fringe pattern. It is the most
convenient to view the interference picture directly.
\item
Seek the START condition illustrated in Fig.(\ref{fpfig4.fig}), in which all bright fringes are evenly spaced.
You do this by moving the micrometer.  Note that alternate fringes may be of somewhat different intensities, one
corresponding to fringes of  $\lambda_1$, the other to those for $\lambda_2$. If you do not see this condition,
try moving the mirror $P2$ across the range of micrometer screw. If you still cannot find them, you can also
move the whole plate by loosening one plate and sliding it a little.
\item  Practice going through the fringe conditions as shown in Fig.(\ref{fpfig4.fig})
by turning the micrometer and viewing the fringes. Do not be surprised if you
have to move the micrometer quite a bit to go back to the original condition.
\item Find a place on the micrometer ($d_1$) where you
have the ``START'' condition for fringes shown in Fig.(\ref{fpfig4.fig}). Now
advance the micrometer rapidly while viewing the fringe pattern ( NO COUNTING
OF FRINGES IS REQUIRED ).  Note how the fringes of one intensity are moving to
overtake those of the other intensity (in the manner of
Fig.(\ref{fpfig4.fig})).  Keep turning until the ``STOP'' pattern is achieved
(the same condition you started with).  Record the micrometer reading as $d_2$.
\emph{Remember that 1 tick mark is 1 micrometer ($10^{-6}m$). That means if you
read 1.24, your really move 124 $\mu m$.}
\end{enumerate}

\noindent \fbox{\parbox{\linewidth}{\textbf{Experimental tip:} You may have to
``home in'' on the best START and STOP conditions. Let's say that the even
fringe spacing for the START condition ($d_1$) is not exactly in view. Now move
the micrometer, looking to see if the pattern moves toward even spacing. If so,
stop and read the micrometer for $d_1$. Move a bit more. If this second fringe
spacing looks better than the first, then accept this for $d_1$. The same
``homing in'' procedure should be used to select the reading for $d_2$. In
other words as you approach the even spacing condition of the STOP pattern,
start writing down the micrometer positions. Eventually you will favor one
reading over all the others.}}

\section*{Analysis}

    Since the condition we are seeking above for ``START'' places
    the bright fringes of   $\lambda_1$ at the point of destructive
    interference for  $\lambda_2$, we can express this for the bull's eye
    center ($\theta= 0 $) as:
\begin{equation}
2d_1=m_1\lambda_1=\left(m_1+n+\frac{1}{2}\right)\lambda_2
\end{equation}

  Here the integer n accounts for the
    fact that   $\lambda_1 >  \lambda_2$ ,  and the  $1/2$  for the
condition of
    destructive interference for   $\lambda_2$  at the center. Since the
    net action of advancing by many fringes has been to increment
    the fringe count of   $\lambda_2$ by one more than that of
$\lambda_1$  ,
    then we express the ``STOP'' condition as:
\begin{equation}
2d_2=m_2\lambda_1=\left(m_2+n+\frac{3}{2}\right)\lambda_2
\end{equation}
   Subtracting the
    two interference equations gives us:
\begin{equation}
2(d_2-d_1)=(m_2-m_1)\lambda_1=(m_2-m_1)\lambda_2+\lambda_2
\end{equation}
 Eliminating  $(m_2-m_1)$
    in this equation we obtain:

\begin{equation}
2(d_2-d_1)=\frac{\lambda_1\lambda_2}{(\lambda_1-\lambda_2)}
\end{equation}

 Solving this for $\Delta \lambda = \lambda_1-\lambda_2$, and
    accepting as valid the approximation that  $\lambda_1\lambda_2\approx
\lambda^2$   ( where $\lambda$ is the
    average of $\lambda_1$  and $\lambda_2 \approx 589.26 nm$ ), we obtain:
\begin{equation}
\boxed{\Delta\lambda=\frac{\lambda^2}{2(d_2-d_1)}}
\end{equation}

Each lab partner should independently align the interferometer and make at least \textit{two} measurements of
``START'' and ``STOP'' positions. A sample table to record the data is shown below. \\{\large
\begin{tabular}{|p{27mm}|p{27mm}|p{27mm}|p{27mm}|}
\hline
 $d_1$ $\pm \dots$ & $d _2$ $\pm \dots$& $(d_1-d_2)$ $\pm \dots$&
$\Delta \lambda(nm) $ $\pm \dots$\\
\hline
&&&\\
\hline &&&\\ \hline &&&\\ \hline &&&\\ \hline &&&\\ \hline
\end{tabular}
}

\vspace{1cm} Calculate average value of Na doublet splitting and its standard deviation. Compare your result
with the established value of $\Delta \lambda_{Na}=0.598$~nm.


\begin{figure}[h]
\centerline{\epsfig{width=0.8\linewidth,file=fpfig4.eps}} \caption{\label{fpfig4.fig}The Sequence of fringe
patterns encountered in the course of the FSS measurements. Note false colors: in your experiment the background
is black, and both sets of rings are bright yellow.}
\end{figure}

\end{document}

\newpage
\noindent
Physics 251 Section:\\
\hskip 4.5in Name:\\
\hskip 4.5in Partners:\\
\vskip 0.5in
\subsection*{The Fabry-Perot Interferometer}
1. Briefly describe how the Fabry-Perot interferometer gives and interference
pattern (in one or two sentences):\\
\vskip 1.2in
2. How does the interferometer's resolving power of the fringes depend on the
reflectivity of plates, r ? That is, does the sharpness of the fringes increase
or decrease with r ? Consult Melissinos or Professor Kane's Mathview program.
(The reflectivity of the plates defines the {\it finess} of the cavity).\\
\vskip 1in.


{\large
\noindent
Fill in:

The sodium doublet lines arise because an atomic$\underline{\hskip 1.in}$
is split into two by$\underline{\hskip 1.in}$ coupling.
The electron has
intrinsic $\underline{\hskip 1.in}$, like a top, with values of
$\underline{\hskip 1.in}$ or$\underline{\hskip 1.in}$. Because of
this, the electron has in intrinsic magnetic$\underline{\hskip 1.in}$  and
has magnetic
energy in a magnetic field given by E=$\underline{\hskip 1.in}$
case comes from the electron's $\underline{\hskip 1.in}$ motion.

}

\subsection*{DATA:}
{\large
\begin{tabular}{|p{27mm}|p{27mm}|p{27mm}|p{27mm}|}
\hline
 $d_1$ $\pm \dots$ & $D _2$ $\pm \dots$& $(d_1-d_2)$ $\pm \dots$&
$\Delta \lambda(nm) $ $\pm \dots$\\
\hline
&&&\\
\hline
&&&\\ \hline
&&&\\ \hline
&&&\\ \hline
&&&\\ \hline
\end{tabular}
}
\vskip .2in
$\Delta \lambda=$\hskip 1.5in nm\\
\vskip .2in
Standard deviation= \hskip 1.5in nm
\newpage
\end{document}
