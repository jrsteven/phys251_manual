%\chapter*{Millikan Oil Drop Experiment}
%\addcontentsline{toc}{chapter}{Millikan Oil Drop Experiment}
\documentclass{article}
\usepackage{tabularx,amsmath,boxedminipage,epsfig}
    \oddsidemargin  0.0in
    \evensidemargin 0.0in
    \textwidth      6.5in
    \headheight     0.0in
    \topmargin      0.0in
    \textheight=9.0in

\begin{document}
\title{Millikan Oil Drop Experiment}
\date {}
\maketitle
\noindent

\textbf{Experiment objectives}: \\ \textbf{Week 1}: explore the experimental
apparatus and data
 acquisition procedure; develop the data analysis routing using a mock Millikan experiment. \\ \textbf{Week 2}: extract the value of a unit charge $e$ by observing the motion
 of charged oil drops in gravitational and electric field.

 \begin{boxedminipage}{\linewidth}
\textbf{Warning: this is a hard experiment!} \\
%
You have two class sessions to complete this experiment - for a good reason:
this experiment is very hard! After all, it took R. A. Millikan 10 years to
collect and analyze enough data to make accurate measurement of the electron
charge. It takes some (often considerable) time to learn how to use the
apparatus and get reliable data with it, so make sure you take take good notes
during the first session on what gives you good and bad results. Also prepare
and debug all the data analysis routines (such as calculations of drop
parameters from the velocity measurements). Then you will hopefully have enough
time
 to make reliable measurements during the second session.
\end{boxedminipage}

    \section*{Introduction and Theory}
The electric charge carried by a particle may be calculated by measuring the
force experienced by the particle in an electric field of known strength.
Although it is relatively easy to produce a known electric field, the force
exerted by such a field on a particle carrying only one or several excess
electrons is very small. For example, a field of $1000$~Volts per cm would
exert a force of only $1.6\cdot l0^{-14}$~N dyne on a particle bearing one
excess electron. This is a force comparable to the gravitational force on a
particle with a mass of $l0^{-l2}$~gram.

The success of the Millikan Oil Drop experiment depends on the ability to
measure forces this small. The behavior of small charged droplets of oil,
having masses of only $l0^{-l2}$~gram or less, is observed in a gravitational
and an electric field. Measuring the velocity of fall of the drop in air
enables, with the use of Stokes� Law, the calculation of the mass of the drop.
The observation of the velocity of the drop rising in an electric field then
permits a calculation of the force on, and hence, the charge carried by the oil
drop.

Consider the motion of a small drop of oil inside the apparatus shown in Fig.
\ref{moplates}.
\begin{figure}[h]
\centerline{\epsfig{width=3in, file=modexp.eps}} \caption{\label{moplates}
Schematic Millikan Oil Drop System with and without electric field.}
\end{figure}


Because of the air drag tiny droplets fall very slowly with some constant
terminal velocity $v_f$:
\begin{equation}\label{fall}
mg=kv_f
\end{equation}
where  $q$ is the charge on the droplet, $m$ is the mass of the droplet, $g$ is
the acceleration due to gravity, and $k$ is a drag coefficient which will be
related to the viscosity of air and the radius of the droplet.

Because of its small mass the motion of the droplets is sensitive to an
external electric field $E$ even if they carry charges of only a few electrons.
A sufficient electric field can cause the oil drop to rise with a constant
velocity $v_r$, such that:
\begin{equation}\label{rise}
Eq=mg+kv_r
\end{equation}
Combining Eqs.~(\ref{rise},\ref{fall}) we can find the charge $q$:
\begin{equation}\label{q}
q=\frac{mg(v_f+v_r)}{Ev_f}
\end{equation}

Therefor, the charge of the droplet can be found by measuring its terminal
velocity $v_t$ and rising velocity in the external magnetic field $v_r$.
However, we also need to know the mass and the radius of a drop. These data has
to be extracted from the same data. The drag coefficient, $k$, can be
determined from the viscosity, $\eta$, and the radius of the droplet, $a$,
using Stokes law:
\begin{equation}
k=6\pi a\eta
\end{equation}
The mass of a drop can be related to its radius:
\begin{equation}\label{m}
m=\frac{4}{3}\pi a^3 \rho,
\end{equation}
and one may solve for $a$ using Eq.~(\ref{fall}):
\begin{equation}\label{simple}
a=\sqrt{\frac{9\eta v_f}{2g\rho}}
\end{equation}
Here $\rho=.886\cdot 10^3 \mathrm{kg/m}^3$ is the density of the oil.

The air viscosity at room temperature is $\eta=1.832\cdot 10^{-5}$Ns/m$^2$ for
relatively large drops. However, there is a small correction for this
experiment for a small drops because the oil drop radius is not so different
from the mean free path of air. This leads to an effective viscosity:
\begin{equation}\label{etaeff}
\eta_{eff}=\eta\frac{1}{1+\frac{b}{Pa}}
\end{equation}
where $b\approx 8.20 \times 10^{-3}$ (Pa$\cdot$m) and $P$ is atmospheric
pressure (1.01 $10^5$ Pa). The idea here is that the effect should be related
to the ratio of the mean free path to the drop radius. This is the form here
since the mean free path is inversely proportional to pressure. The particular
numerical constant can be obtained experimentally if the experiment were
performed at several different pressures. A feature Milikan's apparatus had,
but ours does not.

To take into the account the correction to the air viscosity, one has to
substitute the expression for $\eta_{eff}$ of Eq.~(\ref{etaeff})  into Eq.~(
\ref{simple}) and then solve this more complex equation for $a$:
\begin{equation}\label{complex}
a=\sqrt{\frac{9\eta v_f}{2g\rho}+\left(\frac{b}{2P}\right)^2}-\frac{b}{2P}
\end{equation}

Therefor, the calculation of a charge carried by an oil drop will consists of
several steps:
\begin{enumerate}
\item Measure the terminal velocities for a particular drop with and without
electric field.
\item Using the falling terminal velocity with no electric field, calculate
the radius of a droplet using Eq.~(\ref{complex}), and then find the mass of
the droplet using Eq.~(\ref{m}).
\item Substitute the calculated parameters of a droplet into Eq.~(\ref{q}) to
find the charge of the droplet $q$.

\end{enumerate}
%This second approach leads to:
%
%
%Having found $a$ one can then find $m$ using Eq. \ref{m} and then find
%$q$ from Eq. \ref{q}. I would use this approach rather than the Pasco one.




\section*{Experimental procedure}

\subsection*{Mock Millikan experiment - practice of the data analysis}
\textit{The original idea of this experiment is described here:
http://phys.csuchico.edu/ayars/300B/handouts/Millikan.pdf}

The goal of this section is to develop an efficient data analysis routine for
the electron charge measurements. You will be given a number of envelopes with
a random number of Unidentified Small Objects (USOs), and your goal is to find
a mass of a single USO (with its uncertainty!) without knowing how many USOs
each envelope has. This exercise is also designed to put you in Robert Millikan
shoes (minus the pain of data taking).

Each person working on this experiment will be given a number of envelopes to
weight. Each envelope contain unknown number of USO plus some packing material.
To save time, all the data will be then shared between the lab partners.

Then analyze these data to extract the mass of a single USO and its uncertainty
in whatever way you�d like. For example, graphs are generally useful for
extracting the data - is there any way to make a meaningful graph for those
measurements? If yes, will you be able to extract the mean value of USO mass
and its uncertainty from the graph? \textit{Feel free to discuss your ideas
with the laboratory instructor!}

After finding the mass of a USO, work with your data to determine how the size
of the data set affects the accuracy of the measurements. That will give you a
better idea how many successful measurements one needs to make to determing $e$
in a real Millikan experiment.

This part of the experiment must be a part of the lab report, including the
results of your measurements and the description of the data and error analysis
routine.



\subsection*{Pasco Millikan oil drop setup}

Follow the attached pages from Pasco manual to turn on, align and control the
experimental apparatus. Take time to become familiar with the experimental
apparatus and the measurement procedures. Also, it is highly recommended that
you develop an intuition about ``acceptable'' drops to work with (see Pasco
manual, ``Selection of the Drop'' section).

\subsection*{Data acquisition and analysis}

\begin{itemize}

\item Choose a ``good'' drop and make about 10 measurements for its fall and rise
velocities $v_t$ and $v_r$ by turning the high voltage on and off. Try to find
a drop that does not rise too quickly for it will likely have a large number of
electrons and, further, it will be difficult to determine the $v_r$. If you
can't find slow risers, then lower the voltage so as to get better precision.


\item Calculate the charge on the droplet. If the result of this first
determination for the charge on the drop is greater than 5 excess electron, you
should use slower moving droplets in subsequent determinations. Accepted value
of the electron charge is $e=1.6\times10^{-19}$~C.

\item If the drop is still within viewing range, try to change its charge. To
do that bring the droplet to the top of the field of view and move the
ionization lever to the ON position for a few seconds as the droplet falls. If
the rising velocity of the droplet changes, make as many measurements of the
new rising velocity as you can (10 to 20 measurements). If the droplet is still
in view, attempt to change the charge on the droplet by introducing more alpha
particles, as described previously, and measure the new rising velocity 10�20
times, if possible. Since making measurements with the same drop with changing
charge allows does not require repeating calculations for the drop mass and
radius, try ``recharging'' the same drop as many times as you can.

\item Be sure to measure the separation $d$ between the electrodes and the voltage potential in order to
determine the field from the voltage.

\end{itemize}

Each lab partner should conduct measurements for at least one drop, and the
overall number of measurements should be sufficient to make a reliable
measurement for the unit electron charge. Make a table of all measurements,
identify each drop and its calculated charge(s). Determine the smallest charge
for which all the charges could be multiples of this smallest charge. Estimate
the error in your determination of $e$.

% Answer these questions somewhere in your report:
%
%\begin{enumerate}
%\item You will notice that some drops travel upward and others downward
%   in the applied field. Why is this so? Why do some drops travel
%   very fast, and others slow?
%\item Is the particle motion in a straight line? Or, do you notice that
%   the particle "dances" around ever so slightly? This is due to
%   Brownian motion: the random motion of a small particle in a gas or
%   fluid.
%
%
%\item We made three assumptions in determining the charge from Equation 1
%   above. What are they? Hint: They are related to Stoke's Law.
%
%
%\item Would you, like Millikan, spend 10 years on this experiment?
%
%\end{enumerate}
%
%Extra credit:  Millikan and his contemporaries were only able to
%measure integer values of electron charge (as you are). Has anyone
%measured free charges of other than integer multiples of e?

\end{document}
