%\chapter*{Spectrum of Sodium }
%\addcontentsline{toc}{chapter}{Spectrum of Sodium}

\documentclass{article}
\usepackage{tabularx,amsmath,boxedminipage,epsfig}
    \oddsidemargin  0.0in
    \evensidemargin 0.0in
    \textwidth      6.5in
    \headheight     0.0in
    \topmargin      0.0in
    \textheight=9.0in

\begin{document}
\title{Spectrum of Sodium}
\date {}
\maketitle \noindent
 \textbf{Experiment objectives}: measure the energy spectrum of sodium (Na),
 determine values of quantum defects of low angular momentum states, and measure fine splitting
using Na yellow doublet.

\section*{Theory}
Sodium (Na) belongs to the chemical group of \emph{alkali metals}, together
with lithium (Li), potassium (K), rubidium (Rb), cesium (Cs) and Francium (Fr).
All elements of this group have a closed electron shell with one extra unbound
electron. This makes energy level structure for this free electron to be very
similar to that of hydrogen, as shown in Fig.~\ref{nae}.

For example, a Na atom has 11 electrons, and its electronic configuration is
$1s^22s^22p^63s$, as determined from the Pauli exclusion principle.  Ten
closed-shell electrons effectively screen the nuclear charge number ($Z=11$) to
an effective charge $Z^*\approx 1$, so that the $3s$ valent electron experience
the electric field potential similar to that of a hydrogen atom. As a result,
the electron spectrum of all alkali metal atoms is quite similar to that of
hydrogen:
\begin{equation}\label{Hlevels_Naexp}
E_n=-hcRy\frac{1}{n^2}
\end{equation}
%
where $n$ is the principle quantum number, and $Ry=\frac{2\pi
m_ee^4}{(4\pi\epsilon_0)^2ch^3}$   is  the  Rydberg constant ($Ry = 1.0974
\times 10^5 cm^{-1}$ and $hcRy = 13.605 eV$). For each particular value of
angular momentum $l$ the energy spectrum follows the same scaling as hydrogen
atom. However, the absolute values of energies obey Eq.(\ref{Hlevels_Naexp})
only for electron energy states with orbits far above closed shell - the ones
with large value of an angular momentum $l$. Electron with smaller $l$ spends
more time closer to the nuclear, and ``feels'' stronger bounding electrostatic
potential. As a result the corresponding energy levels are pulled down compare
to those of hydrogen, and the states with the same principle number $n$ but
different angular momenta $l$ are split (\emph{i.e.} have different energies).
\begin{figure}
\includegraphics[height=\columnwidth]{nae.eps}
\caption{\label{nae}Energy spectrum of Na. The energy states of H are shown in
far right for comparison.}
\end{figure}

For each particular value of angular momentum $l$ the energy spectrum follows
the same scaling as hydrogen atom, but with an effective charge $Z^*$:
\begin{equation}\label{heq}
E_n=-\frac{1}{2}\frac{Z^{*2}e^4}{(4\pi\epsilon_0)^2}\frac{mc^2}{\hbar^2c^2}
\frac{1}{n^2}=-Z^{*2}\frac{hcRy}{n^2}
\end{equation}
The value of the effective charge $Z^*$ depends on the angular momentum $l$,
and does not vary much between states with different principle quantum numbers
$n$ but same $l$\footnote{The accepted notation for different electron angular
momentum states is historical, and originates from the days when the proper
quantum mechanical description for atomic spectra has not been developed yet.
Back then spectroscopists had categorized atomic spectral lines corresponding
to their appearend: for example any spectral lines from electron transitions
from $s$-orbital ($l=0$) appeared always \textbf{S}harp on a photographic film,
while those with initial electron states of $d$-orbital ( $l=2$) appeared
always \textbf{D}iffuse. Also spectral lines of \textbf{P}rinciple  series
(initial state is $p$-orbital, $l=1$) reproduced the hydrogen spectrum most
accurately (even though at shifted frequencies), while the \textbf{F}undamental
(initial electron state is $f$-orbital, $l=3$) series matched the absolute
energies of the hydrogen most precisely. The orbitals with higher value of the
angular momentum are denoted in an alphabetic order ($g$,
$h$, \textit{etc}.) }:\\
\begin{tabular}{ll}
States&$Z^*$\\
s~($l=0$)&$\approx$ 11/9.6\\
p~($l=1$)&$\approx$ 11/10.1\\
d~($l=2$)&$\approx$ 1\\
f~($l=3$)&$\approx $ 1\\
\end{tabular}
\\These numbers mean that two states with the lowest angular momentum ($s$ and
$p$) are noticeably affected by the more complicated electron structure of Na,
while the energy levels of the states with the higher values of angular
momentum ($d$, $f$) are identical to the hydrogen energy spectrum.

An alternative (but equivalent) procedure is to assign a {\it quantum defect}
to the principle quantum  $n$ instead of introducing an effective nuclei
charge. In this case Eq.(\ref{heq}) can be written as:
\begin{equation}\label{qdef}
E_n=-\frac{hcRy}{(n^*)^2}=-\frac{hcRy}{(n-\Delta_l)^2}
\end{equation}
where $n*=n-\Delta_l$, and $\Delta_l$  is the corresponding quantum defect.
Fig. \ref{nadell} shows values of quantum defects which work approximately for
the alkalis. One sees that there is one value for each value of the angular
momentum $l$. This is not exactly true for all alkali metals, but for Na there
is very little variation in $\Delta_l$ with $n$ for a given $l$.

\begin{figure}
\includegraphics[width=0.5\columnwidth]{nadell.eps}
\caption{\label{nadell}Quantum Defect $\Delta_l$ versus $l$ for different
alkali metals. Taken from Condon and Shortley p. 143}
\end{figure}
%\begin{figure}
%\includegraphics[height=3in]{nadel.eps}
%\caption{\label{nadel}Quantum Defect $\Delta_l$ variation with $n$. The
%difference between the quantum defect of each term and that of the lowest term
%of the series to which it belongs is plotted against the difference between
%the total quantum numbers of these terms. Again from Condon and Shortley p. 144.}
%\end{figure}

The spectrum of Na is shown in Fig. \ref{nae}. One can immediately see that
there are many more optical transitions because of the lifted degeneracy of
energy states with different angular momenta. However, not all electronic
transition are allowed: since the angular momentum of a photon is $1$, then the
electron angular momentum cannot change by more than one while emitting one
spontaneous photon. Thus, it is important to remember the following
\emph{selection rule} for atomic transitions:
\begin{equation}\label{selrules}
\Delta l = \pm 1.
\end{equation}
According to that rule, only transitions between two ``adjacent'' series are
possible: for example $p \rightarrow s$ or $d \rightarrow p$ are allowed, while
$s \rightarrow s$ or $s \rightarrow d$ are forbidden.

The strongest allowed optical transitions are shown in Fig. \ref{natrns}.
\begin{figure}
\includegraphics[height=\columnwidth]{natrans.eps}
\caption{\label{natrns}Transitions for Na. The wavelengths of selected
transition are shown in {\AA}. Note, that $p$ state is now shown in two
columns, one referred to as $P_{1/2}$ and the other as $P_{3/2}$. The small
difference between their energy levels is the ``fine structure''.}
\end{figure}
%\begin{figure}
%\includegraphics[height=4in]{series.eps}
%\caption{\label{series}Series for Hydrogen, Alkalis are similar.}
%\end{figure}
Note that each level for given $n$ and $l$ is split into two because of the
\emph{fine structure splitting}. This splitting is due to the effect of
electron \emph{spin} and its coupling with the angular momentum. Proper
treatment of spin requires knowledge of quantum electrodynamics and solving
Dirac equation; for now spin can be treated as an additional quantum number
associated with any particle. The spin of electron is $1/2$, and it may be
oriented either along or against the non-zero electron's angular momentum.
Because of the weak coupling between the angular momentum and spin, these two
possible orientation results in small difference in energy for corresponding
electron states.

\section*{Procedure and Data Analysis}
Align a diffraction-grating based spectrometer as described in ``Atomic
Spectroscopy of Hydrogen Atoms'' experimental procedure.

Then determine the left and right angles for as many spectral lines  and
diffraction orders as possible. Each lab partner should measure the postilions
of all lines at least once.

Reduce the data using Eq. \ref{nlambda} to determine wavelengths for each
spectral line (here $m$ is the order number):
\begin{equation}\label{nlambda}
m\lambda=\frac{d}{2}(\sin\theta_r+\sin\theta_l)
\end{equation}
Determine  the wavelengths of eight Na spectral lines measured in both first
and second order. Combining first and second order results obtain the mean and
standard deviation (error) of the mean value of the wavelength for each line.
Compare these measured mean wavelengths to the accepted values given in
Fig.~\ref{natrns} and in the table below:

\begin{tabular}{lll}
  Color&Line$_1$(\AA)&Line$_2$(\AA)\\
Red&6154.3&6160.7\\
Yellow & 5890.0&5895.9\\
Green & 5682.7&5688.2\\
&5149.1&5153.6\\
& 4978.6&4982.9\\
Blue&4748.0&4751.9\\
&4664.9&4668.6\\
Blue-Violet&4494.3&4497.7\\
\end{tabular}

Line$_1$ and Line$_2$ corresponds to transitions to two fine-spitted $3p$
states $P_{1/2}$ and $P_{3/2}$. These two transition frequencies  are very
close to each other, and to resolve them with the spectrometer the width of the
slit should be very narrow. However, you may not be able to see some weaker
lines then. In this case you should open the slit wider to let more light in
when searching for a line. If you can see a spectral line but cannot resolve
the doublet, record the reading for the center of the spectrometer line, and
use the average of two wavelengthes given above.

 Identify  at least seven of the lines with a particular transition, e.g.
$\lambda = 4494.3${\AA} corresponds to $8d \rightarrow 3p$ transition.

\subsection*{Calculation of a quantum defect for $n=3, p$ state}
Identify spectral lines which corresponds to optical transitions from $d$ to
$n=3,p$ states. Since the energy states of $d$ series follows the hydrogen
spectra almost exactly, the wavelength of emitted light $\lambda$ is given by:
\begin{equation}
\frac{hc}{\lambda}=E_{nd}-E_{3p}=-\frac{hcRy}{n^2}+\frac{hcRy}{(3-\Delta_p)^2},
\end{equation}
or
\begin{equation}
\frac{1}{\lambda}=\frac{Ry}{(3-\Delta_p)^2}-\frac{Ry}{n^2},
\end{equation}
 where $n$ is the principle number of the initial $d$ state. To verify this
expression by plotting $1/\lambda$ versus $1/n^2$ for the $n$= 4,5, and 6. From
the slope of this curve determine the value of the Rydberg constant $Ry$. From
the intercept determine the energy $E_{3p}$ of the $n=3,p$ state, and calculate
its quantum defect $\Delta_p$.
\subsection*{Calculation of a quantum defect for $s$ states}
Now consider the transition from the $s$-states ($n=5,6,7$) to to the $n=3, p$
state. Using $hc/\lambda=E_{ns}-E_{3p}$ and the results of your previous
calculations, determine the energies $E_{sn}$ for different $s$ states with
$n=5,6,7$  and calculate $\Delta_s$. Does the value of the quantum defect
depends on $n$?

Compare the results of your calculations for the quantum defects $\Delta_s$ and
$\Delta_p$ with the accepted values given in Fig. \ref{nadell}.

\subsection*{Calculations of fine structure splitting}
For the Na D doublet measure the splitting between two lines
$\Delta\lambda=\lambda_{3/2}-\lambda_{1/2}$ in the second diffraction order
(why the second order is better than the first one?). Compare to the accepted
value: $\Delta\lambda=$5.9\AA . Compare this approach to the use of the
Fabry-Perot interferometer.

\end{document}
\newpage
