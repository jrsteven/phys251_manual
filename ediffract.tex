\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Electron Diffraction}

\textbf{Experiment objectives}:  observe diffraction of the beam of electrons on a graphitized carbon target and  calculate the intra-atomic spacings in the graphite.

\section*{History}

    A primary tenet of quantum mechanics is the wavelike
    properties of matter. In 1924, graduate student Louis de
    Broglie suggested in his dissertation that since light has
    both particle-like {\bf and} wave-like properties, perhaps all
    matter might also have wave-like properties. He postulated
    that the wavelength of objects was given by $\lambda = h/p$, where $h$ is Planck's constant and $p =
    mv$ is the momentum.  {\it This was quite a revolutionary idea},
    since there was no evidence at the time that matter behaved
    like waves. In 1927, however, Clinton Davisson and Lester
    Germer discovered experimental proof of the wave-like
    properties of matter --- particularly electrons. This discovery
    was quite by mistake: while studying electron reflection
    from a nickel target, they inadvertently crystallized their
    target, while heating it, and discovered that the scattered
    electron intensity  as a function of scattering angle showed
    maxima and minima. That is, electrons were ``diffracting'' from
    the crystal planes much like light diffracts from a grating,
    leading to constructive and destructive interference. Not only
    was this discovery important for the foundation of quantum
    mechanics (Davisson and Germer won the Nobel Prize for their
    discovery), but electron diffraction is an extremely important
    tool used to study new materials.  In this lab you will study
    electron diffraction from a graphite target, measuring the
    spacing between the carbon atoms.


\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{./pdf_figs/ed1_new} \caption{\label{ed1}Electron
Diffraction from atomic layers in a crystal.}
\end{figure}
\section*{Theory}
    Consider planes of atoms in a {\bf crystal} as shown in Fig.~\ref{ed1}
separated by distance $d$. Electron ``waves'' reflect from each of these planes.
Since the electron is wave-like, the combination of the reflections from each
interface produces to an interference pattern. This is completely analogous to
light interference, arising, for example, from different path lengths in the
Fabry-Perot or Michelson interferometers. The de Broglie wavelength for the
electron is given by $\lambda=h/p$, where $p$ can be calculated by knowing the
energy of the electrons when they leave the ``electron gun'':
\begin{equation}\label{Va}
\frac{1}{2}mv^2=\frac{p^2}{2m}=eV_a,
\end{equation}
 where $V_a$ is the accelerating potential.  The condition for constructive
interference is that the path length difference for the two waves
shown in Fig. \ref{ed1} be a multiple of a wavelength. This leads to Bragg's
Law:
\begin{equation}\label{bragg}
n\lambda=2d\sin\theta
\end{equation}
where $n = 1,2,\dots$ is integer. In this experiment, only the first order
diffraction $n=1$ is observed. Therefore, the intra-atomic distance in a
crystal can be calculated by measuring the angle of electron diffraction and
their wavelength (\emph{i.e.} their momentum):
\begin{equation}\label{bragg1}
d=\frac{\lambda}{2\sin\theta} = \frac{1}{2\sin\theta}\frac{h}{\sqrt{2em_eV_a}}
\end{equation}
\noindent
where $h$ is Planck's constant, $e$ is the electronic charge, $m_e$ is the
electron's mass, and $V_a$ is the accelerating voltage.
%
%
%
%
%     Knowing $\lambda$ and the angles $\theta$ for which
%constructive interference occurs, the atomic spacing $d$ can be
%extracted.

\section*{Experimental Procedure}

\textbf{Equipment needed}: Electron diffraction apparatus and power supply, ruler and thin receipt paper or masking tape.

\subsection*{Safety}
You will be working with high voltage. The power supply will be connected for you,
but inspect the apparatus when you arrive \textbf{before} turning the power on.
If any wires are unplugged, ask an instructor to reconnect them. Also,
\textbf{before} turning the power on, identify the high voltage contacts on the
electron diffraction tube, make sure these connections are well-protected and
cannot be touched by accident while taking measurements.
\begin{figure}[h]
\centering
\includegraphics[width=6in]{./pdf_figs/ed2} \caption{\label{ed2}Electron Diffraction Apparatus.}
\end{figure}
\subsection*{Setup}

The diagram of the apparatus is given in Fig.\ref{ed2}. An electron gun
(consisting of a heated filament to boil electrons off a cathode and an anode
to accelerate them, similar to the e/m experiment) ``shoots'' electrons at a
carbon (graphite) target.

The electrons diffract from the carbon target and the resulting interference
pattern is viewed on a phosphorus screen.

 The graphitized carbon is not completely crystalline but consists of crystal
sheets in random orientations. Therefore, the constructive interference
patterns will be seen as bright circular rings. For the carbon target, two
rings (an outer and inner, corresponding to different crystal planes) will be
seen, corresponding to two spacings between atoms in the graphite arrangement
(see Fig.~\ref{ed3}).
\begin{figure}
\centering
\includegraphics[width=4in]{./pdf_figs/ed3} \caption{\label{ed3}Spacing of
carbon atoms. Here subscripts \textit{10} and \textit{11} correspond to the
crystallographic directions in the graphite crystal.}
\end{figure}

\subsection*{Data acquisition}
Acceptable power supply settings:
\\\begin{tabular}{lll}
Filament Voltage& $V_F$&6.3 V ac/dc (8.0 V max.)\\
Anode Voltage & $V_A$& 1500 - 5000 V dc\\
Anode Current & $I_A$& 0.15 mA at 4000 V ( 0.20 mA max.)
\end{tabular}

\begin{enumerate}
\item  Switch on the heater and wait one minute for the oxide cathode to achieve thermal stability.
\item  Slowly increase $V_a$ until you observe two rings appear around the direct beam.
Slowly change the voltage and determine the highest achievable accelerating
voltage, and the lowest voltage when the rings are visible.
\item  Measure the diffraction angle $\theta$ for both inner and outer rings for 5-10 voltages from that range,
using the same thin receipt paper (see procedure below). Each lab partner should
repeat these measurements (using an individual length of the thin paper).
\item Calculate the average value of $\theta$ from the individual measurements for each
voltage $V_a$. Calculate the uncertainties for each $\theta$.
\end{enumerate}

\textbf{Measurement procedure for the diffraction angle $\theta$}

To determine the crystalline structure of the target, one needs to carefully
measure the diffraction angle $\theta$. It is easy to see (for example, from
Fig.~\ref{ed1}) that the diffraction angle $\theta$ is 1/2 of the angle
between the beam incident on the target and the diffracted beam to a ring,
hence the $2\theta$ appearing in Fig.~\ref{ed4}. You are going to determine
the diffraction angle $\theta$ for a given accelerated voltage from the
approximate geometrical ratio
\begin{equation}
L\sin{2\theta} = R\sin\phi,
\end{equation}
where the distance between the target and the screen $L = 0.130$~m is controlled during the production process to have an accuracy better than 2\%. {\it Note, this means that the electron tubes are not quite spherical.}

The ratio between the arc length $s$ and the
radius of the curvature for the screen $R = 0.066$~m gives the angle $\phi$ in
radians:  $\phi = s/2R$. To measure $\phi$ carefully place a piece of
thin receipt paper on the tube so that it crosses the ring along the diameter.
Mark the position of the ring for each accelerating voltage, and then
remove the paper and measure the arc length $s$ corresponding to
each ring. You can also make these markings on masking tape placed gently on the tube.


\begin{figure}
\centering
\includegraphics[width=4in]{./pdf_figs/edfig4} \caption{\label{ed4}Geometry of the experiment.}
\end{figure}

\section*{Data analysis}

Use the graphical method to find the average values for the distances between
the atomic planes in the graphite crystal $d_{11}$ (outer ring) and $d_{10}$
(inner ring). To determine the combination of the experimental parameters that
is proportional to $d$, one needs to substitute the expression for the
electron's velocity  Eq.(\ref{Va}) into the diffraction condition given by
Eq.(\ref{bragg}):
\begin{equation}\label{bragg.analysis}
\sin\theta=\frac{h}{2\sqrt{2m_ee}}\frac{1}{d}\frac{1}{\sqrt{V_a}}
\end{equation}

Make a plot of $\sin\theta$ (y-axis, with uncertainty) vs $1/\sqrt{V_a}$ (x-axis) for the inner and outer rings
(both curves can be on the same graph). Fit the linear dependence and measure
the slope for both lines. From the values of the slopes find the distances
between atomic layers $d_{inner}$ and $d_{outer}$.

Compare your measurements to the accepted values: $d_{inner}=d_{10} = .213$~nm
and $d_{outer}=d_{11}=0.123$~nm.

\section*{Seeing with Electrons}
	
The resolution of ordinary optical microscopes
is limited (the diffraction limit) by the wavelength of light ($\approx$ 400
nm). This means that we cannot resolve anything smaller than this by looking at
it with light (even if we had no limitation on our optical instruments). Since
the electron wavelength is only a couple of angstroms ($10^{-10}$~m), with
electrons as your ``light source'' you can resolve features to the angstrom
scale. This is why ``scanning electron microscopes'' (SEMs) are used to look at
very small features. The SEM is very similar to an optical microscope, except
that ``light'' in SEMs is electrons and the lenses used are made of magnetic
fields, not glass.

%\begin{tabular}{lll}
%Filament Voltage& $V_F$&6.3 V ac/dc (8.0 V max.)\\
%Anode Voltage & $V_A$& 2500 - 5000 V dc\\
%Anode Current & $I_A$& 0.15 mA at 4000 V ( 0.20 mA max.)
%\end{tabular}
%
%\newpage
%\noindent
%Section: $\underline{\hskip 1in}$\\
%Name: $\underline{\hskip 1in}$\\
%Partners: $\underline{\hskip 1in}$\\
%$\hskip 1in\underline{\hskip 1in}$\\
%\section*{Electron Diffraction}
%Step through the electron accelerating potential from 5 kV to 2 kV in
%steps of 0.5 kV. Record $V_a$ (from needle reading on power supply) and
%measure the diameter of the two rings. In measuring the diameter,
%either try to pick the middle of the ring, or measure the inside and
%outside of that  ring
%and average. Each lab partner should measure the diameter, and then average
%the result. {\bf Make sure you give units in the following tables}
%
%\begin{center}
%{\bf Final data -- fill out preliminary chart on next page first}
%\end{center}
%
%\begin{tabular}{|p{20mm}|p{20mm}|p{20mm}|p{20mm}|p{20mm}|}\hline
%$V_a$&(inner s)&$d_{inner}$&outer s &$d_{outer}$\\\hline
%&&&&\\\hline
%&&&&\\\hline
%&&&&\\\hline
%&&&&\\\hline
%&&&&\\\hline
%&&&&\\\hline
%\multicolumn{2}{|l|}{Average $d_{inner}\pm\sigma=$}
%&\multicolumn{2}{|l}{Average $d_{outer}\pm\sigma=$}
%&\multicolumn{1}{l|}{}\\\hline
%\end{tabular}
%
%
%
%
%Make a plot of $1/sqrt{V_a}$ versus $s$ for the inner and outer rings
%{both curves can be on the same graph).
%
%Compare your measurements to the known spacing below.$d_{outer}=$\\
%True values are: $d_{inner}=.213 nm$ and $d_{outer}=0.123 nm $.
%
%\begin{boxedminipage}{\linewidth}
%
%
%Turn in this whole stapled report, including your data tables. Attach your
%plot(s) at the end.
%\end{boxedminipage}
%\newpage
%\begin{boxedminipage}{\linewidth}
%Note: start with the External Bias at 30 V. Decrease the bias if you need
%to, to increase the intensity of the rings. DO NOT EXCEED 0.2 mA on ammeter!
%\end{boxedminipage}

%\newpage

\end{document}

