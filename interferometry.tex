\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Optical Interferometry}

\noindent
 \textbf{Experiment objectives}: Assemble and align Michelson and Fabry-Perot
interferometers, calibrate them using a laser of known wavelength, and then use them characterize the bright yellow emission line in sodium (Na).

\section*{Introduction}

Optical interferometers are the instruments that rely on the interference of two or more superimposed reflections of the input laser beam. These are one of the most common optical tools, and are used for precision measurements, surface diagnostics, astrophysics, seismology, quantum information, etc. There are many configurations of optical interferometers, and in this lab you will become familiar with two of the more common setups.

The \textbf{Michelson interferometer}, shown in Fig.~\ref{fig1mich.fig}, is based on the interference of two beams: the initial light is split into two arms on a beam splitter, and then these resulting beams are reflected and recombined on the same beamsplitter again. The difference in optical paths in the two arms leads to a changing relative phase of two beams, so when overlapped the two light fields will interfere constructively or destructively.
\begin{figure}[h]
\centering
\includegraphics[width=0.65\linewidth]{./pdf_figs/michelson.png} \caption{\label{fig1mich.fig}A Michelson Interferometer setup.}
\end{figure}
Such an interferometer was first used by Michelson and Morley in 1887 to determine that electromagnetic waves propagate in vacuum, giving the first strong evidence against the theory of a \textit{luminiferous aether} (a fictitious medium for light wave propagation) and providing insight into the true nature of electromagnetic radiation. Michelson interferometers are widely used in many areas of physics and engineering, including the recent discovery of gravitational waves at the LIGO facility (see Sect.~\ref{LIGO} at the end of this lab for more information).

Figure~\ref{fig1mich.fig} shows the traditional setting for a Michelson
interferometer. A beamsplitter (a glass plate which is partially
silver-coated on the front surface and angled at 45 degrees) splits the
laser beam into two parts of equal amplitude. One beam (that was initially
transmitted by the beamsplitter) travels to a moveable mirror $M_1$ and back
again. One-half of this amplitude is then reflected from the
partially-silvered surface and directed at 90 degrees toward the observer
(you will use a viewing screen). At the same time the second beam
(reflected by the beamsplitter) travels at 90 degrees toward an adjustable mirror $M_2$
and back. Since this beam travels through the glass beamsplitter
plate 3 times, its optical path length is longer than the first beam which only passes through the beamsplitter 1 time.  To
compensate for that, the first beam passes twice through a clear glass plate called the compensator plate, that has the same thickness.  At the beamsplitter one-half of this light is transmitted to an observer, overlapping with the beam reflected by $M_2$, and the total amplitude of the light at the screen is a combination of amplitude of  the two beams:
\begin{equation}
\mathbf{E}_{total} =  \mathbf{E}_1 + \mathbf{E}_2 = \frac12 \mathbf{E}_0 + \frac12 \mathbf{E}_0\cos(k\Delta l)
\end{equation}
Here $k\Delta l$ is a phase shift (``optical delay'') between the two wavefronts caused by the difference in
optical path lengths  for the two beams $\Delta l$ , $k=2\pi n/\lambda$ is the wave number, $\lambda$ is the
wavelength of light in vacuum, and $n$ is the refractive index of the optical medium (in our case - air).

Mirror $M_1$ is mounted on a precision traveling platform. Imagine that we adjust its position (by turning the micrometer screw)  such that the distance traversed on both arms is exactly identical. Because the thickness of the compensator plate and the beamsplitter are the same, both wavefronts pass through the same amount of glass and air, so the path length of the light beams in both interferometer arms will be exactly the same. Therefore, the two fields will arrive in phase to the observer, and their amplitudes will add up constructively, producing a bright spot on the viewing screen. If now you turn the micrometer to offset the length of one arm by a half of light wavelength, $\Delta l = \lambda/2$, they will acquire a relative phase of $\pi$, and total destructive interference will occur:
\begin{displaymath}
\mathbf{E}_1 +\mathbf{E}_2=0\;\;\mathrm{or} \;\;\mathbf{E}_1 = -\mathbf{E}_2.
%\end{displaymath}
% or
%\begin{displaymath}\mathbf{E}_1(t) = -\mathbf{E}_2(t).
\end{displaymath}
It is easy to see that constructive interference happens when the difference between path lengths in the two interferometer arms is equal to the integer number of wavelengths $\Delta l = m\lambda$, and destructive interference corresponds to a half-integer number of wavelengths $\Delta l = (m + 1/2) \lambda$ (here $m$ is an integer number). Since the wavelength of light is small ($600-700$~nm for a red laser), Michelson interferometers are able to measure distance variation with very good precision.


In \textbf{Fabry-Perot configuration} the input light field bounces between two closely spaced partially reflecting surfaces, creating a large number of reflections. Interference of these multiple beams produces sharp spikes in the transmission for certain light frequencies. Thanks to the large number of interfering rays, this type of interferometer has extremely high resolution, much better than a Michelson interferometer. For that reason Fabry-Perot interferometers are widely used in telecommunications, lasers and spectroscopy to control and measure the wavelengths of light. In this experiment we will take advantage of the high spectral resolution of the Fabry-Perot interferometer to resolve two very closely-spaced emission lines in Na spectra by observing changes in the overlapping interference fringes from the two lines.
\begin{figure}[h]
\centering
\includegraphics[width=0.8\linewidth]{./pdf_figs/fpfig1} \caption{\label{fpfig1}Sequence of Reflection and Transmission for a ray arriving at the treated inner surfaces $P_1 \& P_2$.}
\end{figure}

A Fabry-Perot interferometer consists of two parallel glass plates, flat to better than 1/4 of an optical
wavelength $\lambda$, and coated on the inner surfaces with a partially transmitting metallic layer. Such
two-mirror arrangement is normally called an {\it optical cavity}. The light in a cavity by definition bounces
back and forth many times before escaping; the idea of such a cavity is crucial for the construction of a laser.
Any light transmitted through such cavity is a product of interference between beams transmitted at each bounce
as diagrammed in Figure~\ref{fpfig1}. When the incident ray arrives at interface point $A$, a fraction $t$ is
transmitted and the remaining fraction $r$  is reflected, such that $t + r = 1$ ( this assumes no light is lost
inside the cavity). The same thing happens at each of the points $A,B,C,D,E,F,G,H\ldots$, splitting the initial
ray into parallel rays $AB,CD,EF,GH,$ etc. Between adjacent ray pairs, say $AB$ and $CD$, there is a path
difference of :
\begin{equation}
 \delta = BC+CK
\end{equation}%eq1
    where $BK$ is normal to $CD$.  In a development
similar to that used for the Michelson interferometer, you can show that:
\begin{equation}
 \delta = 2d\cos\theta
\end{equation}%eq.2
     If this path difference produces
constructive interference, then  $\delta$  is some integer multiple of $\lambda$, namely,
\begin{equation}
  m\lambda = 2d\cos\theta %eq.3
\end{equation}%eq.3

This applies equally to ray pairs $CD$ and $EF, EF$ and $GH$, etc, so that all parallel rays to the right of
$P2$ will constructively interfere with one another when brought together.

Issues of intensity of fringes \& contrast between fringes and dark background are addressed in Melissinos, {\it
Experiments in Modern Physics}, pp.309-312.

\subsection*{Laser Safety}
\textbf{Never look directly at the laser beam!} Align the laser so that it is not at eye level. Even a weak
laser beam can be dangerous for your eyes.


\section*{Alignment of Michelson interferometer}

\textbf{Equipment needed}: Pasco precision interferometry kit, a laser, Na lamp, adjustable-height platform (or a few magazines or books).

To simplify the alignment of a Michelson interferometer, it is convenient to work with diverging optical beams. In this case an interference pattern will look like a set of concentric bright and dark circles, since the components of the diverging beam travel at slightly different angles, and therefore acquire different phase, as illustrated in Figure \ref{fig2mich.fig}. Suppose that the actual path length difference between two arms is $d$. Then the path length difference for two off-axis rays arriving at the observer is $\Delta l = a + b$ where $a =
d/\cos \theta$ and  $b = a\cos 2\theta$:
\begin{equation}
\Delta l=\frac{d}{\cos \theta}+\frac{d}{\cos \theta}\cos 2\theta
\end{equation}
Recalling that  $\cos 2\theta = 2(\cos \theta)^2-1$, we obtain $ \Delta l=2d\cos\theta$. The two rays interfere
constructively for any angle $\theta_c$ for which $ \Delta l=2d\cos\theta = m\lambda$ ($m$=integer); at the same
time, two beams traveling at the angle $\theta_d$ interfere destructively when $ \Delta l=2d\cos\theta =
(m+1/2)\lambda$ ($m$=integer). Because of the symmetry about the normal direction to the mirrors, this will mean
that interference ( bright and dark fringes) appears in a circular shape.
If the  fringes are not circular, it means
simply that the mirrors are not parallel, and additional alignment of the interferometer is required.

\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{./pdf_figs/fig2} \caption{\label{fig2mich.fig}Explanation the interference pattern. Notice that to simplify the figure we have ``unfold'' the interferometer by neglecting the reflections on the beamsplitter.}
\end{figure}

When the path length difference is varied, by using the micrometer to move one of the mirrors a distance $\Delta l$ along the horizontal axis of Figure~\ref{fig2mich.fig}, the fringes appear to ``move''. As the micrometer is turned, the condition for constructive and destructive interference is alternately satisfied at any given angle.  If we fix our eyes on one particular spot and count how many bright fringes pass that spot as we move mirror $M_1$ by a known distance, we can determine the wavelength of light in the media using the condition for constructive interference

\begin{equation}
\label{eqn:constructive}
2\Delta l \cos\theta = m\lambda.
\end{equation}

The factor of two comes from from the fact that if I move the mirror $\Delta l$ light has to go an additional distance $\Delta l \cos\theta$ to get to it and then the same distance return from it.  For simplicity, we might concentrate on the center of the fringe bull's eye at $\theta = 0$.  Equation~\ref{eqn:constructive} for constructive interference then reduces to $2 \Delta l = m\lambda$ (m = integer). If $X_1$ is the initial position of the mirror $M_1$ (as measured on the micrometer) and $X_2$ is the final position after a number of fringes $\Delta m$ has been counted, we have $2\Delta l = 2(X_2-X_1) = \lambda\Delta m$.   Then the laser wavelength $\lambda$ is then given as:

\begin{equation}\label{old3}
\lambda = 2(X_2-X_1)/\Delta m.
\end{equation}

Set up the interferometer as shown in Figure~\ref{fig1mich.fig} using
components from the PASCO interferometry kit. The mirrors $M_{1,2}$ are, correspondingly, a movable and an adjustable mirror from the kit. Alignment is not too difficult:

\begin{enumerate}
\item Begin by installing the movable mirror, adjustable mirror, component holder with viewing screen (magnetically attaches), and the lens holder. Do not install the lens yet. Make sure screws are firmly tightened as we don't want components to move around.
\item Level the table by adjusting its legs and/or adjust the laser height, location and direction to get the beam spot at the center of the movable mirror.  It is a good idea to tape your laser down at this point, or make sure it's securely mounted in the optical breadboard (if your station has one). 
\item Tweak the laser direction using the two knobs on its back so that the beam reflects back into its aperture.
\item Install the beam splitter, orient it to center the new beam spot on the adjustable mirror.
\item Tweak the adjustable mirror using the two knobs on its back. You want to make the two sets of dots visible on the viewing screen come into alignment. When they are properly aligned you should see interference fringes appear in the dot.
\item It is possible to see interference without the compensator (ask yourself why). But, it can potentially make the pattern better and more distinct. Try inserting it and aligning to improve the pattern.
\item Install the lens to blow up the pattern.
\item Further tweak the adjustable mirror to bring the bullseye on screen. You will find it's very sensitive and a little tweaking goes a long way.
\end{enumerate}

 Be patient, make small changes, think about what you are doing, and get some help from the instructor and TA.  It helps to start the alignment procedure with the micrometer and the movable mirror near the center of their ranges. Also, the alignment knobs on the laser and adjustable mirror should be at the middle of their ranges, so that the plane of each is perpendicular to the table. 

\emph{A word of caution: sometimes dust on a mirror or imperfections on optical surfaces may produce similar intensity patterns. True interference disappears if you block one arm of the interferometer. Try it!}

\textbf{Note}: before starting the measurements, make sure you understand how to read the micrometer properly!
\begin{figure}[h]
\centering
%\includegraphics[width=0.7\columnwidth]{./pdf_figs/fig3}
%\subcaptionbox{Reading = 211~$\mu$m} {
\includegraphics[height=1.3in]{./pdf_figs/micrometer1}
%}
%\subcaptionbox{Reading = 345~$\mu$m} {
\includegraphics[height=1.3in]{./pdf_figs/micrometer2}
%}
%\subcaptionbox{Reading = 166~$\mu$m} {
\includegraphics[height=1.3in]{./pdf_figs/micrometer3}
%}
\caption{\label{fig3mich.fig}Micrometer readings. The coarse division equals to 100~$\mu$m, and smallest division on the rotary dial is 1~$\mu$m (same as 1 micron). The final measurements is the sum of two. So, from left to right, the figures above show 211~$\mu$m, 345~$\mu$m, and 166~$\mu$m.}
\end{figure}

\section*{Wavelength measurements using Michelson interferometer}

\subsection*{ Calibration of the interferometer}

Record the initial reading on the micrometer. Focus on the central fringe and begin turning the micrometer. You will see that the fringes move. For example, the central spot will change from bright to dark to bright again, that is counted as one fringe. A good method: pick out a reference line on the screen and then softly count fringes as they pass the point.  Count a total of about $\Delta m = 50$ fringes and record the new reading on the micrometer.

Each lab partner should make at least two independent measurements, starting from different initial positions of the micrometer. For each trial, approximately 50 fringes should be accurately counted and tabulated with the initial $X_1$ and final $X_2$ micrometer settings. Do this at least five times (e.g., $5\times 50$ fringes). Consider moving the mirror both forward and backward. Make sure that the difference $X_2-X_1$ is consistent between all the measurements. Calculate the average value and standard deviation of the micrometer readings $<X_2-X_1>$.  What value do you obtain for the laser wavelength, and what are your uncertainties on that measurement?  Does this match what you expect for a red laser?

%When your measurements are done, ask the instructor how to measure the wavelength of the laser using a commercial wavemeter. Using this measurement and Equation~\ref{old3} calculate the true distance traveled by the mirror $\Delta l$, and calibrate the micrometer (i.e. figure out precisely what displacement corresponds to one division of the micrometer screw dial).

%\noindent\textbf{\emph{Experimental tips}}:
\subsection*{Experimental tips}
\begin{enumerate}
\item Avoid touching the face of the front-surface mirrors, the beamsplitter, and any other optical elements!
\item The person turning the micrometer should also do the counting of fringes. It can be easier to count them in bunches of 5 or 10 (\textit{i.e.} 100 fringes = 10 bunches of 10 fringes).
\item Use a reference point or line and count fringes as they pass.
\item  Before the initial position $X_1$ is read make sure that the micrometer has engaged the drive screw (There can be a problem with ``backlash''). Just turn it randomly before counting.
\item  Avoid hitting the table which can cause a sudden jump in the number of fringes.
\end{enumerate}

\subsection*{Measurement of the Na lamp wavelength}

A calibrated Michelson interferometer can be used as a \textbf{wavemeter}
to determine the wavelength of different light sources. In this experiment
you will use it to measure the wavelength of strong yellow sodium
fluorescent light, produced by the discharge lamp.\footnote{Actually,
sodium might be a better calibration source than a HeNe laser, since it has well known lines, whereas a HeNe can lase at different wavelengths.  Perhaps an even better calibration source might be a line from the Hydrogen Balmer series, which can be calculated from the Standard Model.}

Without changing the alignment of the interferometer (i.e. without touching
any mirrors), remove the focusing lens and carefully place the
interferometer assembly on top of an adjustable-height platform such that
it is at the same level as the output of the lamp. Since the light power in
this case is much weaker than for a laser, you won't be able to use the
viewing screen. You will have to observe the interference looking directly
to the output beam - unlike laser radiation, the spontaneous emission of a
discharge is not dangerous\footnote{In the ``old days'' beams in high
energy physics were aligned using a similar technique. An experimenter
would close his eyes and then put his head in a collimated particle beam.
Cerenkov radiation caused by particles traversing the experimenter's
eyeball is visible as a blue glow or flashes. This is dangerous but various
people claim to have seen it done... when a radiation safety officer wasn't
around.}.  However, your eyes will get tired quickly! Placing a diffuser plate in front of the lamp will make the observations easier. Since the interferometer is already aligned, you should see the interference picture. Make small adjustments to the adjustable mirror to make sure you see the center of the bull's eye.

Repeat the same measurements as in the previous part by moving the mirror and counting the number of fringes. Each lab partner should make at least two independent measurements, recording initial and final position of the micrometer, and you should do at least five trials. Calculate the wavelength of the Na light for each trial. Then calculate the average value and its experimental uncertainty. Compare with the expected value of \unit[589]{nm}.

In reality, the Na discharge lamp produces a doublet - two spectral lines that are very close to each other: \unit[589]{nm} and \unit[589.59]{nm}. Do you think your Michelson interferometer can resolve this small difference? Hint: the answer is no - we will use a Fabry-Perot interferometer for that task.

\clearpage

\section*{Alignment of the Fabry-Perot interferometer}

\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{./pdf_figs/fpfig3} \caption{\label{fpfig3.fig}The Fabry-Perot Interferometer.  \textbf{For initial alignment the laser and the convex lens are used instead of the Na lamp.}}
\end{figure}
Disassemble the Michelson Interferometer, and assemble the Fabry-Perot interferometer as shown in
Figure~\ref{fpfig3.fig}, initially using the laser for alignment instead of the sodium lamp. First, place the viewing screen behind the two partially-reflecting mirrors ($P1$ and $P2$), and adjust the mirrors such
that the multiple reflections on the screen overlap. Then place a convex
lens after the laser to spread out the beam, and make small adjustments
until you see the concentric circles. Is there any difference between the
thickness of the bright lines for the two different interferometers? Why?

Loosen the screw that mounts the movable mirror and change the distance between the mirrors. Realign the interferometer again, and comment on the difference in the interference picture. Can you explain it?

Align the interferometer one more time such that the distance between the two mirrors is $1.0 - \unit[1.5]{mm}$, but make sure the mirrors do not touch!

\subsection*{Sodium doublet measurements}

\begin{enumerate}
\item Turn off the laser, remove the viewing screen and the lens, and place
	the interferometer on the adjustable-height platform, or
	alternatively place the Na lamp on its side and plan to adjust it's
	height with books or magazines. With the diffuser sheet in front of
	the lamp, check that you see the interference fringes when you look
	directly at the lamp through the interferometer. If necessary, adjust the knobs on the adjustable mirror to get the best fringe pattern.

\item Because the Na emission consists of two lights at two close
	wavelengths, the interference picture consists of two sets of
	rings, one corresponding to fringes of $\lambda_1$, the other to
	those for $\lambda_2$. Move the mirror back and forth (by rotating
	the micrometer) to identify two sets of rings. Notice that they move
	at slightly different rates (due to the wavelength difference).

\item Seek the START condition illustrated in Fig.(\ref{fpfig4.fig}), such that all bright fringes are evenly spaced. Note that alternate fringes may be of somewhat different intensities. Practice going through the fringe conditions as shown in Fig.(\ref{fpfig4.fig}) by turning the micrometer and viewing the relative movement of fringes. Do not be surprised if you have to move the micrometer quite a bit to return to the original condition again.

\item Turn the micrometer close to zero reading, and then find a place on the micrometer ($d_1$) where you have the ``START'' condition for fringes shown in Fig.(\ref{fpfig4.fig}). Now advance the micrometer rapidly while viewing the fringe pattern ( NO COUNTING OF FRINGES IS REQUIRED ).  Note how the fringes of one intensity are moving to overtake those of the other intensity (in the manner of Fig.(\ref{fpfig4.fig})).  Keep turning until the ``STOP'' pattern is achieved (the same condition you started with).  Record the micrometer reading as $d_2$.

\item Each lab partner should repeat this measurement at least one time, and each group should have at least three independent measurements.

\item Make sure to tabulate all the data taken by your group.

\end{enumerate}

We chose the ``START'' condition (the equally spaced two sets of rings)
such that for the given distance between the two mirrors, $d_1$,  the bright fringes of $\lambda_1$ occur at the points of destructive interference for $\lambda_2$. Thus, the bull's eye center ($\theta= 0 $) we can write this down as:

\begin{equation}
2d_1=m_1\lambda_1=\left(m_1+n+\frac{1}{2}\right)\lambda_2
\end{equation}

Here the integer $n$ accounts for the fact that   $\lambda_1 >  \lambda_2$,  and the  $1/2$  for the condition of destructive interference for   $\lambda_2$  at the center. The ``STOP'' condition corresponds to the similar situation, but  the net action of advancing by many fringes has been to increment the fringe count of $\lambda_2$ by one more than that of $\lambda_1$:

\begin{equation}
2d_2=m_2\lambda_1=\left(m_2+n+\frac{3}{2}\right)\lambda_2
\end{equation}

Try to estimate your uncertainty in identifying the START and STOP positions by turning the micrometer back and forth, identifying the points at which you can begin to see doublet, rather than equally spaced, lines.  The variance from multiple measurements should agree, at least approximately, with this estimate.

Subtracting the two interference equations, and solving for the distance traveled by the mirror $d_2-d_1$ we obtain:

\begin{equation}
2(d_2-d_1)=\frac{\lambda_1\lambda_2}{(\lambda_1-\lambda_2)}
\end{equation}

Solving this for $\Delta \lambda = \lambda_1-\lambda_2$, and accepting as
valid the approximation that $\lambda_1\lambda_2\approx \lambda^2$
(where $\lambda$ is the average of $\lambda_1$  and $\lambda_2 \approx
589.26$ nm), we obtain:

\begin{equation}
\boxed{\Delta\lambda \approx \frac{\lambda^2}{2(d_2-d_1)}}
\end{equation}

Use this equation and your experimental measurements to calculate average value of Na doublet splitting and its standard deviation, as well as any experimental uncertainties. Compare your result with the established value of $\Delta \lambda_{Na}=0.598$~nm.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\linewidth]{./pdf_figs/fpfig4} \caption{\label{fpfig4.fig}The sequence of fringe patterns encountered in the course of the measurements. Note false colors: in your experiment the background is black, and both sets of rings are bright yellow.}
\end{figure}

\newpage
\section*{Observation of Gravitational Waves (LIGO)}
\label{LIGO}
\textbf{A Michelson interferometer can help to test the theory of general relativity!} 
%
Gravity waves, predicted by the theory of relativity, are ripples in the fabric
of space and time produced by violent events in the distant universe, such as
the collision of two black holes. Gravitational waves are emitted by
accelerating masses much as electromagnetic waves are produced by accelerating
charges, and often travel to Earth. Until recently the only indirect evidence for these waves
was the observation of the rotation of a binary pulsar  (for which the 1993 Nobel Prize was awarded).


\begin{figure}[h]
\centering
\includegraphics{./pdf_figs/LIGO} \caption{\label{LIGO.fig}For more details see http://www.ligo.caltech.edu/}
\end{figure}

The Laser Interferometry Gravitational-wave Observatory (LIGO) Michelson interferometer
built with the goal of directly detecting gravitational wave. Two mirrors hang $2.5$~mi apart,
forming one ``arm'' of the interferometer, and two more mirrors make a second arm
perpendicular to the first. Laser light enters the arms through a beam splitter
located at the corner of the L, dividing the light between the arms. The light
is allowed to bounce between the mirrors repeatedly before it returns to the
beam splitter. If the two arms have identical lengths, then interference
between the light beams returning to the beam splitter will direct all of the
light back toward the laser. But if there is any difference between the lengths
of the two arms, some light will travel to where it can be recorded by a
photodetector.

The space-time ripples cause the distance measured by a light beam to change as the gravitational wave passes by. These changes are minute: just $10^{-16}$ centimeters, or one-hundred-millionth the diameter of a hydrogen atom over the $2.5$ mile length of the arm. Yet, they are enough to change the amount of light falling on the photodetector, which produces a signal defining how the light falling on the photodetector changes over time. LlGO requires at least two widely separated detectors, operated in unison, to rule out false signals and confirm that a gravitational wave has passed through the earth. Three interferometers were built for LlGO -- two near Richland, Washington, and the other near Baton Rouge, Louisiana, shown in Fig.~\ref{LIGO.fig}.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\linewidth]{./pdf_figs/LIGO_data.png} \caption{\label{LIGO_data.fig} Data from LIGO's two sites from the first detection of gravitational waves caused by the collision of two black holes (from \href{https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.116.061102}{Physical Review Letters \textbf{116}, 061102).}}
\end{figure}

In 2015, LIGO observed the first direct detection of gravitational waves from the collision of two black holes.  Figure~\ref{LIGO_data.fig} shows data from the black hole merger event where the `strain' reflects the observed change in distance in the interferometer arm due to the passing gravitation wave, which oscillates as a function of time.  For a more information you can browse the \href{http://www.ligo.caltech.edu/}{LIGO webpage} and view a \href{https://www.ligo.caltech.edu/video/ligo20170601v2}{simulation of a black hole merger}.

\end{document}

