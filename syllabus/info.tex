\documentclass[letterpaper,10pt]{article}
\usepackage[left=1in,top=0.75in,bottom=1in,right=1in,nohead]{geometry}
%\usepackage{mylatex}
\usepackage{url}
\usepackage{hyperref}

\begin{document}

%\maketitle
\pagestyle{empty}

\section*{Physics 251: Experimental Atomic Physics}

\begin{center}
\rule{\textwidth}{0.5pt}
\end{center}

\subsection*{Instructors}

  \begin{description}
  \item[\underline{Jeff Nelson}]
  \item[Office:] Small 341A
  \item[Phone:] 221-3579
  \item[Email:] {\tt jknels@wm.edu}
  \item[Office Hours:] MT 1400-1500
  \end{description}
  

  \begin{description}
  \item[\underline{Justin Stevens}]
  \item[Office:] Small 343C
  \item[Phone:] 221-5494 
  \item[Email:] {\tt jrstevens01@wm.edu}
  \item[Office Hours:] WF 900-1200 or by appointment
  \end{description}  

\begin{center}
\rule{\textwidth}{0.5pt}
\end{center}

\begin{description}
\item[Website:] We'll use Blackboard. {\tt https://blackboard.wm.edu}

\item[Class Times:] W 1400-1650, R 900-1150 R 1400-1650 [three sections], in Small Hall 133. Prof.~Stevens will be the instructor of record for both R sections, and Prof.~Nelson will be the instructor of record for W.  %We plan to switch up on occasion.

\item[Lab Assistants:] Joseph Cuozzo ({\tt jjcuozzo@email.wm.edu}) and Jozef Trokan-Tenorio ({\tt jmtrokantenori@email.wm.edu})

\item[Text:] There is no required text for this class, aside from the lab manual provided on Blackboard. However, as supplements we suggest: 
\begin{itemize}
\item Melissinos and Napolitano, {\it Experiments in Modern Physics}, 2nd Ed., Academic Press.
\item  G.~L.~Squires, {\it Practical Physics}, 4th Ed., Cambridge.
\item J.~Taylor, {\it Introduction to Error Analysis}, 2nd Ed., University Science Books
\end{itemize}
We have placed copies of these books on reserve at Swem. 

\item[Sections:] There are three independent sections for this class. In general, you cannot switch between the three after the first experiment since you will have a lab partner and some of the experiments require at least two people. 

\item[Physics 201:] Physics 201 is a pre- or co-requisite of this class. However, unlike Physics 101/102, the two classes are separate and we do not try very hard to keep them in sync or make sure that 201 covers the theory behind a topic before 251 does the experiment. This is, more or less, how progress in physics really occurs! Experimenters usually do their work at the frontier where there is no theory or where there are multiple competing theories.

\item[Schedule:]  You'll be conducting a new experiment every week, using the 170 minutes we have in class. 

\begin{description}
\item[Standard Experiments:] These are canonical experiments that everyone will do and then write a report on. In general, we do not have more than 4 setups of each experiment. This means that while one half of the class is experiment A, the other half will be on experiment B. And the next week you'll swap. The standard experiments are:

\begin{center}
\begin{tabular}{|l|c||l|c|}
\hline
1 & optical interferometry & 6 & electron diffraction\\ \hline
2 & Faraday rotation & 7 & double slit experiment\\ \hline
3 & e/m of the electron & 8 & atomic spectroscopy\\ \hline
4 & photo-electric effect & 9 & superconductivity\\ \hline
5 & black-body radiation & & \\ \hline
\end{tabular}
\end{center}

\item[Special Projects:] These are two-week experiments that you will get to pick from a list. They are a little more challenging and sparsely documented than the standard experiments. 

\item[Presentations:] At the end of the semester, in place of a final exam, we'll have a mini-conference in which your group will give a short presentation on your special project. {\it Please plan to be here for the final exam dates:} December 14, 1400-1700 for Wednesday afternoon; December 19, 1400-1700 for Thursday morning; December 18, 1400-1700 for Thursday afternoon.  %We may offer other assignments in lieu of a talk, stay tuned.

\item[Lab Visit:] One week, instead of doing experiments, you'll visit physics and applied science research labs, and maybe even ({\it gasp}) talk with a theorist or two. You'll write up a summary of your visit.

\item[Add/drop and withdrawal:]  The add/drop deadline is September 7 and the withdraw deadline is October 26. 

\end{description}

\item[Reading Ahead:] The standard experiments have lab ``manuals'' which we will post on Blackboard. You should reserve about half an hour to read over them \textbf{\textit{before}} coming to lab. 

\item[Labbook:] You need to keep a good labbook, with your raw data, sketches or pictures of the equipment, notes on your experimental methodology, calculations, etc. Everything you do goes into this book and it provides the foundation for your lab reports. You need to use the labbook we have provided and bring it every week (if you forget you'll have to run home to get it). We'll look at it twice during the semester, assigning a grade each time. Only the highest of the two grades counts (so you can improve).

\item[Lab Reports:] You will need to write a report for each of the standard labs. Though you will do the experiments in collaboration with a partner, your lab report is your own work. They are due at the start of class the following week. Late lab reports will be docked one letter grade per week.

In physics and mathematics \LaTeX{} is the standard program used to format papers. It's great, especially for mathematical forumlae, figures with captions, and tables. A previous student says:

\begin{quote}
{\it Just letting you know I used LaTeX using TeXnicCenter as my editor.
At first I was a bit skeptical because I felt Word could do just as
much without having to build the document to view it. But as
experiments began to need more figures and equations I really started
to enjoy latex because it was so simple. I could make gigantic
equations like the Schrodinger's equation in just a minute whereas
word would have taken me quite a while. Anyways I'm really happy I
learned to use LaTeX and look forward to using it in the future. }
\end{quote}

There is a cloud service \url{https://www.overleaf.com/}, which is free and easy to use.  \LaTeX{} is also available on Windows/Mac/Linux and there are various distributions, including TeXnicCenter, that run on your computer (see BlackBoard for installation options).  We encourage you to try \LaTeX{} but do not require it. MSWord and other WSIWYG programs are not ideal but can get the job done.

\item[Analysis Software:]  You will need to make graphs, histograms and do computations on the raw data. Various programs are available and you can use whatever tool gets the job done for you. %{\bf Beware of Excel... you will eventually hit a wall with it.} 
That said, we suggest you use MATLAB or Python for data analysis in this class. MATLAB is free to download or use through the cloud service \url{https://matlab.mathworks.com/} by requesting an account from \href{https://www.wm.edu/offices/it/services/software/licensedsoftware/mathstats/matlabstud/index.php}{W\&M IT}.  There is some example code for MATLAB on the Blackboard page.  Python is being used in PHYS 256 this semester, and examples will be provided for that as well.  Both are straightforward with good documentation.

\item[Grading:]  The grade will be based on the lab reports + lab visit (80\%), the special project (15\%), and an evaluation of your labbook (5\%). The lowest lab score will be dropped. However, in order to pass the class, {\bf you must do and turn in a report for all labs}. You must also do the special project.  Makeup labs are only allowed with prior permission and for a good reason or due to illness with a doctor's note. Unlike 101/102 labs there is no reserved date. We'll just deal with this as necessary.

\item[Letter Grades:]
Letter grades will be assigned as follows:\\
\begin{center}
\begin{tabular}{ccc}
              & A  $>$ 93 \% &  A-  90-92\\
   B+  87-89 &  B  83-86  &   B-  80-82\\
   C+  77-79 &  C  73-76  &   C-  70-72\\
   D+  67-69 &  D  63-66  &   D-  60-62\\
   F  $<$ 60 & & 
\end{tabular}
\end{center}

\end{description}

\end{document}
