\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Superconductivity}

 \textbf{Experiment objectives}: study the behavior of a high temperature superconducting material
    Yttrium-Barium-Copper-Oxide (YBCO, YBa$_2$Cu$_3$O$_7$) in magnetic field, measure the critical
 temperature for a phase transition in a superconductor.

\subsection*{History}

Solids can be roughly divided into four classes, according to the way they
conduct electricity. They are: Metals, Semiconductors, Insulators and
Superconductors. The behavior of these types of materials is explained by
quantum mechanics. Basically, when atoms form a solid, the atomic levels of the
electrons combine to form bands. That is, over a finite range of energies there
are states available to electrons. Since only one electron can occupy a given
state, as described by the {\bf Pauli Exclusion Principle}, electrons will fill these states up
to some maximum: the Fermi Energy, $E_f$.  A solid is a metal if it has an
energy band that is not full. The electrons are then free to move about,
making a metal a good conductor of electricity. If the solid has a band
that is completely full, with an energy gap to the next band,  that solid will not
conduct electricity very well, making it an insulator. A semiconductor is
between a metal and insulator: while it has a full band (the valence band),
the next band (the conduction band) is close enough in energy, and so the
electrons can easily reach it. Superconductors are in a class by themselves.
They can be metals or insulators at room temperature. Below a certain
temperature, called the critical temperature, the electrons ``pair'' together (in
Cooper pairs) and travel through the solid without resistance. Current in a
superconductor below the critical temperature will travel indefinitely without
dissipation.

 Superconductivity was discovered in 1911 by  H. Onnes. He
    discovered that simple metals (Pb, Nb) superconduct when
    placed in liquid helium (4 K). This was an important
    discovery, but the real excitement came in 1986 when Swiss
    scientists discovered that certain ceramics would superconduct
    at 35 K. Several groups later discovered materials that would
    superconduct at temperatures up to 125 K. These materials are
    called high temperature superconductors (HTS). Their discovery
    was a breakthrough, because it meant that these
    superconductors will work in liquid nitrogen (at 77 K), which
    is relatively cheap and abundant.

    A fascinating fact about superconductors: they will carry
    a current nearly indefinitely, without
    resistance. Superconductors have a critical temperature, above which they lose their
    superconducting properties.

    Another striking demonstration of superconductivity is the \textbf{Meissner effect}.
    Magnetic fields cannot penetrate superconducting surfaces; instead a
    superconductor attempts to expel all magnetic field
    lines. It is fairly simple to intuitively understand the Meissner effect if you imagine a perfect
    conductor of electricity. If a superconductor placed in a magnetic field,
    Faraday's Law says an induced current that opposes the field
    would be set up. But unlike in an ordinary metal, this induced current does not dissipate in
    a perfect conductor. So, this
    induced current would always be present to produce a field
    which opposes the external field. In addition, microscopic dipole moments
    are induced in the superconductor that oppose the applied field. This induced field
repels the source of the applied magnetic field, and will consequently repel
the magnet associated with this field.  Thus, a superconductor will levitate a
magnet placed upon it (this is known as magnetic levitation).

\subsection*{Safety}
\begin{itemize}
\item Wear glasses when pouring liquid nitrogen. Do not get it on your
skin or in your eyes!
\item Do not touch anything that has been immersed in liquid nitrogen until the
item warms up to the room temperature. Use the provided tweezers to remove and
place items in the liquid nitrogen.
\item Do not touch the superconductor: it contains poisonous materials!
\item Beware of the current leads: they are carrying a lethal current!
\end{itemize}


\section*{Experimental procedure}
\textbf{Equipment needed}: YBCO disk, tweezers, thermal insulating container, small magnet.



\subsection*{Magnetic Levitation (the Meissner effect)}

\begin{enumerate}

\item Place one of the small magnets (provided) on top of the superconducting
disk at room temperature. Record the behavior of the magnet.

\item  Using the tweezers, place the superconducting disk in the thermal
	insulating container.
	Attach  the thermocouple leads (see diagram) to a multimeter
   reading on the mV scale. Slowly pour liquid nitrogen over the disk,
   filling the container as much as you can. The nitrogen will boil, and
   then settle down. When the multimeter reads about 6.4 mV, you are
   at liquid nitrogen temperature (77 K).


\item After the disk is completely covered by the liquid nitrogen, use the tweezers
to pick up the provided magnet and attempt to balance it on top of the
superconductor disk. Record what you observe.

\item Try demonstrating a \emph{frictionless magnetic bearing}: if you carefully set the magnet rotating,
you will observe that the magnet continues to rotate for a long time. Also, try
moving the magnet across the superconductor. Do you feel any resistance? If you
   feel resistance, why is this?

\item Using tweezers, take the disk (with the magnet on it) out of the
   nitrogen and place it beside the container, allowing it to
   warm. Watch the thermocouple reading carefully, and take a reading
   when the magnet fails to levitate any longer. This is a rough estimate of the
   critical temperature. Make sure you record it!

\item Repeat the experiment by starting with the magnet on top of the
superconductor disk and observe if the magnet starts levitating when the disk's
temperature falls below critical.

\end{enumerate}
\begin{figure}
\includegraphics[height=2in]{./pdf_figs/scnut}
\caption{\label{scnut} The superconducting disk with leads.}
\end{figure}


\subsection*{Measuring resistance and critical temperature}

    We will measure the resistance by a {\bf four probe method}, as a
    function of temperature. Using four probes (two for current
    and two for voltage) eliminates the contribution of resistance
    due to the contacts, and is good to use for samples with small
    resistances. Connect a voltmeter (with 0.01 mV resolution) to
    the yellow wires. Connect a current source through an ammeter
    to the {\bf black} wires. Place a current of about 0.2 Amps (200 mA)
    through the black leads. Note: {\bf DO NOT EXCEED 0.5 AMP!!!!}
    %On the
%    Elinco power supplies, you hardly have to turn the knobs at
%    all!
At room temperature, you should be reading a non-zero
    voltage reading.

\begin{enumerate}
 \item With the voltage, current and thermocouple leads attached,
    carefully place the disk in the container. Pour liquid nitrogen into the
    container. Wait until the temperature reaches 77 K.
\item With tweezers,  take the disk out of nitrogen and place it outside of
	the container. {\bf Start quickly recording the current, voltage and thermocouple readings
   as the disk warms up.} When superconducting, the disk should have
   V=0 (R=0). At a critical temperature, you will see a voltage
   (resistance) appear.

\item Repeat this measurement several times to acquire a significant number of data points
near the critical temperature (6.4-4.5 mV). Make a plot of
   resistance versus temperature, and make an estimate of the critical
   temperature based on this plot.

\end{enumerate}



\hskip-.8in\includegraphics[height=5in]{./pdf_figs/mvtok}

\newpage

\subsection*{Instructions for a brief writeup}

The abstract, introduction and conclusion are not needed. Do the following analysis steps and/or answer the following questions. Make it clear which question you are addressing. For example:

\noindent{\bf Thermocouple}\\
Blah, blah, blah, blah...

\subsubsection*{Address the following points}

\begin{description}
\item[Diagram] the experimental setup, including the current source, the superconductor and its wires, and the DMMs.
\item[Thermocouple] Explain in your own words what a thermocouple is and how it works (i.e., the physics). No more than a paragraph is needed.
\item[Resistance] In the Meissner effect demonstration, when moving the magnets around, did you feel resistance?  Why or why not? Explain in terms of physics concepts and principles that you learned in PHYS 102. 
\item[Tabulate] your data, including columns for voltage, current, resistance, thermocouple voltage, and temperature.
\item[Uncertainties] Estimate them and state them.
\item[Temperature] Explain how you got the temperature from the thermocouple voltage. What was your procedure? No more than a couple of sentences are needed.
\item[Plot] the resistance (y-axis) vs temperature (x-axis). Based on the plot, what is the critical temperature for YBCO?
\item[Fit] the R vs T plot.  What function?  You tell me.  Based on the fitted function, can you estimate the critical temperature? Parameterizing a dataset and then using it to characterize the data, perhaps with a single number, is a common laboratory task. Get used to it!
\end{description}


\end{document}

