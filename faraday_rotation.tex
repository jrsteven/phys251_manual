\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Faraday Rotation}

%\date {}
%\maketitle \noindent
 

\textbf{Experiment objectives}: Observe the {\it Faraday Effect}, the rotation of a light wave's polarization vector in a material with a magnetic field directed along the wave's direction. Determine the relationship between the magnetic field and the rotation by measuring the so-called {\it Verdet constant} of the material. Become acquainted with some new tools: an oscilloscope, a function generator and an amplifier, and a new technique: phase-locking.

\section*{Introduction}
The term polarization refers to the direction of the electrical field in a
light wave. Generally, light is not polarized when created (e.g., by atomic
deexcitations) but can be made so by passing it through a medium that transmits electric fields oriented in one direction  and absorbs all others. Imagine we create a beam of light traveling in the $z$ direction. We then polarize it in the $x$ direction ($\vect{E}=\vect{\hat x}E_0\cos(kz-\omega t)$) by passing it through a polarizer and then pass it through a second polarizer, with a transmission axis oriented at an angle $\theta$ with respect to the $x$ axis. If we detect the light beam after the second polarizer, the intensity is 
\begin{equation}
I=I_0 \cos^{2}\theta
\end{equation}

In 1845, Michael Faraday discovered that he could create polarized light and then rotate the direction of the polarization using a magnetic field. The field, created in our laboratory by a solenoid, points in the direction of the light beam.  The Faraday effect is caused by a combination of factors:
\begin{enumerate}
\item We can describe the polarization vector in terms of right- and left-handed components. In this description, the electric field of the right-handed component rotates clockwise around the $z$ axis as the wave travels. The left-handed component rotates counter-clockwise. 
\item At the particle level, the right and left hand components correspond to photons with angular momentum $\hbar=h/2\pi$ directed parallel (right) or anti-parallel (left) to the $z$ axis.
\item When an atom is placed in a magnetic field, single atomic energy levels may divide into multiple levels, each with slightly different energies. This is called the {\it Zeeman effect} and qualitatively it occurs because the moving atomic electrons may be thought of as a current $\vect{J}$ which interacts with the external magnetic field $\vect{B}$. 
\item The atomic current $\vect{J}$ contains components with different angular momenta, and those components interact differently with right and left handed photons. 
\item The macroscopic effect is that a material in a magnetic field can have a different index of refraction $n$ for left and right handed light. Since the phase velocity of light $c'$ depends on $n$ as $c'=c/n$, the relative phase between the left and right handed components changes as the light travels a distance $L$ through the material.
\item That phase change causes a rotation in the polarization vector.
\end{enumerate}

The polarization vector rotates in proportion to the length of the material, the magnitude of the magnetic field, and a material dependent constant $C_{V}$ called the {\it Verdet constant}:

\begin{equation} \label{eq:faraday_rotation}
\phi = C_V B L
\end{equation}

Typically $C_V$ depends on the wavelength of the light and has a value of a
few $\mathrm{rad}/\mathrm{T}\cdot\mathrm{m}$. For the solenoid we'll use in
this lab, the field at the center is $B=\unit[11.1]{mT/A}$, and our
material, a special sort of glass, is $\unit[10]{cm}$ long. For a current
of \unit[0.1]{A}, we expect a rotation of a few$\times10^{-4}$ radians. This is a pretty small angle and it will require a special technique to detect.  

We are going to take the polarized laser light and direct it through the glass
rod, which is inserted into the center of the solenoid. The beam will then
pass through a second polarizer with the transmission axis at an angle $\theta$ with respect to the initial polarization of the laser. The intensity of the transmitted light will then depend on the sum of the angle $\theta$ and the additional rotation $\phi$ caused by the magnetic field:
\begin{eqnarray}
I & = & I_0 \cos^{2}(\theta+\phi)\\
  & = & I_0 \frac{1 + \cos(2\theta + 2\phi)}{2} \\
  & = & \frac{I_0}{2} + \frac{I_0}{2}\left[\cos2\theta\cos2\phi-\sin2\theta\sin2\phi \right] \\
  & \approx & I_0\left[\frac{1+\cos2\theta}{2} - \phi \sin2\theta  \right] \qquad (\phi^{2}\ll 1)\\
  & = & I_0\left[\frac{1+\cos2\theta}{2} - C_V L B \sin2\theta  \right] \label{eq:Ifinal}
\end{eqnarray}

We'll see the Faraday effect by observing changes in the intensity of light as we vary the magnetic field. But, there is a problem. The term involving $\phi$ is much smaller than the other terms and it can easily be buried in noise from a photo-detector. We'll use an experimental technique, called phase-locking, to get around the problem. We will then relate the changes in the intensity to changes in the angle $\phi$ using a special calibration dataset. Since we know the corresponding changes in $B$ we can use Eq.~\ref{eq:faraday_rotation} to extract $C_V$.

The phase-locking technique works in the following way. We'll vary the
magnetic field periodically with time as a sine wave, and then observe the
signal from the photodiode as a function of time.  The signal will look
like a large constant with a small sine wave "wobble" on it, along with some random
noise with a similar magnitude to the wobble. However, we can subtract off
the non time-varying portion of the signal, using a high pass filter. Then,
since we know the period and phase of the magnetic field, we can time our
observations to be exactly in sync with the magnetic field, and use this to
average out the noise. What's left over is the wobble in $I$, essentially
the $C_V L B \sin2\theta$ term. Knowing $B,L$ and $\theta$, we can determine $C_V$.

\section*{Experimental Setup}
The setup consists of:
\begin{itemize}
\item A solenoid to produce the magnetic field.
\item A glass rod inside the solenoid. You'll measure $C_V$ of this material.
\item A polarized HeNe laser, to shine light through the field.
\item A second polarizer, the ``analyzer'', to define the angle $\theta$.
\item A photodiode to detect the laser light after the coil and the analyzer.
\item A function generator and amplifier to supply current to the solenoid.
\item A digital multimeter (DMM) to measure the solenoid current.
\item A digital oscilloscope to read out the photodiode.
\end{itemize}

The experimental setup is shown in Fig.~\ref{fig:setup}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{./pdf_figs/faraday_setup} \caption{\label{fig:setup} The experimental setup.}
\end{figure}

\section*{Experimental Procedure}

\subsection*{Preliminaries}
\begin{description}
\item[Laser \& photodiode setup] The amplifier includes the laser power supply on the back. Plug the laser in, {\bf being careful to match colors between the cable and the power supply's connectors}. Align the laser so it travels down the center of the solenoid, through the glass rod, and into the center of the photodiode. Set the photodiode load resistor to $\unit[1]{k\Omega}$. Plug the photodiode into a DMM and measure DC voltage.

\item[Calibration: intensity vs. $\mathbf{\theta}$] We want to understand
	how the angle between the polarization vector of the laser light
	and the polarizer direction affects the intensity.  Vary the angle
	of the analyzing polarizer and use a white screen (e.g., piece of
	paper) to observe how the intensity of the transmitted light
	changes. Find the angles that give you  maximum and minimum transmission. Then, use the DMM to measure the photodiode output as a function of $\theta$, going between the maximum and minimum in $5^\circ$ steps. Tabulate this data. Graph it. What functional form should it have? Does it?

\item[Function Generator Setup] Plug the function generator output and its trigger (a.k.a.~ pulse) output into different channels on the scope. Trigger the scope on the trigger/pulse output from the function generator and look at the function generator signal. Modify the function generator to provide a \unit[200]{Hz} sine wave with an amplitude of about \unit[1]{V}. There is no more need to touch dials on the function generator.

\item[Amplifier setup] Now, plug the output of the function generator into the amplifier. Use a coaxial$\leftrightarrow$double-prong connector to feed the amplifier output into the scope, still triggering on the generator. Vary the amplifier dial setting and observe the change in the output. At some point, the output will become clipped, as the amplifier reaches its power output. Record this dial setting, and the amplitude of the sine wave, just as the clipping sets in. This is the maximum useful output from the amplifier. Turn the dial all the way off, and hook the amplifier up to the solenoid, with the DMM in series to measure AC current. Go back to the maximum setting and measure the current flowing through the solenoid. This is the maximum useful current.  You now have a time varying magnetic field in the solenoid and you can control its magnitude with the amplifier. 

\end{description}

\subsection*{Measuring Faraday Rotation}

\begin{description}
\item[Choice of $\theta$] You need to pick an angle $\theta$, which may seem arbitrary. There is a best choice. Examine Eq.~\ref{eq:Ifinal}.  Pick $\theta$ and be sure to tighten the thumbscrew.
\item[Faraday rotation] Plug the photodiode output into the scope, set the scope so its channel is DC coupled, and make sure that the ``probe'' setting is at 1x. Turn the amplifier dial about halfway to the maximum setting you found. Observe the photodiode trace on the scope, perhaps changing the volts/div setting so you can see the trace more clearly. What is the voltage? Record it.  The changing magnetic field should be causing a change in the polarization angle of the laser light, which should cause a sinusoidal time dependence to the photodiode signal, referred to as the "wobble."   Can you see any wobble? 
\item[AC coupling] The wobble is riding atop a large constant (DC) signal.
	The scope can remove the DC signal by ``AC coupling'' the
	photodiode channel. This essentially directs the scope input
	through a high pass filter.  Do this, and then set the photodiode
	channel to the \unit[2]{mV} setting.  You should now see a wobble.
	Vary the amplifier dial's setting and notice how the amplitude of the wobble changes. You are seeing the Faraday effect.
\item[Remove the noise] The signal is noisy, but now we'll really benefit
	from knowing the waveform that the function generator is producing.
	Because we trigger the scope on the function generator, the maxima
	and minima will, neglecting random noise, occur at the same point
	on the scope screen (and in its memory bank). The scope has a
	feature that allows you to average multiple triggers. Doing this mitigates the noise, since at each point on the trace we are taking a mean, and the uncertainty in a mean decreases as we increase the number of measurements $N$ as $1/\sqrt{N}$. Turn on the averaging feature by going to the ``Acquire'' menu. Observe how the averaged trace becomes more stable as you increase the number of traces being averaged. The larger the better, but $\sim$100 traces should be enough.
\item[Take measurements]  You should now systematically measure the amplitude of the wobble as a function of the current in the solenoid. There should be a linear relationship, which can be fit to extract $C_V$. Take about 10 measurements, evenly separated between the smallest current for which there is a measurable wobble, and the maximum you found earlier.  In each case, you want to start acquisition (Run/Stop on the scope), let the averaged signal converge onto a nice sine wave, stop acquisition and measure the amplitude of the signal using the scope's cursors. One measurement is shown in Fig.~\ref{fig:trace}. Record the negative and positive amplitudes $V_\mathrm{low}$ and $V_\mathrm{high}$ ($\unit[\pm 640]{\mu V}$ in Fig.~\ref{fig:trace}) and the peak to peak voltage $\Delta V$ ($\unit[1.28]{mV}$), along with the current in the coil -- $I_\mathrm{coil}$ . Estimate the uncertainty in your measurements.
\end{description}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{./pdf_figs/faraday_scope_trace}
\caption{\label{fig:trace} An example scope trace. The yellow curve is the
output of the photodiode, AC coupled and averaged over 128 traces. The blue
curve is the trigger output from the function generator and is being used
to trigger the scope readout so that it's in phase with the changing
magnetic field. The maximum and minimum amplitudes of the photodiode signal
are measured with the scope's cursors.}
\end{figure}

\subsection*{Data Analysis}
A variation in the angle $\phi$ is related to a variation in the magnetic field $B$ according to:
\begin{equation}\label{eq:faraday_fit}
\Delta \phi= C_V L \Delta B
\end{equation}
We want to use this equation to extract $C_V$. Both $\phi$ and $B$ vary with time as sine waves. We'll take $\Delta \phi$ and $\Delta B$ to be the amplitude of those waves (half the peak to peak). We didn't directly measure either quantity, but we can compute them. For $\Delta B$ it's easy: $\Delta B = \unit[11.1]{mT/A}\times \sqrt{2} I_\mathrm{coil}$, where the $\sqrt{2}$ accounts for the fact that DMMs measure the root-mean-squared (RMS) value of an AC signal, not the peak value. 

Figuring out $\Delta\phi$ is a little more difficult. We need to use our calibration dataset, which relates the photodiode signal to the angle between the laser polarization and the analyzer's orientation. From that dataset, you can estimate $\mathrm{d}V/\mathrm{d}\theta$ at the value of $\theta$ you are using in your experiment. Of course a change in the polarization of the laser, $\Delta\phi$ is equivalent to holding the laser polarization constant and changing the angle of the analyzer by $-\Delta\theta$.  So, we can calculate:
\begin{equation}
\Delta\phi = \left[\frac{\mathrm{d}V}{\mathrm{d}\theta}\right]^{-1} \frac{\Delta V}{2}
\end{equation}
The factor of two is because we defined $\Delta V$ as the peak to peak voltage.
%\begin{description}

Compute $C_V$ for a few points, along with the uncertainty. Complete the
analysis by fitting $\Delta\phi$ vs. $\Delta B$ to a straight line and then use Eq.~\ref{eq:faraday_fit} to extract $C_V$ and its uncertainty. Do you see a nice linear relationship? What should the intercept be? Is it what you expect? Are there any outlier points, particularly at the ends of your curve? You could do the same analysis using $V_\mathrm{low}$ or $V_\mathrm{high}$... does doing so yield the same answer for $C_V$?

\end{document}

