\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Blackbody Radiation}

%\date {}
%\maketitle \noindent
 \textbf{Experiment objectives}: explore radiation from objects at certain temperatures,
    commonly known as ``blackbody radiation''; make measurements testing the Stefan-Boltzmann
    law in high- and low-temperature ranges; measure the inverse-square law for thermal radiation.
\vspace{0.25in}
\hrule
\textbf{A brief writeup}: Your instructor may tell you to do a brief writeup of this experiment. If so you will take data as instructed in the pages that follow. However when it comes time to do the analysis and write the report you will follow the {\it Instructions for a brief writeup} on page~\pageref{pag:briefwriteupBB}.
\hrule

\section*{Theory}

    A familiar observation to us is that dark-colored objects
    absorb more thermal radiation (from the sun, for example) than
    light-colored objects. You may have also observed that a good
    absorber of radiation is also a good emitter (like
    dark-colored seats in an automobile). Although we observe
    thermal radiation (``heat'') mostly through our sense of touch,
    the range of energies at which the radiation is emitted can
    span the visible spectrum (thus we speak of high-temperature
    objects being ``red hot'' or ``white hot''). For temperatures
    below about $600^{\circ}C$, however, the radiation is emitted in the
    infrared, and we cannot see it with our eyes, although there
    are special detectors (like the one you will use in this lab)
    that can measure it.

    An object which absorbs all radiation incident on it is known
as an ``ideal blackbody''.  In 1879 Josef Stefan found an empirical
relationship between the power per unit area radiated by a blackbody and the
temperature, which Ludwig Boltzmann derived theoretically a few years later.
This relationship is the {\bf Stefan-Boltzmann law:}
\begin{equation}\label{SBl}
 S =\sigma T^4
\end{equation}
where $S$ is the radiated power per unit area ($W/m^2$), $T$ is the temperature (in Kelvins), and $\sigma=5.6703
\times 10^{-8} W/m^2K^4$ is the Stefan's constant.

Most hot, opaque objects can be approximated as blackbody emitters, but the most ideal blackbody is a closed volume (a cavity) with a very small hole in it. Any radiation entering the cavity is absorbed by the walls, and then is re-emitted out. Physicists first tried to calculate the spectral distribution of the radiation emitted from the ideal blackbody using {\it classical thermodynamics}. This method involved finding the number of modes of oscillation of the electromagnetic field in the cavity, with the energy per mode of oscillation given by $kT$. The classical theory gives the {\bf Rayleigh-Jeans law:}
\begin{displaymath}
 u(\lambda,T) = \frac{8\pi kT}{\lambda^4}
\end{displaymath}
where $u(\lambda)(J/m^4)$ is the spectral radiance (energy radiated per unit area at a single wavelength or frequency), and $\lambda$ is the wavelength of radiation. This law agrees with the experiment for radiation at long wavelengths (infrared), but predicts that $u(\lambda)$ should increase infinitely at short wavelengths. This is not observed experimentally (Thank heaven, or we would all be constantly bathed in ultraviolet light-a true ultraviolet catastrophe!). It was known that the wavelength distribution peaked at a specific temperature as described by {\bf Wien's law:}
\begin{displaymath}
 \lambda_{max}T = 2.898\times 10^{-3} m\cdot K
\end{displaymath}
and went to zero for short  wavelengths.

    The breakthrough came when Planck assumed that the energy of the oscillation modes can only take on discrete values rather than a continuous distribution of values, as in classical physics. With this assumption, Planck's law was derived:
\begin{displaymath}
u(\lambda,T)=\frac{8\pi hc\lambda^{-5}}{e^{hc/\lambda kT}-1}
\end{displaymath}
where $c$ is the speed of light and $h=6.626076\times 10^{-34} J\cdot s$ is the Planck's constant. This proved to be the correct description.


\begin{boxedminipage}{\linewidth}
\textbf{Sometimes physicists have to have crazy ideas!} \\
%
``\emph{The problem of radiation-thermodynamics was solved by Max Planck, who
was a 100 percent classical physicist (for which he cannot be blamed). It was
he who originated what is now known as {\it modern physics}. At the turn of the
century, at the December 14, 1900 meeting of the German Physical Society,
Planck presented his ideas on the subject, which were so unusual and so
grotesque that he himself could hardly believe them, even though they caused
intense excitement in the audience and the entire world of physics}.''

From George Gamow, {\it ``Thirty Years that Shook Physics, The Story of Quantum Physics''}, Dover Publications,
New York, 1966.
\end{boxedminipage}


\subsection*{Safety}
The Stefan lamp and thermal cube will get very hot - be careful!!!

\section*{Thermal radiation rates from different surfaces}
\textbf{Equipment needed}: Pasco Radiation sensor, Pasco Thermal Radiation Cube, two multimeters, window glass.

Before starting actual experiment take some time to have fun with the thermal radiation sensor. Can you detect your lab partner? What about people across the room? Point the sensor in different directions and see what objects affect the readings. \textbf{These exercises are fun, but you will also gain important intuition about various factors which may affect the accuracy of the measurements!}



\begin{boxedminipage}{\linewidth}
\textbf{How does the radiation sensor work?} \\
\vspace{0.25in}

\includegraphics[height=2.5in]{./pdf_figs/thermopile} \\

\vspace{0.25in}

Imagine a metal wire connected to a cold reservoir at one end and a hot reservoir at the other.  Heat will flow between the ends of the wire, carried by the electrons in the conductor, which will tend to diffuse from the hot end to the cold end.  Vibrations in the conductor's atomic lattice can also aid this process. This diffusion causes a potential difference between the two ends of the wire. The size of the potential difference depends on the temperature gradient and on details of the conductive material, but is typically in the few 10s of $\mu V/ K$. A thermocouple, shown on the left, consists of two different conductive materials joined together at one end and connected to a voltmeter at the other end. The potential is, of course, the same on either side of the joint, but the difference in material properties causes $\Delta V=V_1 - V_2 \neq 0$. This $\Delta V$ is measured by the voltmeter and is proportional to $\Delta T$.  Your radiation sensor is a thermopile, simply a ``pile'' of thermocouples connected in series, as shown at the right. This is done to make the potential difference generated by the temperature gradient easier to detect. 
\end{boxedminipage}


\begin{figure}[h]
\includegraphics[height=2.5in]{./pdf_figs/bbx}
\caption{\label{bbx}Thermal radiation setup}
\end{figure}

\begin{enumerate}
\item Connect the two multimeters and position the sensor as shown in Fig.~\ref{bbx}. The multimeter attached to the cube should be set to read resistance while the one attached to the infrared radiation sensor will monitor the potential (in the millivolt range). Make sure the shutter on the sensor is pushed all the way open!
\item Before turning on the cube, measure the resistance of the thermistor at room temperature, and obtain the room temperature from the instructor.  You will need this information for the data analysis.
%
\item Turn on the thermal radiation cube and set the power to ``high''. When the ohmmeter reading decreases to 40 k$\Omega$ (5-20 minutes) set power switch to ``$8$''. (If the cube has been preheated, immediately set the switch to ``$8$''.)
\\
\begin{boxedminipage}{\linewidth}
\textbf{Important}: when using the thermal radiation sensor, make each reading quickly to keep the sensor from heating up. Use both sheets of white isolating foam (with the silvered surface facing the lamp) to block the sensor between measurements.
\\
\textbf{Sensor calibration}: To obtain the radiation sensor readings for radiated power per unit area $S$ in the correct units ($W/m^2$), you need to use the voltage-to-power conversion factor $22~mV/mW$, and the area of the sensor $2mm\times2mm$:
\begin{displaymath}
S[W/m^2]=\frac{S[mV]}{22 [mV/mW]}\cdot 10^{-3}\cdot \frac{1}{4\cdot
10^{-6}[m^2]}
\end{displaymath}
\end{boxedminipage}
%
%
\item When the cube has reached thermal equilibrium the ohmmeter will be
	fluctuating around a constant value. Record the resistance of the
	thermistor in the cube and determine the approximate value of the
	temperature using the data table in Fig~\ref{tcube}. Use the
	radiation sensor to measure the radiation emitted from the four
	surfaces of the cube. Place the sensor so that the posts on its end
	are in contact with the cube surface (this ensures that the
	distance of the measurement is the same for all surfaces) and
	record the sensor reading.  Each lab partner should make an
	independent measurement.

\item  Place the radiation sensor approximately 5~cm from the black surface of the radiation cube and record its reading. Place a piece of glass between the sensor and the cube. Record again. Does window glass effectively block thermal radiation?  Try observing the effects of other objects, recording the sensor reading as you go.

\end{enumerate}

%Below is a possible way to record the results of these measurements in your lab
%book. Don't forget to specify the uncertainties of your measurements!
%
% \noindent\\
%\begin{tabular}{lr}
%Thermistor reading (room temperature)=&$\underline{\hskip 1in}\Omega$\\
%   T (room temperature)=&$\underline{\hskip 1in}K$
%\end{tabular}
%
%\begin{tabular}{|p{13mm}|p{13mm}|p{13mm}|p{13mm}|p{13mm}|p{13mm}|p{13mm}|
%p{13mm}|}
%\hline
%\multicolumn{2}{|l|}
%{\bf Power=5.0}&\multicolumn{2}{|l|}{\bf  Power=6.5} &\multicolumn{2}{|l|}
%{\bf Power=8.0}&\multicolumn{2}{|l|}{\bf Power= ``High''}\\ \hline
%\multicolumn{2}{|l|}{Th. Res. $\underline{\hskip .4in}\Omega$}&
%\multicolumn{2}{|l|}{Th. Res. $\underline{\hskip .4in}\Omega$}&
%\multicolumn{2}{|l|}{Th. Res. $\underline{\hskip .4in}\Omega$}&
%\multicolumn{2}{|l|}{Th. Res. $\underline{\hskip .4in}\Omega$}\\ \hline
%\multicolumn{2}{|l|}{Temp. $\underline{\hskip .5in}K$}
%&\multicolumn{2}{|l|}{Temp. $\underline{\hskip .5in}K$}
%&\multicolumn{2}{|l|}{Temp. $\underline{\hskip .5in}K$}
%&\multicolumn{2}{|l|}{Temp. $\underline{\hskip .5in}K$}\\ \hline
%{\bf Surface}&{\bf Sensor Reading (mV)}&
%{\bf Surface}&{\bf Sensor Reading (mV)}&
%{\bf Surface}&{\bf Sensor Reading (mV)}&
%{\bf Surface}&{\bf Sensor Reading (mV)}\\\hline
%Black&&Black&&Black&&Black&\\\hline
%White&&White&&White&&White&\\\hline
%Polished Aluminum&&Polished Aluminum&&Polished Aluminum&&Polished Aluminum&\\
%\hline
%Dull Aluminum&&Dull Aluminum&&Dull Aluminum&&Dull Aluminum&\\ \hline
%\end{tabular}

%Plot the measured radiated power as function of temperature for different
%surfaces.
Use your data to address the following questions in your lab report:
\begin{enumerate}
\item Is it true that good absorbers of radiation are good emitters?
\item Is the emission from the black and white surface similar?
\item Do objects at the same temperature emit different amounts of radiation?
\item Does glass effectively block thermal radiation? Comment on the other objects that you tried.
\end{enumerate}


\begin{figure}
\includegraphics[height=4in]{./pdf_figs/tcube}
\caption{\label{tcube}Resistance vs. temperature for the thermal radiation cube}
\end{figure}
\begin{figure}
\includegraphics[height=3.5in]{./pdf_figs/bbht}
\caption{\label{bbht}Lamp connection for high-temperature Stefan-Boltzmann
setup}
\end{figure}

\subsection*{Tests of the Stefan-Boltzmann Law}
\subsubsection*{ High temperature regime}
\textbf{Equipment needed}: Radiation sensor, 3 multimeters, Stefan-Boltzmann
Lamp, Power supply.

\begin{enumerate}
\item  \textbf{Before turning on the lamp}, measure the resistance of the filament of the Stefan-Boltzmann lamp at room temperature. Record the room temperature, visible on the wall thermostat and on the bench mounted thermometers in the room.

%
%\begin{tabular}{lr}
%  T (room temperature)=&$\underline{\hskip .7in}K$\\
% Resistance of filament (room temperature)=&$\underline{\hskip .7in}$
%\end{tabular}

\item Connect a multimeter as voltmeter to the output of the power supply.
	{\bf Important:} make sure it is in the {\bf voltmeter mode}.
	Compare voltage readings provided by the power supply and the multimeter. 
	Which one is the correct one? Think about your measurement
	uncertainties.

\item Set up the equipment as shown in Fig. \ref{bbht}. VERY IMPORTANT:
	make all connections to the lamp when the power is off. Turn the
	power off before changing/removing connections. The voltmeter
	should be directly connected to the binding posts of the
	Stefan-Boltzmann lamp. In this case the multimeter voltmeter has
	direct access to the voltage drop across the bulb, while the power
	supply voltmeter reads an extra voltage due to finite resistance of
	the current meter. Compare readings on the multimeter and the power
	supply current meters.  Which one is the correct one? Think about
	your measurement uncertainties.

\item Place the thermal sensor at the same height as the filament, with the front face of the sensor approximately 6 cm away from the filament (this distance will be fixed throughout the measurement). Make sure no other objects are viewed by the sensor other than the lamp.
%
\item  Turn on the lamp power supply. Set the voltage, $V$, in steps of one
	volt from 1-12 volts. At each $V$, record the ammeter (current)
	reading from the lamp and the voltage from the radiation sensor.
	Calculate the resistance of the lamp using Ohm's Law and determine
	the temperature $T$ of the lamp from the table shown in Fig.
	\ref{w_res:fig}. You can use a table to record your data similar to
	the sample table~\ref{tbl:sampla_data_table}.

\end{enumerate}

\begin{figure}[h]
\includegraphics[width=\columnwidth]{./pdf_figs/w_res}
\caption{\label{w_res:fig}Table of tungsten's resistance as a function of temperature.}
\end{figure}


\begin{table}[ht]
\begin{center}
\begin{tabular}{|p{20mm}|p{20mm}|p{20mm}|p{20mm}|p{20mm}|p{20mm}|}\hline
\multicolumn{3}{|c|}{Data($\pm$ error)}& \multicolumn{3}{|c|}{Calculations}\\
\hline $V$((volts)&$I$(amps)&$Rad(mV)$&$R$(ohms)&$T(K)$&$T^4(K^4)$\\\hline
1.00&&&&&\\\hline \dots &&&&&\\\hline&&&&&\\\hline
\end{tabular}
\caption{
	Sample table for recording your data. 
	\label{tbl:sampla_data_table} % spaces are big no-no withing labels
}
\end{center}
\end{table}

In the lab report plot the reading from the radiation sensor (convert to $W/m^2$) (on the y axis) versus the temperature $T$ on the x axis. According to the Stefan-Boltzmann Law, the data should show a $S\propto T^4$ behavior. Do a fit to $S=C T^n$ where $C$ and $n$ are free parameters. Report the value of each parameter and its uncertainty. Does $n=4$, within uncertainty? How does it compare to the accepted value of Stefan's constant?  

Examine the fit.  How many points are within 1 uncertainty ($\sigma$) of the line?  How many are between 1 and 2 $\sigma$? For a good fit, about 2/3 of the points should be within $1\sigma$ of the line, and the other 1/3 between 1-$2\sigma$, with perhaps one point between 2 and $3\sigma$, if we have about 20 points. That latter number scales up with the number of points. Unless you have a few 100 datapoints, a good fit should not have any points further than $3\sigma$. Based on this criteria, was is your fit good?  Do the points seem to systematically differ from the fit line in a particular region?  Can you think of a reason why that would be?

The parameter $C$ is equal to $A\sigma$. For an ideal black body $A=1$, but in your case it will be very different. $A<1$ means the object does not absorb (or emit) all the radiation incident on it (this object only radiates a fraction of the radiation of a true blackbody). The material lampblack has $A=0.95$ while tungsten wire has $A=0.032$ (at $30^{\circ} C$) to 0.35 (at $3300^{\circ}C$). Using the result of your fit, and assuming we know the Stephan-Boltzman constant $\sigma$ by some other means, what is $A$ and what is the uncertainty on it?  Is it consistent with tungsten? What else could be affecting this measurement?


\section*{Test of the inverse-square law}
\textbf{Equipment needed}: Radiation sensor, Stefan-Boltzmann lamp, multimeter,
power supply, meter stick.
\begin{figure}
\includegraphics[height=2.5in]{./pdf_figs/bb31}
\caption{\label{bb31}Inverse square law setup}
\end{figure}
A point source of radiation emits that radiation according to an inverse square
law: that is, the intensity of the radiation in $(W/m^2)$ is proportional to
the inverse square of the distance from that source. You will determine if this
is true for a lamp.

\begin{enumerate}
\item  Set up the equipment as shown in Fig. \ref{bb31}. Tape the meter stick to the table. Place the Stefan-Boltzmann lamp at one end, and the radiation sensor in direct line on the other side. The zero-point of the meter stick should align with the lamp filament (or, should it?). Adjust the height of the radiation sensor so it is equal to the height of the lamp. Align the system so that when you slide the sensor along the meter stick the sensor still aligns with the axis of the lamp. Connect the multimeter (reading millivolts) to the sensor and the lamp to the power supply.
\item  With the {\bf lamp off}, slide the sensor along the meter stick. Record the reading of the voltmeter at 10 cm intervals. Average these values to determine the ambient level of thermal radiation. You will need to subtract this average value from your measurements with the lamp on.
\item  Turn on the power supply to the lamp. Set the voltage to
	approximately 10 V. {\bf Do not exceed 13 V!} Adjust the distance
	between the sensor and lamp from 2.5-100 cm and record the sensor
	reading. \textbf{Before the actual experiment think carefully about
	at what distances you want to take the measurements. Is taking them
	at constant intervals the optimal approach? At what distances would you expect
	the sensor reading change more rapidly?}

 \item  Make a plot of the corrected radiation measured from the lamp versus the distance from the lamp to the sensor $x$.  Fit the data to $S= B + C x^n$ where $B$, $C$ and $n$ are free parameters. Based on the criteria in the previous section, how good is the fit? 


\item What are the values of $B$, $C$ and $n$ (and, of course, their uncertainties)?  How do they agree with what you expect from the inverse square law?

\item  Can the lamp be considered a point source? If not, how could this affect your measurements?

\end{enumerate}

\hrule

{\huge Your instructor may have told you to do a ``brief writeup'' of this experiment. If so, please see the instructions starting on the next page (page~\pageref{pag:briefwriteupBB}).}

\hrule

\newpage

\section*{Instructions for a brief writeup}\label{pag:briefwriteupBB}

This lab is reasonably straightforward. There is much data to take and several fits, but describing the setup and providing an introduction seems less necessary than for some of the others. Therefore, just answer the following questions/bullet points in your report. {\bf Please restate each one so it's easy for us to follow along.} {\it Refer the the relevant section earlier in the manual for additional discussion. This is just a bullet point summary of the deliverables.}

\subsection*{Thermal radiation rates from different surfaces}
First, tabulate your data and include it (with label and caption).  Then use your data to address the following questions:
\begin{enumerate}
\item Is it true that good absorbers of radiation are good emitters? \textit{Hint:} use what you know about absorption of visible light for these surfaces
\item Is emission from the black and white surfaces similar?
\item Do objects at the same temperature emit different amounts of radiation?
\item Is glass effective in blocking thermal radiation? What about other objects that you tried? 
\end{enumerate}

\subsection*{Tests of the Stephan-Boltzmann Law}


\begin{enumerate}
\item Tabulate your data as in Tab 4.1 and include it.
\item Estimate and report uncertainties on the voltage, current, and radiation sensor.
\item Plot the readings from the radiation sensor (y-axis) vs $T$ (x-axis) with uncertainties and fit to $S=C T^n$. Include the plot in your report.
\item Is it a good fit? 
\item Does the Stephan-Boltzman equation seem to hold? Does $n=4$, within uncertainties?
\item What value (and uncertainty) do your data imply for $A$? Is it consistent with tungsten? What else could be affecting your measurement? 


\end{enumerate}

\subsection*{Test of the inverse square law}

\begin{enumerate}
\item Tabulate your data and include it in the report. Be sure to include the lamp-off data, used to correct the lamp-on data for ambient radiation.
\item Estimate and report uncertainties on the distance between the sensor and lamp,  and on the readings from the radiation sensor.
\item Make a plot of the corrected radiation (y-axis, with uncertainty) vs distance (x-axis) and fit it to $S= B + C x^n$. Include it in your report. 
\item How good is the fit?
\item Does $n=-2$ within uncertainty? Do the data confirm the inverse square law?
\item What are $B$ and $C$?  What did you expect?  Comment on any discrepancies. 
\end{enumerate}

%
%\item The Blackbody Spectrum Equipment: Spectrometer, computer, high
%temperature source.
%\begin{itemize}
%
%\item  There are two spectra at the end of this lab.
%From Wien's law, estimate the temperature of the sources.
%\begin{displaymath}
%T_{ estimate}=\underline{\hskip .75in}K
%\end{displaymath}
%\begin{displaymath}
%T_{ estimate}=\underline{\hskip .75in}K
%\end{displaymath}
%
%
%\item  On a separate graph, plot the expected spectrum
%from the Rayleigh-Jeans law and Planck's law. Which law best
%represents the spectrum acquired above?
%\end{itemize}
%
%Addendum to Blackbody Radiation Handout
%\section*{Thermal Radiation rates from Different Surfaces}
%
%\begin{itemize}
%\item Make sure the shutter on the Sensor is pushed all the
%way open!
%\item Make sure the temperature is stable when you begin readings from
%surfaces of Cube (you may have to wait at least 5 minutes between
%temperature changes).
%\item Make the readings quickly!
%\end{itemize}
%
%
%
%\section*{Inverse Square Law}
%
%Make sure that you keep the Sensor in line with the filament as you
%slide it (and you do not introduce an angle).
%




%\subsection*{The Blackbody Spectrum}
%
%In Fig. \ref{Sun} is an approximate spectrum of the sun. Determine
%the approximate temperature of the sun from this spectrum using Wien's
%law. $T=\underline{\hskip .75in}$.
%In Fig. \ref{Cobe} is the spectrum for the microwave background
%assumed to arise from the time when the photons decoupled from the
%charged particles, i.e., when most free electrons became bound.
%Determine the temperature of the microwave background. Note that what
%is plotted is waves/cm, not cm. If the present size of the visible
%universe is $13\cdot 10^9$ light years. How large was the visible
%universe when the decoupling took place. Hint: $R_u$ has experienced the
%same expansion as the wavelength and the temperature at decoupling must
%correspond to about 10 eV.
%\begin{itemize}
%\item Extra credit: what is the accepted value
%for the
%temperature of the surface of the sun?
%\item How does your extracted value
%compare?
%\item Include a copy of this spectrum in your lab report.
%On a
%separate graph, plot Planck's Law and the Rayleigh-Jeans Law for this
%same temperature.
%\item Which law does the solar spectrum appear to behave?
%\end{itemize}


%
%
%
%\begin{figure}
%\includegraphics[height=2.5in]{LinearSp.eps}
%\caption{\label{Sun}Approximate Sun Spectrum}
%\end{figure}
%\begin{figure}
%\includegraphics[height=2.5in]{cobespc.eps}
%\caption{\label{Cobe}Cobe: Cosmic Black Body Spectrum}
%\end{figure}
%\begin{figure}
%
%\newpage

\end{document}
