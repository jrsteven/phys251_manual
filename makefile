all:manual.pdf

%.dvi: %.tex
	latex $<
	latex $<

%.ps: %.dvi
	dvips -t letter $< -o $@
	rm $<

%.pdf: %.tex
	pdflatex $<
	pdflatex $<
