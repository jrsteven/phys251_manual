\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Photoelectric Effect}

%\date {}
%\maketitle \noindent
 \textbf{Experiment objectives}: measure the ratio of Planck's constant to the electron charge $h/e$ using the photoelectric effect.

\section*{History}


    The photoelectric effect and its understanding was an important step in the development
    of quantum mechanics. You probably know that Max Planck was the first to postulate that
    light was made up of discrete packages of energy. At that time it was a proposed hypothetical light property that
    allowed for the proper description of black body radiation. Several years after Planck made this
    suggestion, Albert Einstein refined the idea to explain the
    strange features of electrons which are emitted from metal
    surfaces after absorbing energy from light. Later he received a Nobel prize for this work.

\section*{Theory}

\begin{figure}[h]
\centering \includegraphics[height=3in]{./pdf_figs/pefig1} \caption{\label{pefig1} A simplified schematic of the photoelectric apparatus.}
\end{figure}
 Consider an apparatus as outlined in Figure \ref{pefig1} (it is the
apparatus which Heinrich Hertz used to inadvertently discover the ``photoelectric effect''). Light of a
frequency $\nu$ strikes a cathode, causing electrons to be emitted with velocity $v$. A positive voltage applied
between the anode and the cathode can accelerate emitted electrons towards the positive anode, producing an
electrical current in the circuit. A reverse bias potential applied between the anode and cathode will slow down
the electrons, and even stop them from reaching the anode by matching their kinetic energy. This is the way to
carefully measure the kinetic energies of the photoelectrons.
\begin{equation}
KE_{max} = (\frac{1}{2}mv^2)_{max} = eV_0
\end{equation}
%
where $KE_{max}$ is the maximum kinetic energy, $m$ and $e$ are the mass and the charge of an electron, and
$V_0$ is a potential required to stop the electrons (known as the stopping potential).

From the point of view of the wave theory of light, the energy carried by the light wave is proportional to its
intensity and independent of light frequency. Thus, it was logical to expect that stronger light should
increase the energy of photoelectrons. However, the experiments of Philipp Lenard in 1900 showed something
different: although the maximum current increased with light intensity, the stopping potential $V_0$ was
independent of light intensity. This meant that increasing the rate of energy falling onto the cathode does not
increase the maximum energy of the electrons. This was quite a surprising result from the classical point of
view.

This ``controversy'' was elegantly resolved five years later by Albert Einstein, who postulated that light
arrived in discrete quanta known as ``photons'', and each photon led to an emission of a single electron. The
energy of each photon is determined by the frequency of light - $h\nu$. Thus the energy of an emitted
photoelectron (and therefore the value of the stopping potential $V_0$) is determined by the frequency of an
individual incident photon:
\begin{equation}
\label{eq:pe}
 eV_0= \frac{1}{2}mv^2 = h\nu-\phi = \frac{hc}{\lambda} - \phi
\end{equation}
where $\phi$ is known as the ``work function''  - the amount of energy needed to free the electron from the
cathode. The value of the work energy is a property of the cathode material. Also, it is now clear why the
intensity of light does not affect the stopping potential: more intense light has higher photon flux and thus
increases the number of emitted photoelectrons, but the energy of each
electron is determined only by the energy
of a single photon, and that depends only on light frequency  $\nu$.

\begin{boxedminipage}{\linewidth}
\textbf{Where do all those electrons come from?} \\
%
At Jefferson Lab in nearby Newport News, high-energy electrons (GeV range) are
aimed at targets to probe the fundamental properties of quarks. But where do
all those electrons come from? From the photoelectric effect, of course! A
laser beam is aimed at a cathode consisting of a material like copper or GaAs.
The frequency of the laser is such that electrons will be emitted from the
cathode. In this way, high current electron beams are produced. One benefit of
using a laser and the photoelectric effect is that if the laser is ``pulsed'' in
time, the electron beam will be also (this allows synchronized timing in the
experiments). Also, the polarization of the laser can be manipulated to allow
for the emission of electrons with particular spin. See: www.jlab.org
\end{boxedminipage}

\section*{Procedure}

\textbf{Equipment needed}: Pasco photoelectric apparatus, Hg lamp, digital
voltmeter. The Pasco apparatus contains a circuit which automatically determines the stopping potential, which you measure off of a voltmeter, so there is no need to adjust the stopping potential yourself or measure the current (lucky you!). Read the brief description of its operation in the appendix.

\begin{figure}
\centering \includegraphics[width=\linewidth]{./pdf_figs/pefig3} \caption{\label{pefig3}
The Pasco photoelectric effect setup.}
\end{figure}

Set up the equipment as shown in Fig. \ref{pefig3}. First, place a lens/grating assembly in front of the Mercury
lamp, and observe a dispersed spectrum on a sheet of paper, as shown in Fig.~\ref{fig:mercury_spectrum}. Identify spectral
lines in both the first and the second diffraction orders on both sides. Keep in mind that the color
``assignment'' is fairly relative, and make sure you find all lines mentioned in the table in Fig.~\ref{fig:mercury_spectrum}.
Often the first/second order lines on one side are brighter than on the other - check your apparatus and
determine what orders you will be using in your experiment.

After that, install the $h/e$ apparatus and focus the light from the mercury vapor light source onto the slot in
the white reflective mask on the $h/e$ apparatus. Tilt the light shield of the apparatus out of the way to
reveal the white photodiode mask inside the apparatus. Slide the Lens/Grating assembly forward and back on its
support rods until you achieve the sharpest image of the aperture centered on the hole in the photodiode mask.
Secure the Lens/Grating by tightening the thumbscrew. Align the system by rotating the $h/e$ apparatus on its
support base so that the same color light that falls on the opening of the light screen falls on the window in
the photodiode mask, with no overlap of color from other spectral
 lines. Return the light shield to its closed position.

Check the polarity of the leads from your digital voltmeter (DVM), and connect them to the OUTPUT terminals on
the $h/e$ apparatus.


\section*{Experimental procedure}



\section*{Part A: The dependence of the stopping
    potential on the intensity of light}
\begin{enumerate}
\item Adjust the $h/e$ apparatus so that one of the blue first order spectral lines falls upon the opening of the mask of the photodiode. 
\item Press the instrument discharge button, release it, and observe how much time\footnote{Use the stopwatch feature of your cell phone. You don't need a precise measurement.} is required to achieve a stable voltage.

\item It's important to check our data early on to make sure we are not off
	in crazyland. This procedure is technically known as ``sanity
	checking''. According to equation Eq.~\ref{eq:pe} the frequency
	(or, better, wavelength) of light can be found from the measured
	voltage with knowledge of the work function $\phi$. Unfortunately,
	we don't know the work function. 
	You'll measure it in the second part of the lab. 
\begin{itemize}
	\item For now, switch to violet color and measure the stopping
		potential for this color.
	
	\item compute $h c$ using  $\Delta
		V_0$ between the two measurements and  values of light
		wavelength for these
		two cases.  Does your data agree with the accepted value?  For this
		computation, it might be helpful to realize that $eV_0$ is the electron
		charge times the voltage. If you measured $V_0=\unit[1]{V}$ then $eV_0 =1$
		``electron-volt'' or ``eV''.  This is why electron-volt units are useful.
		Also, $hc=\unit[1240]{nm~eV}$.
\end{itemize}

\item Go back to the original spectral line.
\item Place the variable transmission filter in front of the white reflective mask (and over the colored filter, if one is used) so that the light passes through the section marked 100\% and reaches the photodiode. Record the DVM voltage reading and time to recharge after the discharge button has been pressed and released.
\item Do the above measurements for all sections of the variable transmission filter. 

\end{enumerate}

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{./pdf_figs/mercury_diffraction_spectrum} \caption{\label{fig:mercury_spectrum}
The mercury lamp visible diffraction spectrum.}
\end{figure}

\section*{Part B: The dependence of the stopping potential on the frequency of light}

In this section you'll measure the stopping power for the five different emission lines of Mercury to demonstrate that the stopping power depends on, and is a measurement of, the wavelength (and frequency).  Your measurements will consist of the stopping power for each of the five Mercury lines, measured for both first and second orders.  This is a total of 10 data points.  You'll then analyze the data by fitting stopping power vs frequency to extract Planck's constant and the work function. That procedure is described in more detail in the next section. 

There is a particular issue with the yellow and green lines that, if left unaddressed, can corrupt your measurements. The procedure below walks you through the measurements of those two lines lines, highlighting a few a few places that systematic problems can creep in if we are not careful.  In particular, you will be using yellow and green filters to mitigate one source of systematic bias.  

\begin{enumerate}


\item Adjust the $h/e$ apparatus so that the 1st order yellow colored band falls upon the opening of the mask of the photodiode. Take a quick measurement with the lights on and record the DVM voltage. Do the same for the green and blue lines. 

\item Now, with the lights still on, repeat the yellow and green measurements with the yellow and green filters attached to the h/e detector. What do you see now?  In order to explain your observations try holding the filters just in front of the diffraction grating and look at the pattern on the screen.

\item Now turn the lights off.  This will require coordinating with other groups and the instructor. Repeat the measurements with and without the filters. Are the two sets of measurements the same?  Form a hypothesis for why or why not.

\item Now, you need to decide on the best procedure for collecting the data. Lights on or lights off? Filters or no filters? Discuss amongst your group and with the instructor or TA.   You will need to measure both the first and second order lines. This is all five colors, for a total of 10 data points.

Note: When collecting the data in the steps above, be sure to estimate uncertainties on the stopping power. These come in part from the digital voltmeter, but also from the repeatability of your measurements, how well lines are centered on the slit (perhaps off-center could be better?) and so on.

\end{enumerate}

\section*{Analysis}
\section*{Classical vs. Quantum model of light}
\begin{enumerate}
\item Describe the effect that passing different amounts of the same colored light through the Variable Transmission Filter has on the stopping potential and thus the maximum energy of the photoelectrons, as well as the charging time after pressing the discharge button.

\item Describe the effect that different colors of light had on the stopping potential and thus the maximum energy of the photoelectrons.

\item Defend whether this experiment supports a classical wave or a quantum model of light based on your lab results.
\end{enumerate}

Read the theory of the detector operation in the Appendix, and explain why there is a slight drop in the measured stopping potential as the light intensity is reduced. 

\section*{The relationship between Energy, Wavelength and Frequency}

\begin{enumerate}
\item Use the table in Fig.~\ref{fig:mercury_spectrum} to find the exact frequencies and wavelengths of the spectral lines you used and plot the measured stopping potential values versus light frequency for measurements of the first and second order lines.

\item Fit your data according to $eV_0 = h\nu-\phi$, extracting values for the slope and intercept. {\it Note, this fitting step takes the measurement uncertainties on the stopping power and propagates them to the slope and intercept.} It's important to do this step with Igor, Matlab, or some other tool which can compute a $\chi^2$, minimize it with respect to the fit parameters, and then report the parameter uncertainties.  From the slope, determine  $h$ using $e=1.6\cdot10^{-19}$~C. Find the average value and uncertainty on the average. Does your value agree with the accepted value of  $h=6.62606957(29) \times 10^{-34}$J$\cdot$s within uncertainty?

\item From the intercepts, find the average value and uncertainty of the work function $\phi$. Look up some values of work functions for typical metals. Is it likely that the detector material is a simple metal?

\end{enumerate}

\section*{Appendix: Operation principle of the stopping potential detector}

The schematic of the apparatus used to measure the stopping potential is shown in Fig.~\ref{pefig5}. Monochromatic light falls on the cathode plate of a vacuum photodiode tube that has a low work function $\phi$. Photoelectrons ejected from the cathode collect on the anode. The photodiode tube and its associated electronics have a small capacitance which becomes charged by the photoelectric current. When the potential on this capacitance reaches the stopping potential of the photoelectrons, the current decreases to zero, and the anode-to-cathode voltage stabilizes. This final voltage between the anode and cathode is therefore the stopping potential of the photoelectrons.

\begin{figure}[h]
\centering \includegraphics[width=0.7\linewidth]{./pdf_figs/pe_det}
\caption{\label{pefig5} The electronic schematic diagram of the $h/e$
apparatus.}
\end{figure}

To let you measure the stopping potential, the anode is connected to a built-in
differential amplifier with an ultrahigh input impedance ($> 10^{13}~\Omega$),
and the output from this amplifier is connected to the output jacks on the
front panel of the apparatus. This high impedance, unity gain ($V_{out}/V_{in}
= 1$) amplifier lets you measure the stopping potential with a digital
voltmeter.

Due to the ultra high input impedance, once the capacitor has been charged from the photodiode current, it takes a long time to discharge this potential through some leakage. Therefore a shorting switch labeled ``PUSH TO Zero'' enables the user to quickly bleed off the charge. While the impedance of the unity gain amplifier is very high ($10^{13}~\Omega$), it is not infinite and some charge gradually leaks off. This effect can bias the measured stopping power for low intensity light sources.

\newpage
\section*{Sample data tables:}

\subsection*{Part A}

{\large
%\begin{tabular}{|p{27mm}|p{27mm}|p{27mm}|p{27mm}|}
\begin{tabular}{|c|c|c|c|}
\hline
 Color  & \%Transmission & Stopping Potential &
Approx. Charge Time \\
\hline
&100&&\\\hline
&80&&\\ \hline
&60&&\\ \hline
&40&&\\ \hline
&20&&\\ \hline
\hline
\end{tabular}
}

\subsection*{Part B}

{\large
\begin{tabular}{|c|c|c|c|}
\hline
Color&$\lambda$ (nm) &$\nu$  ($10^{14}Hz$) &
Stopping Potential (V) \\
\hline Yellow&&&\\\hline Green&&&\\ \hline Blue&&&\\ \hline Violet&&&\\
\hline Ultraviolet&&&\\ \hline 
\end{tabular}
}

You'll either want different tables for different orders, or perhaps add additional stopping power columns to the one above.

\end{document}

