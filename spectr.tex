\documentclass[./manual.tex]{subfiles}
\begin{document}

\chapter{Atomic Spectroscopy}

 \textbf{Experiment objectives}: test a diffraction grating-based spectrometer, study the energy spectrum of atomic hydrogen (H) and a hydrogen-like atomic sodium (Na), determine values of quantum defects of low angular momentum states of Na, and measure fine splitting using Na yellow doublet.

\subsection*{History}

    The observation of discrete lines in the emission spectra of
    atomic gases gives insight into the quantum nature of
    atoms. Classical electrodynamics cannot explain the existence
    of these discrete lines, whose energies (or wavelengths) are
    given by characteristic values for specific atoms. These
    emission lines are so fundamental that they are used to
    identify atomic elements in objects, such as in identifying
    the constituents of stars in our universe. When Niels Bohr
    postulated that electrons can exist only in orbits of discrete
    energies, the explanation for the discrete atomic lines became
    clear.  In this laboratory you will measure the wavelengths of
    the discrete emission lines from two elements - hydrogen and sodium -
    to determine the energy levels in the hydrogen-like atoms.

\section*{Hydrogen Spectrum}

    The hydrogen atom is the simplest atom: it consists of a single proton and a single
electron. Since it is so simple, it is possible to calculate the energy spectrum of the electron
bound states - in fact you will do that in your Quantum Mechanics course.
From these calculations it
follows that the electron energy is quantized, and these energies are given by the expression:
\begin{equation}\label{Hlevels_inf}
E_n=- \frac{2\pi^2m_ee^4}{(4\pi\epsilon_0)^2h^2n^2}
 = -hc{R_y}\frac{1}{n^2}
\end{equation}
%
where  $n$  is the  {\bf principal quantum number} of the atomic level, and
\begin{equation} \label{Ry}
{R_y}=\frac{2\pi m_ee^4}{(4\pi\epsilon_0)^2ch^3}
\end{equation}
is a fundamental physical constant called the {\bf Rydberg constant} (here $m_e$ is the electron
mass).  Numerically, ${R_y} = 1.0974 \times  10^5 cm^{-1}$ and $hc{R_y} = 13.605 eV$.

Because of the allowed energies of an electron in a hydrogen atom, the electron can change its state
only by making a transition (``jump'') from one state of energy  $E_1$ to another state of lower
energy $E_2$ by emitting a photon of energy $h\nu  = E_1 - E_2$ that carries away the excess energy.
Thus, by exciting atoms into high-energy states using a discharge and then measuring the  frequencies
of  emission  one can figure out the energy separation between various energy levels. Since it is
more convenient to use a wavelength of light $\lambda$ rather than its frequency $\nu$, and using the
standard connection between the wavelength and the frequency $h\nu = hc/\lambda$, we can write the
wavelength of a photon emitted by electron jumping between the initial state $n_1$ and the final
state $n_2$ as follows:
\begin{equation} \label{Hlines_inf}
\frac{1}{\lambda_{12}}=\frac{2\pi^2m_ee^4}{(4\pi\epsilon_0)^2ch^3}
\left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right]= {R_y} \left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right]
\end{equation}
%Based on this description it is clear that by measuring the frequencies (or
%wavelengths) of photons emitted by an excited atomic system, we can glean
%important information about allowed electron energies in atoms.

%To make more accurate calculation of the Hydrogen spectrum, we need to take
%into account that a hydrogen nucleus has a large, but finite mass, M=AMp (mass
%number A=1 and Mp = mass of proton)\footnote{This might give you the notion
%that the mass of any nucleus of mass number $A$ is equal to $AM_p$. This is not
%very accurate, but it is a good first order approximation.} such that the
%electron and the nucleus orbit a common center of mass.  For this two-mass
%system the reduced mass is given by $\mu=m_e/(1+m_e/AM_p)$. We can take this
%into account by modifying the above expression (\ref{Hlines_inf}) for
%1/$\lambda$ as follows:
%\begin{equation}\label{Hlines_arb}
%\frac{1}{\lambda_A}=R_AZ^2\left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right] \mbox{
%where } R_A=\frac{R_{\infty}}{1+\frac{m_e}{AM_p}}
%\end{equation}
%In particular, for the hydrogen case of  ($Z=1$; $M=M_p$) we have:
%\begin{equation}\label{Hlines_H}
%\frac{1}{\lambda_H}=R_H\left[\frac{1}{n_2^2}-\frac{1}{n_1^2}\right]
%\end{equation}
%Notice that the value of the Rydberg constant will change slightly for
%different elements. However, these corrections are small since nucleus is
%typically several orders of magnitude heavier then the electron.


Fig.~\ref{Hspec.fig} shows the energy levels of hydrogen, and indicates a large number of observed
transitions, grouped into series with the common electron final state.  Emitted photon frequencies
span the spectrum from the UV (UltraViolet) to the IR (InfraRed). Among all
the series, only the
Balmer series, corresponding to $n_2$ = 2, has its transitions in the visible part of the spectrum.
Moreover, from all the possible transitions, we will be able to observe and measure only the
following four lines: $n_1=6 \rightarrow  2$, $5 \rightarrow  2$, $4 \rightarrow 2$,
 and $3 \rightarrow 2$.

\begin{figure}
\includegraphics[width=0.7\linewidth]{./pdf_figs/spec}
\caption{\label{Hspec.fig}Spectrum of Hydrogen. The numbers on the left show the energies of the
hydrogen levels with different principle quantum numbers $n$ in $eV$. The
wavelength of the emitted
photon in {\AA}  is shown next to each electron transition. }
\end{figure}

%In this lab, the light from the hydrogen gas is broken up into its spectral
%components by a diffraction grating.  You will measure the angle at which each
%line occurs on the left ($\theta_L$) and ($\theta_R$) right sides for as many
%diffraction orders $m$ as possible, and use Eq.(\ref{mlambda}) to calculate
%$\lambda$, using the following expression, derived in the Appendix.
%\begin{equation}\label{mlambda}
%m\lambda = \frac{h}{2}\left(\sin\theta_L+\sin\theta_R\right)
%\end{equation}
% Then the same
%expression will be used to check/calibrate the groove spacing $h$ by making
%similar measurements for a sodium spectral lines with known wavelengths.
%
%We will approach the data in this experiment both with an eye to confirming
%    Bohr's theory and from Balmer's early perspective of someone
%    trying to establish an integer power series linking the
%    wavelength of these four lines.

\section*{Sodium spectrum}
Sodium (Na) belongs to the chemical group of \emph{alkali metals} [together with lithium (Li),
potassium (K), rubidium (Rb), cesium (Cs) and Francium (Fr)]. All these elements consist of a closed
electron shell with one extra unbound electron. Not surprisingly, the energy level structure for this
free electron is very similar to that of hydrogen. For example, a Na atom has 11 electrons, and its
electronic configuration is $1s^22s^22p^63s$.  Ten closed-shell electrons effectively screen the
nuclear charge number ($Z=11$) to an effective charge $Z^*\approx 1$, so
that the $3s$ valence
electron experiences the electric field potential similar to that of a hydrogen atom, given by the
Eq.~\ref{Hlevels_inf}.

However, there is an important variation of the energy spectrum of alkali
metals, which is related to the
electron angular momentum $l$. In hydrogen the energy levels with same principle quantum number $n$
but different electron angular momentum $l=0, 1, \cdots (n-1)$ are degenerate. For Na and others the
levels with different values of $l$ are shifted with respect to each other. This is mainly due to the
interaction of the unpaired electrons with the electrons of the closed shells.  For example, the
orbits of the  electron with large angular momentum values $l$ are far
above the  closed shell, and thus
their energies are basically the same as for the hydrogen atom. An electron with smaller $l$ spends
more time closer to the nucleus, and ``feels'' stronger bounding electrostatic potential. As a result
the corresponding energy levels are pulled down compared to those of hydrogen, and the states with the
same principle number $n$ but different angular momenta $l$ are split (\emph{i.e.} have different
energies).


To take into account the modification of the atomic spectra while still using the same basic
equations used for hydrogen, it is convenient to introduce a small correction $\Delta_l$ to the
principle quantum number $n$ to take into account the level shifts. This correction is often called a
{\it quantum defect}, and its value % an effective nuclei charge $Z^*$ keeping the For each particular value
%of angular momentum $l$ the energy spectrum follows the same scaling as hydrogen atom, but with an
%effective charge $Z^*$:
%\begin{equation}\label{heq}
%E_n=-\frac{1}{2}\frac{Z^{*2}e^4}{(4\pi\epsilon_0)^2}\frac{mc^2}{\hbar^2c^2}
%\frac{1}{n^2}=-Z^{*2}\frac{hc{R_y}}{n^2}
%\end{equation}
%The value of the effective charge $Z^*$
depends on the angular momentum $l$, and does not vary much between states with different principle
quantum numbers $n$ but same $l$\footnote{The accepted notation for different electron angular
momentum states is historical, and originates from the days when the proper quantum mechanical
description for atomic spectra had not been developed yet. Back then spectroscopists had categorized
atomic spectral lines corresponding to their appearance: for example any spectral lines from electron
transitions from $s$-orbital ($l=0$) always appeared \textbf{S}harp on a photographic film, while
those with initial electron states of $d$-orbital ( $l=2$) always appeared \textbf{D}iffuse. Also
spectral lines of \textbf{P}rinciple  series (initial state is $p$-orbital, $l=1$) reproduced the
hydrogen spectrum most accurately (even though at shifted frequencies), while the
\textbf{F}undamental (initial electron state is $f$-orbital, $l=3$) series matched the absolute
energies of the hydrogen most precisely. The orbitals with higher values of the angular momentum are
denoted in an alphabetic order ($g$, $h$, \textit{etc}.) }:
\begin{equation}\label{qdef}
E_{nl}=-\frac{hc{R_y}}{(n-\Delta_l)^2}%=-\frac{hc{R_y}}{(n-\Delta_l)^2}
\end{equation}

%\begin{tabular}{ll}
%States&$Z^*$\\
%s~($l=0$)&$\approx$ 11/9.6\\
%p~($l=1$)&$\approx$ 11/10.1\\
%d~($l=2$)&$\approx$ 1\\
%f~($l=3$)&$\approx $ 1\\
%\end{tabular}
In particular, the energies of two states with the lowest angular momentum ($s$ and $p$) are
noticeably affected by the more complicated electron structure of Na, while the energy levels of the
states with the higher values of angular momentum ($d$, $f$) are identical to the hydrogen energy
spectrum.
%
%An alternative (but equivalent) procedure is to assign a {\it quantum defect} to the principle
%quantum  $n$ instead of introducing an effective nuclei charge. In this case Eq.(\ref{heq}) can be
%written as:
%
%where $n*=n-\Delta_l$, and $\Delta_l$  is the corresponding quantum defect. Fig. \ref{nadell} shows
%values of quantum defects which work approximately for the alkalis. One sees that there is one value
%for each value of the angular momentum $l$. This is not exactly true for all alkali metals, but for
%Na there is very little variation in $\Delta_l$ with $n$ for a given $l$.
\begin{figure}
\includegraphics[height=\columnwidth]{./pdf_figs/nae}
\caption{\label{nae}Energy spectrum of Na. The energy states of H are shown in far right for
comparison.}
\end{figure}
%\begin{figure}
%\includegraphics[width=0.5\columnwidth]{nadell.eps}
%\caption{\label{nadell}Quantum Defect $\Delta_l$ versus $l$ for different alkali metals. Taken from
%Condon and Shortley p. 143}
%\end{figure}
%\begin{figure}
%\includegraphics[height=3in]{nadel.eps}
%\caption{\label{nadel}Quantum Defect $\Delta_l$ variation with $n$. The
%difference between the quantum defect of each term and that of the lowest term
%of the series to which it belongs is plotted against the difference between
%the total quantum numbers of these terms. Again from Condon and Shortley p. 144.}
%\end{figure}

The spectrum of Na is shown in Fig. \ref{nae}. One can immediately see that there are many more
optical transitions because of the lifted degeneracy of energy states with different angular momenta.
However, not all electronic transitions are allowed: since the angular momentum of a photon is $1$,
then the electron angular momentum cannot change by more than one while emitting one spontaneous
photon. Thus, it is important to remember the following \emph{selection rule} for atomic transitions:
\begin{equation}\label{selrules}
\Delta l = \pm 1.
\end{equation}
According to that rule, only transitions between two ``adjacent'' series are possible: for example $p
\rightarrow s$ or $d \rightarrow p$ are allowed, while $s \rightarrow s$ or $s \rightarrow d$ are
forbidden. The strongest allowed optical transitions are shown in Fig. \ref{natrns}.
\begin{figure}
\includegraphics[height=\columnwidth]{./pdf_figs/natrans}
\caption{\label{natrns}Transitions for Na. The wavelengths of selected
	transitions are shown in {\AA}.
Note that $p$ state is now shown in two columns, one referred to as $P_{1/2}$ and the other as
$P_{3/2}$. The small difference between their energy levels is the ``fine structure''.}
\end{figure}
%\begin{figure}
%\includegraphics[height=4in]{series.eps}
%\caption{\label{series}Series for Hydrogen, Alkalis are similar.}
%\end{figure}
Note that each level for given $n$ and $l$ is split into two because of the \emph{fine structure
splitting}. This splitting is due to the effect of electron \emph{spin} and its coupling with the
angular momentum. Proper treatment of spin requires knowledge of quantum electrodynamics and solving
the Dirac equation; for now spin can be treated as an additional quantum number associated with any
particle. The spin of electron is $1/2$, and it may be oriented either along or against the non-zero
electron's angular momentum. Because of the weak coupling between the angular momentum and spin,
these two possible orientations result in small differences in energy for corresponding electron
states.

\section*{Experimental setup}
\textbf{Equipment needed}: Gaertner-Peck optical spectrometer, hydrogen discharge lamp, sodium
discharge lamp.

Fig. \ref{expspec} gives a top view of the Gaertner-Peck optical spectrometer used in this lab. The
spectrometer consists of two tubes. One tube holds a vertical collimator slit of variable width, and
should be directed toward a discharge lamp. The light from the discharge lamp passes through the
collimator slit and then gets dispersed on the diffraction grating mounted in the mounting pedestal in
the middle of the apparatus. The other tube holds the telescope that allows you to magnify the image
of the collimator slit to get a more accurate reading on the rotating table. This tube can be rotated
around the central point, so that you will be able to align and measure the wavelength of all visible
spectral lines of the lamp in all orders of diffraction grating.
\begin{figure}
\includegraphics[height=4in]{./pdf_figs/expspec}
\caption{\label{expspec}Gaertner-Peck Spectrometer}
\end{figure}

It is likely that you will find a spectrometer at nearly aligned condition at the beginning on the
lab. Nevertheless, take time making sure that all elements are in order to insure good quality of
your data.

\textit{Telescope Alignment:} Start by adjusting the telescope eyepiece in or out to bring the
crosshairs into sharp focus.  Next aim the telescope out the window to view a distant object such as
leaves in a tree.  If the distant object is not in focus, you will have to adjust the position of the
telescope tube - ask your instructor for directions.

\textit{Collimator Conditions:} Swing the telescope to view the collimator which is accepting light
from the hydrogen discharge tube through a vertical slit of variable width.  The slit opening should
be set to about 5-10 times the crosshair width to permit sufficient light to see the faint violet
line and to be able to see the crosshairs.  If the bright column of light is not in sharp focus, you
should align the position of the telescope tube again (with the help of the instructor).

\textit{Diffraction Grating Conditions:} In this experiment you will be using a diffraction grating
that has 600 lines per mm. A brief summary of diffraction grating operation is given in the Appendix
of this manual. If the grating is not already in place, put it back on the baseclamp and fix it
there. The table plate that holds the grating can be rotated, so try to orient the grating surface to
be maximally perpendicular to the collimator axis. However, the accurate
measurement of angles does
not require the perfect grating alignment. Instead, for each spectral line in each diffraction order
you will be measuring the angles on the left ($\theta_l$) and on the right ($\theta_r$), and use both
of the measurements to figure out the optical wavelength using the following equation:
\begin{equation}\label{nlambda}
m\lambda=\frac{d}{2}(\sin\theta_r+\sin\theta_l),
\end{equation}
where $m$ is the diffraction order, and $d$ is the distance between the lines in the grating.

   %  Use
%    the AUTOCOLLIMATION procedure  to achieve a fairly accurate
%    alignment of the grating surface.  This will determine how to
%    adjust the three leveling screws H1, H2, and H3 and the
%    rotation angle set screw for the grating table.
%
%    \textbf{AUTOCOLLIMATION} is a sensitive way to align an optical
%    element.  First, mount a ``cross slit'' across the objective lens of
%    the collimator, and direct a strong light source into the
%    input end of the collimator.  Some of the light exiting through
%    the cross slit will reflect from the grating and return to the
%    cross slit.  The grating can then be manipulated till this
%    reflected light retraces its path through the cross slit
%    opening.  With this the grating surface is normal to the
%    collimator light.
     Then, with the hydrogen tube ON and in place at
    the collimator slit, swing the rotating telescope slowly
    through 90 degrees both on the left \& right sides of the forward
    direction.  You should observe diffraction maxima for most
    spectral wavelength, $\lambda$, in 1st and 2nd order.  If these
    lines seem to climb uphill or drop downhill
    the grating has to be adjusted in its baseclamp to
    bring them all to the same elevation.

    Also, turn on the sodium lamp as soon as you arrive, since it requires 10-15 minutes to warm up
    and show clear Na spectrum.

\subsection*{Experimental studies of Hydrogen Balmer series}

\begin{figure}[h]
	\includegraphics[width=\linewidth]{./pdf_figs/Emission_spectrum-H}
	\caption{\label{fig:hydrogen_visible_spectrum} Hydrogen visible spectrum}
\end{figure}
Swing the rotating telescope slowly and determine which spectral lines from Balmer series you
observe. You should be able to see three bright lines - Blue, Green and Red - in the first (m=1) and
second (m=2) diffraction orders on both left \& right sides. %In the third order (m=3) only the Blue, \& Green lines are visible, and you will not see the Red.

One more line of the Balmer series is in the visible range - Violet, but its intensity is much lower
than for the other three line. However, you will be able to find it in the first order if you look
carefully with the collimator slit open a little wider. You should see
spectrum resembling one shown in Fig.~\ref{fig:hydrogen_visible_spectrum}.

%
%\emph{Lines to be measured:}
%\begin{itemize}
%\item \emph{Zero order} (m=0): All spectral lines merge.
%\item \emph{First order} (m=1): Violet, Blue, Green, \& Red on both left \&
%    right sides.
%\item \emph{Second order} (m=2): Violet, Blue, Green, \& Red on
%    both left \& right sides.
%\item \emph{Third order} (m=3):  Blue, \& Green.
%\end{itemize}
%    You might not see the Violet line due to its low
%    intensity. Red will not be seen in 3rd  order.

After locating all the lines, measure  the angles at which each line occurs. The spectrometer reading for each of the first order lines should be measured at least \emph{twice} by \textit{different} lab partners to avoid systematic errors.  For the second order lines, you only need one measurement for your group.  \textbf{Don't forget}: for every line you need to measure the angles to the right and to the left!

You should be able to determine the angle with accuracy of $1$ minute, but you should know how to
read angles with high precision in the spectrometer: first use the bottom scale to get the rough
angle reading within a half of the degree. Then use the upper scale for more accurate reading in
minutes. To get this reading find a tick mark of the upper scale that aligns perfectly with some tick
mark of the bottom scale - this is your minute reading. Total angle is the sum of two readings.

To measure the frequency precisely, center the crosshairs on the line as accurately as possible.
Choose the width of lines by turning the collimator slit adjustment screw. If the slit is too wide,
it is hard to judge the center of the line accurately; if the slit is too narrow, then not enough
light is available to see the crosshairs. For Violet the intensity is noticeably less than for the
other three lines, so you may not see it. However, even if you find it, a little assistance is
usually required in order to locate the crosshairs at this line. We suggest that a low intensity
flashlight be aimed toward the Telescope input, and switched ON and OFF repeatedly to reveal the
location of the vertical crosshair relative to the faint Violet line.

%\subsubsection*{ Calibration of Diffraction Grating:} Replace the hydrogen tube with
%    a sodium (Na) lamp and take readings for the following two
%    lines from sodium: $568.27$~nm (green) and $589.90$~nm (yellow).  Extract from
%    these readings the best average value for $h$ the groove
%    spacing in the diffraction grating.  Compare to the statement
%    that the grating has 600 lines per mm.  Try using your measured value
%    for $h$ versus the stated value $600$ lines per mm in
%    the diffraction formula when obtaining the measured
%    wavelengths of hydrogen. Determine which one provide more accurate results, and discuss the conclusion.

\subsubsection*{ Data analysis for Hydrogen Data}
Calculate the wavelength $\lambda$ for each line observed in all orders, calculate the average
wavelength value and uncertainty for each line, and then identify the initial and final electron
states principle numbers ($n_1$ and $n_2$) for each line using Fig.~\ref{Hspec.fig}.

Make a plot of $1/\lambda$  vs $1/n_1^2$ where $n_1$ = the principal quantum number of the electron's
initial state. Put all $\lambda$ values you measure above on this plot.
Your data points should form a
straight line. From Equation~(\ref{Hlines_inf}) determine the physical meaning of both slope and
intercept, and compare the data from the fit to the expected values for each of them. The slope
should be the Rydberg constant for hydrogen, ${R_y}$. The intercept is ${R_y}/(n_2)^2$. From this,
determine the value for the principal quantum number $n_2$. Compare to the accepted value in the
Balmer series.

\subsection*{Experimental studies of Sodium Spectrum}

Switch to Sodium lamp and make sure the lamp warms up for approximately 5-10 minutes before starting
the measurements.

Double check that you see a sharp spectrum in the spectrometer (adjust the width of the collimator
slit if necessary). In the beginning it will be very useful for each lab partner to quickly scan the
spectrometer telescope through all first-order lines, and then discuss
which lines you see correspond
with  which transitions in Table~\ref{tab:sodium} and Fig.~\ref{natrns}. Keep in mind that the color
names are symbolic rather than descriptive!

After that, carefully measure the left and right angles for as many
spectral lines in the first and second orders
as possible. The spectrometer reading for each line should be measured at least \emph{once} by
both lab partners to avoid systematic errors.

Determine  the wavelengths of all measured Na spectral lines using Eq. \ref{nlambda}. Compare these measured mean wavelengths to the accepted values given in Fig.~\ref{natrns} and in Tab.~\ref{tab:sodium}. Identify  at least seven of the lines with a particular transition. The eight line in Tab.~\ref{tab:sodium} has a wavelength $\lambda = 4494.3${\AA}. It corresponds to the $8d \rightarrow 3p$ transition but isn't shown in Fig.~\ref{natrns}.

\begin{table}

\centering
\begin{tabular}{l|l|l} 
  Color&Line$_1$(\AA)&Line$_2$(\AA)\\ \hline
Red&6154.3&6160.7\\
Yellow & 5890.0&5895.9\\
Green & 5682.7&5688.2\\
&5149.1&5153.6\\
& 4978.6&4982.9\\
Blue&4748.0&4751.9\\
&4664.9&4668.6\\
Blue-Violet&4494.3&4497.7\\
\end{tabular}
\caption{\label{tab:sodium}Wavelength of the visible sodium lines.}
\end{table}
Line$_1$ and Line$_2$ correspond to transitions to two fine-splitted $3p$ states $P_{1/2}$ and
$P_{3/2}$. These two transition frequencies  are very close to each other, and to resolve them with
the spectrometer the width of the slit should be very narrow. However, you may not be able to see
some weaker lines then. In this case you should open the slit wider to let more light in when
searching for a line. If you can see a spectral line but cannot resolve the doublet, record the
reading for the center of the spectrometer line, and use the average of two wavelengths given above.

\textbf{Measurements of the fine structure splitting}. Once you measured all visible spectral lines,
go back to some bright line (yellow should work well), and close the collimator slit such that you
can barely see any light going through. In this case you should be able to see the splitting of the
line because of the \emph{fine structure splitting} of states $P_{1/2}$ and $P_{3/2}$ . For the Na D
doublet the splitting between two lines $\Delta\lambda=\lambda_{3/2}-\lambda_{1/2}$.

Measure the splitting between two lines in the first and the second orders. Which one works better?
Discuss this issue in your lab report. Compare to the accepted value: $\Delta\lambda=5.9$~\AA.
Compare this approach to the use of the Fabry-Perot interferometer.


\subsection*{Data analysis for Sodium}

\textbf{Calculation of a quantum defect for $n=3, p$ state. }

Once you identified all of your measured spectral lines, choose only those that correspond to optical
transitions from any $d$ to $n=3,p$ states. Since the energy states of $d$ series follows the
hydrogen spectra almost exactly, the wavelength of emitted light $\lambda$ is given by:
\begin{equation}
\frac{hc}{\lambda}=E_{nd}-E_{3p}=-\frac{hc{R_y}}{n_d^2}+\frac{hc{R_y}}{(3-\Delta_p)^2},
\end{equation}
or
\begin{equation}
\frac{1}{\lambda}=\frac{{R_y}}{(3-\Delta_p)^2}-\frac{{R_y}}{n_d^2},
\end{equation}
 where $n_d$ is the principle number of the initial $d$ state. Verify this
expression by plotting $1/\lambda$ versus $1/n_d^2$ for the $n_d$= 4,5, and 6. From the slope of this
curve determine the value of the Rydberg constant ${R_y}$. The value of the intercept in this case is
$\frac{{R_y}}{(3-\Delta_p)^2}$, so use it to find the quantum defect $\Delta_p$.

Compare the results of your calculations for the quantum defect with the accepted value
$\Delta_p=0.86$.

%\subsection*{Calculation of a quantum defect for $s$ states}
%Now consider the transition from the $s$-states ($n=5,6,7$) to to the $n=3, p$ state. Using
%$hc/\lambda=E_{ns}-E_{3p}$ and the results of your previous calculations, determine the energies
%$E_{sn}$ for different $s$ states with $n=5,6,7$  and calculate $\Delta_s$. Does the value of the
%quantum defect depends on $n$?
%

%\textbf{Example data table for writing the results of the measurements}:
%
%\noindent
%\begin{tabular}{|p{1.in}|p{1.in}|p{1.in}|p{1.in}|}
%\hline
% Line &$\theta_L$&$\theta_R$&Calculated $\lambda$  \\ \hline
% m=1 Violet&&&\\ \hline
% m=1 Blue&&&\\ \hline
% m=1 Green&&&\\ \hline
% m=1 Red&&&\\ \hline
% m=2 Violet&&&\\ \hline
% \dots&&&\\ \hline
% m=3 Blue&&&\\ \hline
% \dots&&&\\\hline
%\end{tabular}

\section*{Appendix: Operation of a diffraction grating-based optical spectrometer}

%\subsection*{Fraunhofer Diffraction at a Single Slit}
%Let's consider a plane electromagnetic wave incident on a vertical slit of
%width $D$ as shown in Fig. \ref{frn}. \emph{Fraunhofer} diffraction is
%calculated in the far-field limit, i.e. the screen is assume to be far away
%from the slit; in this case the light beams passed through different parts of
%the slit are nearly parallel, and one needs  a lens to bring them together and
%see interference.
%\begin{figure}[h]
%\includegraphics[width=0.7\linewidth]{frnhfr.eps}
%\caption{\label{frn}Single Slit Fraunhofer Diffraction}
%\end{figure}
%To calculate the total intensity on the screen we need to sum the contributions
%from different parts of the slit, taking into account phase difference acquired
%by light waves that traveled different distance to the lens. If this phase
%difference is large enough we will see an interference pattern. Let's break the
%total hight of the slit by very large number of point-like radiators with
%length $dx$, and we denote $x$ the hight of each radiator above the center of
%the slit (see Fig.~\ref{frn}). If we assume that the original incident wave is
%the real part of $E(z,t)=E_0e^{ikz-i2\pi\nu t}$, where $k=2\pi/\lambda$ is the
%wave number. Then the amplitude of each point radiator on a slit is
%$dE(z,t)=E_0e^{ikz-i2\pi\nu t}dx$. If the beam originates at a hight $x$ above
%the center of the slit then the beam must travel an extra distance $x\sin
%\theta$ to reach the plane of the lens. Then we may write a contributions at
%$P$ from a point radiator $dx$ as the real part of:
%\begin{equation}
%dE_P(z,t,x)=E_0e^{ikz-i2\pi\nu t}e^{ikx\sin\theta}dx.
%\end{equation}
%To find the overall amplitude one sums along the slit we need to add up the
%contributions from all point sources:
%\begin{equation}
%E_P=\int_{-D/2}^{D/2}dE(z,t)=E_0e^{ikz-i2\pi\nu
%t}\int_{-D/2}^{D/2}e^{ikx\sin\theta}dx = A_P e^{ikz-i2\pi\nu t}.
%\end{equation}
%Here $A_P$ is the overall amplitude of the electromagnetic field at the point
%$P$. After evaluating the integral we find that
%\begin{equation}
%A_P=\frac{1}{ik\sin\theta}\cdot
%\left(e^{ik\frac{D}{2}\sin\theta}-e^{-ik\frac{D}{2}\sin\theta}\right)
%\end{equation}
%After taking real part and choosing appropriate overall constant multiplying
%factors the amplitude of the electromagnetic field at the point $P$ is:
%\begin{equation}
%A=\frac{\sin (\frac{\pi D}{\lambda}\sin\theta)}{\frac{\pi
%D}{\lambda}\sin\theta}
%\end{equation}
%The intensity is proportional to the square of the amplitude and thus
%\begin{equation}
%I_P=\frac{(\sin (\frac{\pi D}{\lambda}\sin\theta))^2}{(\frac{\pi
%D}{\lambda}\sin\theta)^2}
%\end{equation}
%The minima of the intensity occur at the zeros of the argument of the sin. The
%maxima are near, but not exactly equal to the solution of:
%\begin{equation}
%  (\frac{\pi D}{\lambda}\sin\theta)=(m+\frac{1}{2})\pi \end{equation}
%for integer $m$.
%
%The overall pattern looks like that shown in Fig. \ref{sinxox}.
%\begin{figure}
%\includegraphics[width=\linewidth]{sinxox.eps}
%\caption{\label{sinxox}Intensity Pattern for Fraunhofer Diffraction}
%\end{figure}

%\subsection*{The Diffraction Grating}
A diffraction grating is a common optical element, which consists of a pattern
with many equidistant slits or grooves. Interference of multiple beams passing
through the slits (or reflecting off the grooves) produces sharp intensity
maxima in the output intensity distribution, which can be used to separate
different spectral components on the incoming light. In this sense the name
``diffraction grating'' is somewhat misleading, since we are used to
talking about
diffraction with regard to the modification of light intensity distribution  to
the finite size of a single aperture.
\begin{figure}[h]
\includegraphics[width=\linewidth]{./pdf_figs/grating}
\caption{\label{grating}Intensity Pattern for Fraunhofer Diffraction}
\end{figure}

To describe the properties of a light wave after passing through the grating,
let us first consider the case of 2 identical slits separated by the distance
$h$, as shown in Fig.~\ref{grating}a. We will assume that the size of the slits
is much smaller than the distance between them, so that the effect of
Fraunhofer diffraction on each individual slit is negligible. Then the
resulting intensity distribution on the screen is given by the familiar Young
formula:
\begin{equation} \label{2slit_noDif}
I(\theta)=\left|E_0 +E_0e^{ikh\sin\theta} \right|^2 = 4I_0\cos^2\left(\frac{\pi
h}{\lambda}\sin\theta \right),
\end{equation}
where $k=2\pi/\lambda$, $I_0$ = $|E_0|^2$, and the angle $\theta$ is measured
with respect to the normal to the plane containing the slits.
%If we now include the Fraunhofer diffraction on each slit
%same way as we did it in the previous section, Eq.(\ref{2slit_noDif}) becomes:
%\begin{equation} \label{2slit_wDif}
%I(\theta)=4I_0\cos^2\left(\frac{\pi h}{\lambda}\sin\theta
%\right)\left[\frac{\sin (\frac{\pi D}{\lambda}\sin\theta)}{\frac{\pi
%D}{\lambda}\sin\theta} \right]^2.
%\end{equation}

Interference on $N$ equidistant slits illuminated by a plane wave
(Fig.~\ref{grating}b) produces much sharper maxima. To find light intensity on
a screen, the contributions from all N slits must be summarized taking into
account their acquired phase difference, so that the optical field intensity
distribution becomes:
\begin{equation} \label{Nslit_wDif}
I(\theta)=\left|E_0
+E_0e^{ikh\sin\theta}+E_0e^{2ikh\sin\theta}+\dots+E_0e^{(N-1)ikh\sin\theta}
\right|^2 = I_0\left[\frac{sin\left(N\frac{\pi
h}{\lambda}\sin\theta\right)}{sin\left(\frac{\pi h}{\lambda}\sin\theta\right)}
\right]^2.
\end{equation}
 Here we again neglect the diffraction from each individual slit, assuming that the
 size of the slit is much smaller than the separation $h$ between the slits.

The intensity distributions from a diffraction grating with illuminated
 $N=2,5$ and $10$ slits are shown in Fig.~\ref{grating}c. The tallest (\emph{principle}) maxima occur when the denominator
 of Eq.(~\ref{Nslit_wDif}) becomes zero: $h\sin\theta=\pm m\lambda$ where
 $m=1,2,3,\dots$ is the diffraction order. The heights of the principle maxima are
 $I_{\mathrm{max}}=N^2I_0$, and their widths are $\Delta \theta =
 2\lambda/(Nh)$.
 Notice that the more slits are illuminated, the narrower diffraction peaks
 are, and the better the resolution of the system is:
 \begin{equation}
\frac{ \Delta\lambda}{\lambda}=\frac{\Delta\theta}{\theta} \simeq \frac{1}{Nm}
\end{equation}
For that reason in any spectroscopic equipment a light beam is usually expanded
to cover the maximum surface of a diffraction grating.

\subsection*{Diffraction Grating Equation when the Incident Rays are
not Normal}

Up to now, we assumed that the incident optical wavefront is normal to the pane of a grating. Let's
now consider more general case when the angle of incidence $\theta_i$ of the incoming wave is
different from the normal to the grating, as shown in Fig. \ref{DGnotnormal}(a). Rather than
calculating the whole intensity distribution, we will determine the positions of principle maxima.
The path length difference between two rays 1 and 2 passing through the consequential slits is $a+b$,
where:
\begin{equation}
a=h\sin \theta_i;\,\, b=h\sin \theta_R
\end{equation}
Constructive interference occurs for order $m$ when $a+b=m\lambda$, or:
\begin{equation}
h\sin \theta_i + h\sin\theta_R=m\lambda
\end{equation}

\begin{figure}
\includegraphics[width=\columnwidth]{./pdf_figs/pic4i}
%\includegraphics[height=3in]{dn.eps}
\caption{\label{DGnotnormal}Diagram of the light beams diffracted to the right
(a) and to the left (b).}
\end{figure}

Now consider the case shown in Fig. \ref{DGnotnormal}(b). The path length between two beams is now
$b-a$ where $b=h\sin\theta_L$. Combining both cases we have:
\begin{eqnarray} \label{angles}
h\sin\theta_L-h\sin\theta_i&=&m\lambda\\
h\sin\theta_R+h\sin\theta_i&=&m\lambda \nonumber
\end{eqnarray}
Adding these equations and dividing the result by 2 yields the following expression connecting the
right and left diffraction angles:
\begin{equation}m\lambda=\frac{h}{2}\left(\sin\theta_L+\sin\theta_R\right)
\end{equation}

\end{document}

